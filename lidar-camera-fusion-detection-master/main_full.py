import pandas as pd
import numpy as np

from sklearn.preprocessing import LabelEncoder,StandardScaler
from sklearn.model_selection import train_test_split,TimeSeriesSplit
import datetime
import h5py
import torch.nn.utils.prune as prune
from torch.utils.data import DataLoader,TensorDataset
import warnings
warnings.filterwarnings("ignore")
# 画出roc_auc曲线
from sklearn.metrics import roc_curve
from sklearn.metrics import RocCurveDisplay
from matplotlib import pylab as plt

from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import roc_auc_score,f1_score,precision_score,recall_score
import seaborn as sns
from src.models.edgenet import EdgeFirstNet,EdgeNet
from data import *
from glob import glob
from config import *
from src.models.edgeconv import EdgeConv,SmartDownSampling,Dynamic_conv2d,BNReLUConv2d,LightweightConv2d

import torch.nn as nn
import torch.nn.functional as F
import torch
from torch import nn
import torchvision.datasets as dset
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler,Dataset
from torch.utils.data.distributed import DistributedSampler
from torch.nn.parallel import DistributedDataParallel 

import torch.nn.functional as F
from torch import optim
from torch.autograd import Variable
from torch.autograd import grad

from  torchvision.datasets import mnist
import time

from tqdm import tqdm as tqdm
import time
from datetime import timedelta
import os
from torchvision import transforms
import config
import logging
logging.basicConfig(filename='train.log',
                    filemode='a',##模式，有w和a，w就是写模式，每次都会重新写日志，覆盖之前的日志
                    format='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s')


class FocalLoss(nn.Module):
    def __init__(self, alpha=1, gamma=2, logits=False, reduce=True):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.logits = logits
        self.reduce = reduce

    def forward(self, inputs, targets,ind=None, mask=None, cat=None):
        if self.logits:
            BCE_loss = F.binary_cross_entropy_with_logits(inputs, targets, reduce=False)
        else:
            BCE_loss = F.binary_cross_entropy(inputs, targets, reduce=False)
        pt = torch.exp(-BCE_loss)
        F_loss = self.alpha * (1-pt)**self.gamma * BCE_loss

        # mask = targets == 1
        # F_loss = F_loss.masked_select(mask).mean()


        if self.reduce:
            return torch.mean(F_loss)
        else:
            return F_loss

def _gather_feat(feat, ind, mask=None):
    # dim = B,M,C
    dim  = feat.size(2)

    ind  = ind.unsqueeze(2).expand(ind.size(0), ind.size(1), dim)
    
    feat = feat.gather(1, ind)
    if mask is not None:
        mask = mask.unsqueeze(2).expand_as(feat)
        feat = feat[mask]
        feat = feat.view(-1, dim)
    return feat

def _transpose_and_gather_feat(feat, ind):


    # B x C x H x W
    feat = feat.permute(0, 2, 3, 1).contiguous()
    # B x H x W x C
    feat = feat.view(feat.size(0), -1, feat.size(3))
    # M = H x W
    # B x M x C

    feat = _gather_feat(feat, ind)
    return feat




class RegLoss(nn.Module):
  '''Regression loss for an output tensor
    Arguments:
      output (batch x dim x h x w)
      mask (batch x max_objects)
      ind (batch x max_objects)
      target (batch x max_objects x dim)
  '''
  def __init__(self):
    super(RegLoss, self).__init__()
  
  def forward(self, output, mask, ind, target):
    pred = _transpose_and_gather_feat(output, ind)
    # print('pred',pred.size())
    mask = mask.float().unsqueeze(2) 
    # print('mask',mask.size())
    

    # print('pred',pred[0])
    # print('target',target[0])
    
    loss = F.l1_loss(pred*mask, target*mask, reduction='none')
    loss = loss / (mask.sum() + 1e-8)
    loss = loss.transpose(2 ,0).sum(dim=2).sum(dim=1)
    return loss.sum()

class FastFocalLoss(nn.Module):
  '''
  Reimplemented focal loss, exactly the same as the CornerNet version.
  Faster and costs much less memory.
  '''
  def __init__(self,alpha=2,beta=4):
    super(FastFocalLoss, self).__init__()
    self.alpha = alpha
    self.beta = beta

  def forward(self, out, target, ind, mask, cat):
    '''
    Arguments:
      out, target: B x C x H x W
      ind, mask: B x M
      cat (category id for peaks): B x M
    '''
    
    
    # out = F.softmax(out,1)
    # print('out',out.sum())
    mask = mask.float()
    gt = torch.pow(1 - target, self.beta)
    # print('gt',gt.sum())
    # print('torch.log(1 - out) ',(torch.log(1 - out)).sum())
    neg_loss = torch.log(1 - out) * torch.pow(out, self.alpha) * gt
    neg_loss = neg_loss.sum()
    # print('neg_loss',neg_loss)
    
    pos_pred_pix = _transpose_and_gather_feat(out, ind) # B x M x C
    target_pix = _transpose_and_gather_feat(target, ind) # B x M x C
    
    
    # print('target_pix',target_pix)
    
    # print('pos_pred_pix',pos_pred_pix.sum())
    
    pos_pred = pos_pred_pix.gather(2, cat.unsqueeze(2)) # B x M
    # print('pos_pred',pos_pred.sum())
    
    num_pos = mask.sum()+1e-8
    pos_loss = torch.log(pos_pred) * torch.pow(1 - pos_pred, self.alpha) * \
               mask.unsqueeze(2)
    pos_loss = pos_loss.sum()
    # print('pos_loss',pos_loss)
    if num_pos == 0:
      return - neg_loss
    
    return - (pos_loss + neg_loss) / num_pos




def set_seed(seed):
     torch.manual_seed(seed)
     torch.cuda.manual_seed_all(seed)
     np.random.seed(seed)
     torch.backends.cudnn.deterministic = True


def evaluate( model, data_iter,device=None):

    model.eval()

    loss_total = 0.0
    cls_loss_total = 0.0
    reg_loss_total = 0.0

    with torch.no_grad():
        for trains,labels,reg_labels,ind,mask,cat in data_iter: 
            trains = trains.to(device)
            labels = labels.to(device)
            reg_labels = reg_labels.to(device)

            ind = ind.to(device)
            mask = mask.to(device)
            cat = cat.to(device)

            
            cls_labels = labels[:,:n_classes,:,:]

            

            outputs = model(trains)

            cls_outputs,reg_outputs = outputs



            cls_loss = cls_loss_function(cls_outputs,cls_labels,ind,mask,cat)
            # print('cls_loss',cls_loss,cls_loss.size())
            
            reg_loss = reg_loss_function(reg_outputs,mask,ind,reg_labels)
            # print('reg_loss',reg_loss,reg_loss.size())

            loss = cls_loss + loc_loss_weight*reg_loss


            cls_loss_total+=cls_loss
            reg_loss_total+=reg_loss

            loss_total+=loss

    return loss_total / len(data_iter),cls_loss_total / len(data_iter),reg_loss_total / len(data_iter)






def predict( model, data_iter,device=None):

    model.eval()
    loss_total = 0

    class_gt = []
    reg_gt = []
    class_res = []
    reg_res = []
    with torch.no_grad():
        for trains,labels,reg_labels,ind,mask,cat in data_iter: 
            trains = trains.to(device)
            labels = labels.to(device)
            reg_labels = reg_labels.to(device)
            
            cls_labels = labels[:,:n_classes,:,:]


            class_gt.append(cls_labels.cpu().numpy())
            reg_gt.append(reg_labels.cpu().numpy())
            
            outputs = model(trains)
            
            cls_outputs,reg_outputs = outputs

            class_res.append(cls_outputs.cpu().numpy())
            reg_res.append(reg_outputs.cpu().numpy())
            # cls_loss = cls_loss_function(cls_outputs,cls_labels)
            # # print('cls_loss',cls_loss,cls_loss.size())

            # reg_loss = reg_loss_function(reg_outputs,reg_labels)
            # # print('reg_loss',reg_loss,reg_loss.size())

            # loss = cls_loss.to(device) + 0.1*reg_loss.to(device)

            # loss_total+=loss


    class_gt = np.concatenate(class_gt,0)
    reg_gt = np.concatenate(reg_gt,0)
    class_res = np.concatenate(class_res,0)
    reg_res = np.concatenate(reg_res,0)

    print('reg_gt',reg_gt.shape)
    print('reg_res',reg_res.shape)

    return class_gt,reg_gt,class_res,reg_res




def get_time_dif(start_time):
    """获取已使用时间"""
    end_time = time.time()
    time_dif = end_time - start_time
    return timedelta(seconds=int(round(time_dif)))
import argparse




def parse_args():
    parser = argparse.ArgumentParser(description="Train a detector")
    parser.add_argument("--merge_data", type=int, default=config.merge_data, help="merge_data")
    parser.add_argument("--train_model", type=int, default=config.train_model, help="train_model")
    parser.add_argument("--load_model", type=int, default=config.load_model, help="load_model")
    parser.add_argument("--batch_size", type=int, default=config.batch_size, help="batch_size")
    parser.add_argument("--learning_rate", type=float, default=config.learning_rate, help="learning_rate")
    parser.add_argument("--save_path", type=str, default='dla_dynamic_conv2d_bn_normal.h5', help="save_path")
    parser.add_argument("--log_path", type=str, default='train.log', help="log_path")
    parser.add_argument("--data_path", type=str, default='/workspace/input', help="data_path")
    parser.add_argument("--loc_loss_weight", type=float, default=config.loc_loss_weight, help="loc_loss_weight")

    parser.add_argument("--n_train_samples", type=int, default=8000, help="n_train_samples")
    parser.add_argument("--n_test_samples", type=int, default=2000, help="n_train_samples")
    parser.add_argument("--device_id", type=int, default=2, help="device_id")
    parser.add_argument("--kernel", type=str, default='dynamic', help="kernel")
    
    

    # parser.add_argument("--save_path", type=str, default='dla_edgeFirstnet_bn_normal.h5', help="save_path")

    args = parser.parse_args()
    return args


set_seed(seed)

def data_loader(data_path):
    range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box = pd.read_pickle(data_path)
    mask = (ind>0).astype(np.float32)
    range_image_tensor = np.expand_dims(range_image_tensor,0)
    range_image_mask = np.expand_dims(range_image_mask,-1)
    range_image_mask = np.expand_dims(range_image_mask,0)
    x = np.concatenate([range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask],axis=-1).transpose(0,3,1,2).squeeze(0)
    # print('x.shape',x.shape)
    # print('hm.shape',hm.shape)
    # print('ind.shape',ind.shape)
    # print('mask.shape',mask.shape)
    # print('cat.shape',cat.shape)

    return torch.FloatTensor(x),torch.FloatTensor(hm),torch.FloatTensor(reg_box),torch.LongTensor(ind),torch.LongTensor(mask),torch.LongTensor(cat)


class CustomDataset(Dataset):

    def __init__(self,files = '/workspace/input/train/*.pkl',loader=data_loader,limit=100000000000):
        print('files',files)
        self.file_list = glob(files)[:limit]
        self.loader = loader


    def __getitem__(self,index):

        return self.loader(self.file_list[index])

    def __len__(self):
        return len(self.file_list)




if __name__ == '__main__':
    args = parse_args()
    merge_data = args.merge_data==1
    load_model = args.load_model==1
    train_model = args.train_model==1
    learning_rate = args.learning_rate
    save_path = args.save_path
    batch_size = args.batch_size
    loc_loss_weight = args.loc_loss_weight
    n_train_samples = args.n_train_samples
    n_test_samples = args.n_test_samples
    data_path = args.data_path
    kernel = args.kernel
    log_path = args.log_path
    device_id = args.device_id

    if torch.cuda.is_available():
        torch.cuda.set_device(device_id)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') 

    if kernel == 'dynamic':
        kernel_func  = Dynamic_conv2d
    elif kernel == 'edgeconv':
        kernel_func = EdgeConv
    elif kernel == 'conv2d':
        kernel_func = BNReLUConv2d
    elif kernel =='lightconv2d':
        kernel_func = LightweightConv2d


    print('merge_data',merge_data)
    print('load_model',load_model)
    print('train_model',train_model)

    import faulthandler
    # 在import之后直接添加以下启用代码即可
    faulthandler.enable()

    
    # train_dataset = TensorDataset(torch.FloatTensor(X_train),torch.FloatTensor(y_train),torch.FloatTensor(y_train_box),torch.LongTensor(X_train_ind),torch.LongTensor(X_train_mask),torch.LongTensor(X_train_cat))
    
    train_dataset = CustomDataset(data_path+'/train/*.pkl',limit = n_train_samples)
    train_iter = DataLoader(dataset=train_dataset,batch_size=batch_size,shuffle=True)
    
    # dev_dataset = TensorDataset(torch.FloatTensor(X_test),torch.FloatTensor(y_test),torch.FloatTensor(y_test_box),torch.LongTensor(X_test_ind),torch.LongTensor(X_test_mask),torch.LongTensor(X_test_cat))
    dev_dataset = CustomDataset(data_path+'/test/*.pkl',limit = 2000)
    dev_iter = DataLoader(dataset=dev_dataset,batch_size=16,shuffle=False)
    
    
    # 初始化模型
    
    model = EdgeNet(in_channels=7,num_classes=n_classes,kernel=kernel_func)
    # def weight_init(m):
    #     if isinstance(m, nn.Linear):
    #         nn.init.xavier_normal_(m.weight)
    #         nn.init.constant_(m.bias, 0)
    #     # 也可以判断是否为conv2d，使用相应的初始化方式 
    #     elif isinstance(m, nn.Conv2d):
    #         nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
    #      # 是否为批归一化层
    #     elif isinstance(m, nn.BatchNorm2d):
    #         nn.init.constant_(m.weight, 1)
    #         nn.init.constant_(m.bias, 0)
    # # 2. 初始化网络结构        
    # # 3. 将weight_init应用在子模块上
    # model.apply(weight_init)

    # device_ids = [0,1,2,3]
    # nn.DataParallel(net, device_ids=device_ids)
    # model = nn.DataParallel(model)
    
    model = model.to(device)
    
    
    
    cls_loss_function = FastFocalLoss()
    # cls_loss_function = FocalLoss()
    reg_loss_function = RegLoss()
    


    files=data_path+'/train/*.pkl'
    X_train = glob(files[:n_train_samples])
    # early stoping所需要的的提升批次数
    require_improvement = require_improvement_epoch*int(len(X_train)/batch_size)
    
    # save_path = '%s.h5'%model.__class__.__name__

    # save_path = 'dla_edge.h5'
    
    # 加载模型
    if load_model:
        model.load_state_dict(torch.load(save_path))


    # 当前时间
    start_time = time.time()
    

    # 初始化RMSprop作为梯度下降算法，赋值模型的所有参数
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)

    total_batch = 0  # 记录进行到多少batch
    dev_best_loss = float('inf')
    last_improve = 0  # 记录上次验证集loss下降的batch数
    flag = False  # 记录是否很久没有效果提升
    
    evaluation_results = []

    # 选择是否训练模型，开始训练
    if train_model:
        for epoch in range(num_epochs):
            model.train()
            # batches = [[x,y] for x,y in zip(X_train,y_train)]
            print('Epoch [{}/{}]'.format(epoch + 1, num_epochs))
            for i, (trains,labels,reg_labels,ind,mask,cat) in tqdm(enumerate(train_iter)):
                trains = trains.to(device)
                labels = labels.to(device)
                reg_labels = reg_labels.to(device)
                
                # print('reg_labels',reg_labels.size())

                ind = ind.to(device)
                mask = mask.to(device)
                cat = cat.to(device)

                # labels = labels.view(-1)

                cls_labels = labels#[:,:n_classes,:,:]
                # reg_labels = labels[:,n_classes:,:,:]


                # print('mask.size()',mask.size())

                model.zero_grad()


                outputs = model(trains)
                
                cls_outputs,reg_outputs = outputs
                
                # print('cls_outputs',cls_outputs.shape)
                # print('reg_outputs',reg_outputs.shape)
                
                cls_loss = cls_loss_function(cls_outputs,cls_labels,ind,mask,cat)
                # print('cls_loss',cls_loss,cls_loss.size())
                
                reg_loss = reg_loss_function(reg_outputs,mask,ind,reg_labels)
                # print('reg_loss',reg_loss,reg_loss.size())

                loss = cls_loss + loc_loss_weight*reg_loss
                
                
                # print('cls_outputs',cls_outputs)
                # print('reg_outputs',reg_outputs)
                # # loss = reg_loss
                
                # print('cls_loss',cls_loss,'reg_loss',reg_loss)
                
                
                loss.backward()
                
                nn.utils.clip_grad_norm_(model.parameters(),10,1)
                optimizer.step()
                
                # print('steped')
                # 是否训练完一轮
                if (total_batch % int(len(X_train)/batch_size) == 0):
                #if i%5==0:
                # if (total_batch % int(X_train.shape[0]/batch_size) == 0):
                    model.eval()
                    # 每多少轮输出在训练集和验证集上的效果
                    train_acc = 0
                    
                    train_loss = loss.item()
                    
                    test_train_dataset = CustomDataset(data_path+'/train/*.pkl',limit=100)
                    test_train_iter = DataLoader(dataset=test_train_dataset,batch_size=16,shuffle=False)
                    train_loss,train_cls_loss,train_reg_loss = evaluate(model, test_train_iter,device)
                    
                    dev_iter = DataLoader(dataset=dev_dataset,batch_size=16,shuffle=False)
                    dev_loss,dev_cls_loss,dev_reg_loss = evaluate(model, dev_iter,device)
                    dev_acc = 0

                    if ealry_stop:
                        # 当前loss小于最优loss时，保存模型且赋值为新的最优loss
                        if dev_loss < dev_best_loss:
                            dev_best_loss = dev_loss
                            torch.save(model.state_dict(), save_path)
                            improve = '*'
                            last_improve = total_batch
                        else:
                            improve = ''
                                        
                    else:
                        dev_best_loss = dev_loss
                        torch.save(model.state_dict(), save_path)
                        improve = '*'
                        last_improve = total_batch


                    time_dif = get_time_dif(start_time)
                    evaluation_results.append(dev_loss)
                    print(f'Epoch: {epoch+1:2} Loss: {loss.item():10.8f}')
                    msg = 'Iter: {0:>6},  Train Loss: {1:>5.4},  Train cls: {2:>5.4},  Train reg: {3:>5.4},  Val Loss: {4:>5.4},  Val cls: {5:>5.4},  Val reg: {6:>5.4}  Time: {7} {8}'
                    print(msg.format(total_batch, train_loss, train_cls_loss,train_reg_loss, dev_loss,dev_cls_loss,dev_reg_loss, time_dif, improve))

                    logging.info(f'Epoch: {epoch+1:2} Loss: {loss.item():10.8f}')
                    # logging.info()

                    with open(log_path,'a') as fo:
                        fo.write(msg.format(total_batch, train_loss, train_cls_loss,train_reg_loss, dev_loss,dev_cls_loss,dev_reg_loss, time_dif, improve)+'\n')

                    model.train()
                total_batch += 1

                if total_batch - last_improve > require_improvement:
                    # 验证集loss超过1000batch没下降，结束训练
                    print("No optimization for a long time, auto-stopping...")
                    flag = True
                    break
            if flag:
                break
    
    


    train_dataset = CustomDataset(data_path+'/train/*.pkl',limit=100)
    train_iter = DataLoader(dataset=train_dataset,batch_size=batch_size,shuffle=True)
    
    # dev_dataset = TensorDataset(torch.FloatTensor(X_test),torch.FloatTensor(y_test),torch.FloatTensor(y_test_box),torch.LongTensor(X_test_ind),torch.LongTensor(X_test_mask),torch.LongTensor(X_test_cat))
    dev_dataset = CustomDataset(data_path+'/test/*.pkl',limit=6000)
    dev_iter = DataLoader(dataset=dev_dataset,batch_size=16,shuffle=False)
    

    model.load_state_dict(torch.load(save_path))
    

    start_time = time.time()
    class_gt,reg_gt,class_res,reg_res = predict(model,train_iter,device)
    
    
    class_gt,reg_gt,class_res,reg_res = predict(model,dev_iter,device)
    
    time_dif = get_time_dif(start_time)
    print('inference time',time_dif)
    pd.to_pickle((class_gt,reg_gt,class_res,reg_res),data_path+'/pred_train.pkl')
    pd.to_pickle((class_gt,reg_gt,class_res,reg_res),data_path+'/pred_test.pkl')
    
