import os
import pickle
import numpy as np
from numpy import unravel_index
import pandas as pd
from scipy.stats import norm
import torch
import torch.nn as nn
import tensorflow as tf
from src.models.edgeconv import SmartDownSampling
from waymo_open_dataset.utils import frame_utils, transform_utils, range_image_utils,box_utils
from waymo_open_dataset import dataset_pb2 as open_dataset
from waymo_open_dataset import dataset_pb2
from matplotlib import pylab as plt
import pandas as pd
import cv2
from glob import glob
from tqdm import tqdm
try:
    tf.enable_eager_execution()
except:
    pass

from config import *


output_stride = 2
min_overlap = 0.1
_min_radius = 2



def wrapper(gen):
    while True:
        try:
            yield next(gen)
        except StopIteration:
            break
        except Exception as e:
            # print('exception in wrapper',e)
            continue



# def get_heatmap3d(points_tensor,centers,dims,angles,range_image_size,coords=None,masks=None):
#     # print('len(centers)',len(centers))

#     # print('np.array(centers)',np.array(centers).shape)
#     # print('np.array(dims)',np.array(dims).shape)
#     # print('np.array(angles)',np.array(angles).shape)

#     if len(masks.shape)==3:
#         masks = np.expand_dims(masks,0)

#     print('coords',coords.shape)
#     print('masks',masks.shape)

#     bbox = np.concatenate([np.array(centers),np.array(dims),np.array(angles).reshape(-1,1)],1)
#     # print('bbox',bbox.shape)
#     bbox = tf.convert_to_tensor(bbox.astype(np.float32))
#     # print('bbox',bbox.shape,bbox.dtype)
#     # print('pc',pc.shape,pc.dtype)
    
#     # print('pc_in_box',pc_in_box)
    
#     num_pc_in_box = box_utils.compute_num_points_in_box_3d(points_tensor,bbox).numpy().astype(np.float32)
#     print('num_pc_in_box',num_pc_in_box)
    

#     new_centers = []
#     new_dims = []
#     new_angles = []
#     for c,d,a,n in zip(centers,dims,angles,num_pc_in_box):
#         if n>5:
#             new_centers.append(c)
#             new_dims.append(d)
#             new_angles.append(a)

#     centers = new_centers
#     dims = new_dims
#     angles = new_angles

#     bbox = np.concatenate([np.array(centers),np.array(dims),np.array(angles).reshape(-1,1)],1)
#     # print('bbox',bbox.shape)
#     bbox = tf.convert_to_tensor(bbox.astype(np.float32))
    
#     pc_in_box = box_utils.is_within_box_3d(points_tensor,bbox).numpy().astype(np.float32)
#     print('pc_in_box in',pc_in_box.shape)
    
#     center_repeat = np.array(centers)
#     center_repeat = np.expand_dims(center_repeat,0)
#     repeat = points_tensor.shape[0]
#     center_repeat = np.repeat(center_repeat,repeat,axis=0)
#     # print('center_repeat',center_repeat.shape)
    
#     points_tensor_repeat = np.expand_dims(points_tensor,1)
#     repeat = len(centers)
#     points_tensor_repeat = np.repeat(points_tensor_repeat,repeat,axis=1)
#     # print('points_tensor_repeat',points_tensor_repeat.shape)
#     sigmas = 0.25 # set to 0.25 for pedestrains

#     distances =( ((points_tensor_repeat-center_repeat)**2).sum(-1)**0.5) * sigmas
#     print('distances',distances,distances.shape)

    

    

#     def gaussian(x, mu, sig):
#         return np.exp(-np.power(x - mu, 2.).sum(-1) / (2 * np.power(sig, 2.)))

#     # def gaussian(x, mu, sig):
#     #     import math
#     #     return 1./(math.sqrt(sig**math.pi))*np.exp(-sig*np.power((x - mu).sum(-1), 2.)**0.5)


#     def gaussian(x,mu,sig):

#         return (1/((np.pi**0.5)*sig))*np.exp(-(((x-mu)**2).sum(-1))**0.5/(2*sig**2))



#     # def gaussian(x, mu, sig):
#     #     return np.exp(-(np.power(x - mu, 2.).sum(-1))**0.5 / (2 * np.power(sig, 2.)))



#     dga = gaussian(points_tensor_repeat,center_repeat,sigmas)

#     distance_in_bbox = dga*pc_in_box
#     # mean = center_repeat
#     # standard_deviation = sigmas
#     # dga = norm(mean, standard_deviation).pdf(points_tensor_repeat)#.mean(-1)
#     print('dga',dga)

#     print('dga',dga,dga.shape)
#     print('dga max',np.max(dga))
#     distances = dga

    
#     # print('distance_in_bbox',distance_in_bbox,distance_in_bbox.shape)


#     # sij_maxj =( np.max(distances,1)*range_image_mask.numpy().reshape(-1))/(np.max(distance_in_bbox)*range_image_mask.numpy().reshape(-1)+1e-8)
#     # sij_maxj = np.max(pc_in_box,1)*sij_maxj
#     # sij_maxj =( np.max(distance_in_bbox,1)*range_image_mask.numpy().reshape(-1))/(np.max(distance_in_bbox)*range_image_mask.numpy().reshape(-1)+1e-8)
#     raw_hm = np.zeros((len(points_tensor)), dtype=np.float32)

#     # for j in range(len(centers)):
#     #     distance_in_bbox[j]
#     max_bij = np.expand_dims((np.max(distance_in_bbox,1)+1e-8),1)
#     raw_hm = distance_in_bbox/max_bij
    
#     raw_hm_centers = np.argmax(raw_hm,0)
#     print('raw_hm',raw_hm.shape)
#     print('raw_hm_centers',raw_hm_centers,raw_hm_centers.shape)
#     cx,cy = np.unravel_index(raw_hm_centers, range_image_size)
#     print('cx,cy raw',cx,cy)
#     #exit()
#     # print('distance_in_bbox/max_bij',raw_hm.shape)
    
#     raw_hm_channel = torch.FloatTensor(raw_hm).permute(1,0).view(1,len(centers),range_image_size[0],range_image_size[1])

#     raw_hm = np.max(raw_hm,1).reshape(-1,range_image_size[0],range_image_size[1])




#     # ###########

#     # print('distances',distances.shape)

#     # # maxj_sij = np.max(dga,1)

#     # max_iinj_sij = np.max(dga*pc_in_box,1) 

#     # print('max_iinj_sij',max_iinj_sij.shape)


#     # raw_hm = maxj_sij/(max_iinj_sij+1e-50)
#     # raw_hm = raw_hm.reshape(-1,range_image_size[0],range_image_size[1])
#     # print('raw_hm',raw_hm.shape,np.max(raw_hm),np.argmax(raw_hm))

#     # # exit()

#     # #######


#     ###########

#     print('distances',distances.shape)

#     # raw_hm = dga/ np.expand_dims((np.max(dga*pc_in_box,0)+1e-8),1)



#     a = np.max(dga*pc_in_box,0)+1e-8
#     print('a',a.shape,a)
#     # raw_hm = dga /  np.expand_dims((a+1e-8),0)
#     raw_hm = dga*pc_in_box/a

#     # raw_hm = raw_hm*pc_in_box

#     # raw_hm = pc_in_box
#     # raw_hm = dga/a
#     #raw_hm = dga
#     print('dga/a',(dga/a).shape)
#     raw_hm_channel = torch.FloatTensor(raw_hm).permute(1,0).view(1,len(centers),range_image_size[0],range_image_size[1])
#     print('raw_hm_channel',raw_hm_channel.size())
    
#     raw_hm_centers = np.argmax(raw_hm,0)
#     print('raw_hm',raw_hm.shape)
#     print('raw_hm_centers',raw_hm_centers,raw_hm_centers.shape)
#     cx,cy = np.unravel_index(raw_hm_centers, range_image_size)
#     print('cx,cy raw',cx,cy)
#     #exit()

#     raw_hm = np.max(raw_hm,1)

#     #raw_hm = raw_hm[:,14]
    
#     # raw_hm = raw_hm*pc_in_box
    

    



#     raw_hm = raw_hm.reshape(-1,range_image_size[0],range_image_size[1])


#     idx = np.argmax(raw_hm)
#     print('dga[idx]',dga[idx])
#     print('distance_in_bbox[idx]',distance_in_bbox[idx])
#     tmp = np.expand_dims((np.max(dga*pc_in_box,1)+1e-8),1)
#     print('distance_in_bbox[idx]',tmp)

#     sorted_raw_hm = np.sort(raw_hm.flatten())
#     print('sorted_raw_hm',sorted_raw_hm)
#     for shm in sorted_raw_hm[-100:]:
#         print('shm',shm)

#     print('raw_hm',raw_hm.shape,np.max(raw_hm),np.argmax(raw_hm))
#     if np.max(raw_hm)>1:
#         print('np.max(raw_hm)',np.max(raw_hm))
#         print('np.argmax(raw_hm)',np.argmax(raw_hm))
#         exit()

#     # exit()

#     #######



#     sorted_raw_hm = np.sort(np.max(raw_hm[0],1))
#     # print('sorted_raw_hm',sorted_raw_hm)
#     # for shm in sorted_raw_hm[0][-100:]:
#     #     print('shm',shm)



    

#     raw_hm = torch.FloatTensor(raw_hm).view(1,1,range_image_size[0],range_image_size[1])
#     coords = torch.FloatTensor(coords).permute(0,3,1,2).view(1,3,range_image_size[0],range_image_size[1])
#     masks = torch.FloatTensor(masks).view(1,1,range_image_size[0],range_image_size[1])
    
    
#     cv2.imwrite('raw_hm.png',raw_hm.view(range_image_size[0],range_image_size[1]).detach().numpy()*255)

#     hm = nn.functional.max_pool2d(raw_hm,2,2)

#     sds = SmartDownSampling(32,32,kernel_size=output_stride,stride=output_stride)


#     ds_hm,ds_corrd,ds_mask = sds(raw_hm_channel,coords,masks)


#     ds_hm = ds_hm.view(-1,range_image_size[0]//output_stride*range_image_size[1]//output_stride).detach().numpy()
#     ds_hm = ds_hm.transpose(1,0)
#     print('ds_hm',ds_hm.shape)
#     # hm = ds_hm
    


#     ds_hm_centers = ds_hm#.detach().cpu().numpy()
#     ds_centers = np.argmax(ds_hm_centers,0)
#     tx,ty = np.unravel_index(ds_centers, (32,1325))
#     print('ds_centers',ds_centers.shape)
#     print('tx,ty',tx,ty)
    
#     rv = coords[:,2:,:,:].view(1,1,range_image_size[0],range_image_size[1])

#     print('rv',rv,torch.max(rv))
#     raw_rv =rv.view(range_image_size[0],range_image_size[1]).detach().numpy()
#     cv2.imwrite('raw_rv.png',raw_rv*10)
    
#     ds_rv,ds_corrd,ds_mask = sds(rv,coords,masks)

    
#     ds_rv = ds_rv.view(range_image_size[0]//2,range_image_size[1]//2).detach().numpy()
#     cv2.imwrite('ds_rv.png',ds_rv*10)

#     # print('hm',hm.shape)
#     hm = hm.view(range_image_size[0]//2,range_image_size[1]//2).detach().numpy()

#     # print('hm',hm.shape)
#     # ds_hm_centers = np.argmax(ds_hm,0)
#     # hm = np.max(ds_hm,-1).reshape(range_image_size[0]//2,range_image_size[1]//2)
#     # cv2.imwrite('ds_hm.png',np.max(ds_hm,-1).reshape(range_image_size[0]//2,range_image_size[1]//2)*255)
    
#     # print('ds_hm_centers',ds_hm_centers.shape)
#     # # print('raw_hm',np.sort(raw_hm),raw_hm.shape)
#     hm = cv2.resize(raw_hm.detach().cpu().numpy().reshape(range_image_size[0],range_image_size[1]), (range_image_size[1]//output_stride,range_image_size[0]//output_stride),interpolation = cv2.INTER_AREA).astype(np.float32)
#     hm_centers = np.zeros((len(centers),2),dtype=np.int)
#     # hm_centers[:,0] = cx//output_stride
#     # hm_centers[:,1] = cy//output_stride

#     # tx,ty = np.unravel_index(ds_hm_centers, hm.shape)
#     # print('tx,ty',tx.shape,ty.shape)

#     hm_centers[:,0] = tx
#     hm_centers[:,1] = ty
#     # print('hm',np.sort(hm.flatten()),hm.shape)
#     #hm = np.max(ds_hm,1).reshape(1,32,1325)
    


#     return hm,hm_centers,cx,cy,raw_hm.detach().cpu().numpy(),raw_hm_centers,raw_hm_channel.detach().cpu().numpy(),dga,pc_in_box,centers,dims,angles
    





def get_heatmap3d(points_tensor,centers,dims,angles,range_image_size,coords=None,masks=None):
    # print('len(centers)',len(centers))

    # print('np.array(centers)',np.array(centers).shape)
    # print('np.array(dims)',np.array(dims).shape)
    # print('np.array(angles)',np.array(angles).shape)

    if len(masks.shape)==3:
        masks = np.expand_dims(masks,0)

    print('coords',coords.shape)
    print('masks',masks.shape)

    bbox = np.concatenate([np.array(centers),np.array(dims),np.array(angles).reshape(-1,1)],1)
    # print('bbox',bbox.shape)
    bbox = tf.convert_to_tensor(bbox.astype(np.float32))
    # print('bbox',bbox.shape,bbox.dtype)
    # print('pc',pc.shape,pc.dtype)
    
    # print('pc_in_box',pc_in_box)


    points_tensor = points_tensor.reshape(range_image_size[0],range_image_size[1],3)
    points_tensor = torch.FloatTensor(points_tensor).permute(2,0,1).reshape(1,3,range_image_size[0],range_image_size[1])

    coords = torch.FloatTensor(coords).permute(0,3,1,2).view(1,3,range_image_size[0],range_image_size[1])
    masks = torch.FloatTensor(masks).view(1,1,range_image_size[0],range_image_size[1])
    sds = SmartDownSampling(32,32,kernel_size=output_stride,stride=output_stride)
    points_tensor,ds_corrd,ds_mask = sds(points_tensor,coords,masks)
    points_tensor = points_tensor.reshape(3,-1).permute(1,0).detach().cpu().numpy()
    


    num_pc_in_box = box_utils.compute_num_points_in_box_3d(points_tensor,bbox).numpy().astype(np.float32)
    print('num_pc_in_box',num_pc_in_box)
    

    new_centers = []
    new_dims = []
    new_angles = []
    for c,d,a,n in zip(centers,dims,angles,num_pc_in_box):
        if n>5:
            new_centers.append(c)
            new_dims.append(d)
            new_angles.append(a)

    centers = new_centers
    dims = new_dims
    angles = new_angles

    bbox = np.concatenate([np.array(centers),np.array(dims),np.array(angles).reshape(-1,1)],1)
    # print('bbox',bbox.shape)
    bbox = tf.convert_to_tensor(bbox.astype(np.float32))
    
    pc_in_box = box_utils.is_within_box_3d(points_tensor,bbox).numpy().astype(np.float32)
    print('pc_in_box in',pc_in_box.shape)
    
    center_repeat = np.array(centers)
    center_repeat = np.expand_dims(center_repeat,0)
    repeat = points_tensor.shape[0]
    center_repeat = np.repeat(center_repeat,repeat,axis=0)
    # print('center_repeat',center_repeat.shape)
    
    points_tensor_repeat = np.expand_dims(points_tensor,1)
    repeat = len(centers)
    points_tensor_repeat = np.repeat(points_tensor_repeat,repeat,axis=1)
    # print('points_tensor_repeat',points_tensor_repeat.shape)
    sigmas = 0.25 # set to 0.25 for pedestrains

    distances =( ((points_tensor_repeat-center_repeat)**2).sum(-1)**0.5) * sigmas
    print('distances',distances,distances.shape)

    

    

    def gaussian(x, mu, sig):
        return np.exp(-np.power(x - mu, 2.).sum(-1) / (2 * np.power(sig, 2.)))

    # def gaussian(x, mu, sig):
    #     import math
    #     return 1./(math.sqrt(sig**math.pi))*np.exp(-sig*np.power((x - mu).sum(-1), 2.)**0.5)


    def gaussian(x,mu,sig):

        return (1/((np.pi**0.5)*sig))*np.exp(-(((x-mu)**2).sum(-1))**0.5/(2*sig**2))



    # def gaussian(x, mu, sig):
    #     return np.exp(-(np.power(x - mu, 2.).sum(-1))**0.5 / (2 * np.power(sig, 2.)))



    dga = gaussian(points_tensor_repeat,center_repeat,sigmas)

    distance_in_bbox = dga*pc_in_box
    # mean = center_repeat
    # standard_deviation = sigmas
    # dga = norm(mean, standard_deviation).pdf(points_tensor_repeat)#.mean(-1)
    print('dga',dga)

    print('dga',dga,dga.shape)
    print('dga max',np.max(dga))
    distances = dga

    
    # # print('distance_in_bbox',distance_in_bbox,distance_in_bbox.shape)


    # # sij_maxj =( np.max(distances,1)*range_image_mask.numpy().reshape(-1))/(np.max(distance_in_bbox)*range_image_mask.numpy().reshape(-1)+1e-8)
    # # sij_maxj = np.max(pc_in_box,1)*sij_maxj
    # # sij_maxj =( np.max(distance_in_bbox,1)*range_image_mask.numpy().reshape(-1))/(np.max(distance_in_bbox)*range_image_mask.numpy().reshape(-1)+1e-8)
    # raw_hm = np.zeros((len(points_tensor)), dtype=np.float32)

    # # for j in range(len(centers)):
    # #     distance_in_bbox[j]
    # max_bij = np.expand_dims((np.max(distance_in_bbox,1)+1e-8),1)
    # raw_hm = distance_in_bbox/max_bij
    
    # raw_hm_centers = np.argmax(raw_hm,0)
    # print('raw_hm',raw_hm.shape)
    # print('raw_hm_centers',raw_hm_centers,raw_hm_centers.shape)
    # cx,cy = np.unravel_index(raw_hm_centers, range_image_size)
    # print('cx,cy raw',cx,cy)
    # #exit()
    # # print('distance_in_bbox/max_bij',raw_hm.shape)
    
    # raw_hm_channel = torch.FloatTensor(raw_hm).permute(1,0).view(1,len(centers),range_image_size[0]//output_stride,range_image_size[1]//output_stride)

    # raw_hm = np.max(raw_hm,1).reshape(-1,range_image_size[0]//output_stride,range_image_size[1]//output_stride)




    # ###########

    # print('distances',distances.shape)

    # # maxj_sij = np.max(dga,1)

    # max_iinj_sij = np.max(dga*pc_in_box,1) 

    # print('max_iinj_sij',max_iinj_sij.shape)


    # raw_hm = maxj_sij/(max_iinj_sij+1e-50)
    # raw_hm = raw_hm.reshape(-1,range_image_size[0],range_image_size[1])
    # print('raw_hm',raw_hm.shape,np.max(raw_hm),np.argmax(raw_hm))

    # # exit()

    # #######


    ###########

    print('distances',distances.shape)

    # raw_hm = dga/ np.expand_dims((np.max(dga*pc_in_box,0)+1e-8),1)



    a = np.max(dga*pc_in_box,0)+1e-8
    print('a',a.shape,a)
    # raw_hm = dga /  np.expand_dims((a+1e-8),0)
    raw_hm = dga*pc_in_box/a

    # raw_hm = raw_hm*pc_in_box

    # raw_hm = pc_in_box
    # raw_hm = dga/a
    #raw_hm = dga
    print('dga/a',(dga/a).shape)
    raw_hm_channel = torch.FloatTensor(raw_hm).permute(1,0).view(1,len(centers),range_image_size[0]//output_stride,range_image_size[1]//output_stride)
    print('raw_hm_channel',raw_hm_channel.size())
        
    raw_hm_centers = np.argmax(raw_hm,0)
    print('raw_hm',raw_hm.shape)
    print('raw_hm_centers',raw_hm_centers,raw_hm_centers.shape)
    cx,cy = np.unravel_index(raw_hm_centers, (range_image_size[0]//2,range_image_size[1]//2))
    print('cx,cy raw',cx,cy)
    #exit()

    raw_hm = np.max(raw_hm,1)

    #raw_hm = raw_hm[:,14]
    
    # raw_hm = raw_hm*pc_in_box
    

    



    raw_hm = raw_hm.reshape(-1,range_image_size[0]//output_stride,range_image_size[1]//output_stride)


    idx = np.argmax(raw_hm)
    print('dga[idx]',dga[idx])
    print('distance_in_bbox[idx]',distance_in_bbox[idx])
    tmp = np.expand_dims((np.max(dga*pc_in_box,1)+1e-8),1)
    print('distance_in_bbox[idx]',tmp)

    sorted_raw_hm = np.sort(raw_hm.flatten())
    print('sorted_raw_hm',sorted_raw_hm)
    for shm in sorted_raw_hm[-100:]:
        print('shm',shm)

    print('raw_hm',raw_hm.shape,np.max(raw_hm),np.argmax(raw_hm))
    if np.max(raw_hm)>1:
        print('np.max(raw_hm)',np.max(raw_hm))
        print('np.argmax(raw_hm)',np.argmax(raw_hm))
        exit()

    # exit()

    #######



    sorted_raw_hm = np.sort(np.max(raw_hm[0],1))
    # print('sorted_raw_hm',sorted_raw_hm)
    # for shm in sorted_raw_hm[0][-100:]:
    #     print('shm',shm)



    

    raw_hm = torch.FloatTensor(raw_hm).view(1,1,range_image_size[0]//output_stride,range_image_size[1]//output_stride)

    hm = raw_hm.detach().cpu().numpy()

    hm_centers = np.zeros((len(centers),2),dtype=np.int)
    # hm_centers[:,0] = cx//output_stride
    # hm_centers[:,1] = cy//output_stride

    # tx,ty = np.unravel_index(ds_hm_centers, hm.shape)
    # print('tx,ty',tx.shape,ty.shape)
    
    hm_centers[:,0] = cx
    hm_centers[:,1] = cy
    # print('hm',np.sort(hm.flatten()),hm.shape)
    #hm = np.max(ds_hm,1).reshape(1,32,1325)
    

    print('finish heatmap')
    return hm,hm_centers,cx,cy,raw_hm.detach().cpu().numpy(),raw_hm_centers,raw_hm_channel.detach().cpu().numpy(),dga,pc_in_box,centers,dims,angles,points_tensor
    
        # argmax = np.argmax(raw_hm)

def data_generator(frames_path,ri_index=0,max_objs=100):
    dataset = tf.data.TFRecordDataset(frames_path, compression_type='')
    gen = wrapper(enumerate(dataset))
    output_stride = 2
    for idx,data in gen:
        try:
            if idx!=26:
                continue
            frame = open_dataset.Frame()
            frame.ParseFromString(bytearray(data.numpy()))
            (range_images, camera_projections, range_image_top_pose) = (
              frame_utils.parse_range_image_and_camera_projection(frame))
            
            
            calibrations = sorted(frame.context.laser_calibrations, key=lambda c: c.name)
            points = []
            cp_points = []
            points_NLZ = []
            points_intensity = []
            points_elongation = []
            
            frame_pose = tf.convert_to_tensor(np.reshape(np.array(frame.pose.transform), [4, 4]))
            # [H, W, 6]
            range_image_top_pose_tensor = tf.reshape(
                tf.convert_to_tensor(range_image_top_pose.data), range_image_top_pose.shape.dims
            )
            # [H, W, 3, 3]
            range_image_top_pose_tensor_rotation = transform_utils.get_rotation_matrix(
                range_image_top_pose_tensor[..., 0], range_image_top_pose_tensor[..., 1],
                range_image_top_pose_tensor[..., 2])
            range_image_top_pose_tensor_translation = range_image_top_pose_tensor[..., 3:]
            range_image_top_pose_tensor = transform_utils.get_transform(
                range_image_top_pose_tensor_rotation,
                range_image_top_pose_tensor_translation)

            for c in calibrations:
                range_image = range_images[c.name][ri_index]
                if len(c.beam_inclinations) == 0:  # pylint: disable=g-explicit-length-test
                    beam_inclinations = range_image_utils.compute_inclination(
                        tf.constant([c.beam_inclination_min, c.beam_inclination_max]),
                        height=range_image.shape.dims[0])
                else:
                    beam_inclinations = tf.constant(c.beam_inclinations)

                beam_inclinations = tf.reverse(beam_inclinations, axis=[-1])
                extrinsic = np.reshape(np.array(c.extrinsic.transform), [4, 4])

                range_image_tensor = tf.reshape(
                    tf.convert_to_tensor(range_image.data), range_image.shape.dims)

                pixel_pose_local = None
                frame_pose_local = None
                if c.name == dataset_pb2.LaserName.TOP:
                    pixel_pose_local = range_image_top_pose_tensor
                    pixel_pose_local = tf.expand_dims(pixel_pose_local, axis=0)
                    frame_pose_local = tf.expand_dims(frame_pose, axis=0)
                range_image_mask = range_image_tensor[..., 0] > 0
                range_image_NLZ = range_image_tensor[..., 3]
                range_image_intensity = range_image_tensor[..., 1]
                range_image_elongation = range_image_tensor[..., 2]

                # print('tf.expand_dims(range_image_tensor[..., 0], axis=0)',tf.expand_dims(range_image_tensor[..., 0], axis=0).shape)
                range_image_polar = range_image_utils.compute_range_image_polar(
                    tf.expand_dims(range_image_tensor[..., 0], axis=0),
                    tf.expand_dims(extrinsic, axis=0),
                    tf.expand_dims(tf.convert_to_tensor(beam_inclinations), axis=0),
                    )
                range_image_cartesian = range_image_utils.compute_range_image_cartesian(
                range_image_polar, tf.expand_dims(extrinsic, axis=0),)
                
                # print('range_image_cartesian',range_image_cartesian)
                # print('range_image_cartesian',range_image_cartesian.shape)
                # print('range_image_mask',range_image_mask.shape)
                # print('tf.compat.v1.where(range_image_mask)',tf.compat.v1.where(range_image_mask).shape)
                B,h,w,_ = range_image_cartesian.shape
                # print('range_image_cartesian')
                # pc = range_image_cartesian.numpy().reshape(B,-1,3)
                # pc = tf.convert_to_tensor(pc.reshape(-1,3).astype(np.float32))

                # points_tensor = tf.gather_nd(tf.reshape(range_image_cartesian,(64,2650,3)),
                #                  tf.compat.v1.where(range_image_mask))

                points_tensor = range_image_cartesian.numpy().reshape(-1,3)
                # print('points_tensor',points_tensor)
                # print('points_tensor',points_tensor.shape)

                range_image_size = range_image_polar.shape[1:3]
                if range_image_size[0]!=64:
                    continue

                centers,dims,angles,class_inds = get_bb_waymo(frame)
                if len(centers)==0:
                    continue


                ind = np.zeros((max_objs), dtype=np.int64)
                mask = np.zeros((max_objs), dtype=np.uint8)
                cat = np.zeros((max_objs), dtype=np.int64)
                
                reg_box = np.zeros((max_objs,8), dtype=np.float32)
                
                
                num_objs = min(len(centers), max_objs) 


                '''

                get class heatmap
                '''

                
                hm = np.zeros((n_classes, range_image_size[0]//output_stride, range_image_size[1]//output_stride),
                                      dtype=np.float32)
                
                reg_target = np.zeros((7,range_image_size[0]//output_stride, range_image_size[1]//output_stride),
                                      dtype=np.float32)
                



                
                print('range_image_tensor[..., 0]',range_image_tensor[..., 0].numpy())
                print('range_image_polar[..., 2]',range_image_polar[..., 2].numpy())
                cls_hm,cls_hm_centers,cxs,cys,raw_hm,raw_hm_centers,raw_hm_channel,dga,pc_in_box,centers,dims,angles,ds_points_tensor= get_heatmap3d(points_tensor,centers,dims,angles,range_image_size,range_image_polar.numpy(),range_image_mask.numpy())

                
                hm[0] = cls_hm

                # print('cls_hm_centers',cls_hm_centers)
                for new_idx,(center,dim,angle,class_ind,cx,cy) in enumerate(zip(centers,dims,angles,class_inds,cxs,cys)):
                    tx = cls_hm_centers[new_idx][0]
                    ty = cls_hm_centers[new_idx][1]

                    print('tx,ty',(tx,ty))                    
                    print('raw_hm',raw_hm.shape)    
                    ind[new_idx] = tx * range_image_size[1]//output_stride + ty
                    mask[new_idx] = 1
                    cat[new_idx] = class_ind
                    #hm[class_ind][tx,ty]=1
                    # pc_in_box_bug_pix = pc_in_box[:,new_idx].reshape(-1,range_image_size[0]//2,range_image_size[1]//output_stride).reshape(-1)

                    # if mask[new_idx]==1 and ind[new_idx]==0:
                    # # if True:
                    #     print('idx',idx)
                    #     print('frames',frames_path)
                    #     print('new_index',new_idx,len(centers),len(cxs))
                    #     print('mask',mask,'ind',ind)
                    #     print('tx,ty',tx,ty)
                    #     print('cx,cy',cxs,cys)
                    #     print('dim',dim)
                    #     print('center',center)
                    #     print('raw_hm_centers',raw_hm_centers)
                        
                    #     tmp = raw_hm.reshape(64,2650)
                    #     tmp = tmp[cx,cy]
                    #     print('raw_hm center',tmp)
                    #     bug_pix= raw_hm_channel.reshape(-1,64,2650)[new_idx].reshape(-1)#[cy,cy]
                    #     print('bug_pix',np.sort(bug_pix))

                    #     print('dga',dga.shape)

                    #     dga_bug_pix= dga[:,new_idx].reshape(-1,64,2650).reshape(-1)#[cy,cy]
                    #     print('dga_bug_pix',np.sort(dga_bug_pix))
                    #     print('dga',dga.shape)

                    #     dga_in_box_bug_pix=( dga*pc_in_box)[:,new_idx].reshape(-1,64,2650).reshape(-1)#[cy,cy]
                    #     print('dga_in_box_bug_pix',np.sort(dga_in_box_bug_pix))
                    #     print('dga_in_box',( dga*pc_in_box).shape)

                    #     a = np.max(dga*pc_in_box,0)+1e-32

                    #     dga_box_a = dga*pc_in_box/a
                    #     dga_box_a_bug_pix = dga_box_a[:,new_idx].reshape(-1,64,2650).reshape(-1)
                    #     print('dga_in_box_bug_pix',np.sort(dga_in_box_bug_pix))
                    #     print('dga_in_box',( dga*pc_in_box).shape)


                    #     pc_in_box_bug_pix = pc_in_box[:,new_idx].reshape(-1,64,2650).reshape(-1)
                    #     print('pc_in_box_bug_pix',np.sort(pc_in_box_bug_pix))
                    #     print('pc_in_box',( pc_in_box).shape)



                    #     print('hm[class_ind].flatten()[ind[new_idx]]',hm[class_ind].flatten()[ind[new_idx]],mask[new_idx])
                    #     resized_hm = cv2.resize(hm[0]*255, (2650,64), )
                    #     cv2.imwrite('gt_overlaped_rv.png',range_image_tensor.numpy()[:,:,0]+resized_hm)



                    #     resized_rv = cv2.resize(range_image_tensor.numpy()[:,:,0],(1325,32))
                    #     cv2.imwrite('gt_overlaped_rv_resized.png',resized_rv+hm[0]*255)
                        
                    #     cv2.imwrite('gt_overlaped_raw_hm.png',range_image_tensor.numpy()[:,:,0]+raw_hm[0][0]*255)

                       
                        
                    #     gt_hm = np.zeros(hm[0].flatten().shape)
                    #     for idx,(i,m) in enumerate(zip(ind,mask)):
                    #         gt_hm[i] = 255
                    #         print('hm[class_ind].flatten()[ind[new_idx]]',hm[0].flatten()[i],i,m)

                    #     gt_hm = gt_hm.reshape(32,1325)
                    #     resized_gt_hm = cv2.resize(gt_hm*255, (2650,64), )
                        
                        
                        
                    #     cv2.imwrite('gt_overlaped_gt_hm.png',range_image_tensor.numpy()[:,:,0]+resized_gt_hm)
                    #     exit()
                    
                    
                    # cx,cy = np.unravel_index(ind[new_idx], cls_hm.shape)

                    l, w, h= dim
                    x,y,z = center
                    
                    sample = np.array(center).reshape(1,1,3)
                    
                    center  = tf.convert_to_tensor(sample)

                    relative_displacement = np.squeeze((ds_points_tensor[ind[new_idx]]-np.squeeze(center.numpy(),0)),0)
                    
                    
                    
                    reg_box[new_idx] = np.array([relative_displacement[0],relative_displacement[1],relative_displacement[2],np.log(l), np.log(w), np.log(h),np.cos(angle),np.sin(angle)])
                    print('new_idx',new_idx)
                    print('relative_displacement',relative_displacement)
                    # print('reg_box[new_idx]',reg_box[new_idx])
                    print('hm[class_ind].flatten()[ind[new_idx]]',hm[class_ind].flatten()[ind[new_idx]])

                    if hm[class_ind].flatten()[ind[new_idx]]<0.9 and mask[new_idx]!=1:
                        print('hm[class_ind].flatten()[ind[new_idx]]',hm[class_ind].flatten()[ind[new_idx]],mask[new_idx])
                        exit()
                        
                        
                yield range_image_tensor.numpy(),range_image_cartesian.numpy(),range_image_polar.numpy(),range_image_mask.numpy(),hm,reg_target,ind,mask,cat,reg_box,raw_hm,pc_in_box,centers,dims,angles
        except Exception as e:
            print('Exception in iters',e)
            continue


def get_bb_waymo(frame):

    obj_name, difficulty, dimensions, locations, heading_angles = [], [], [], [], []
    tracking_difficulty, speeds, accelerations, obj_ids = [], [], [], []
    num_points_in_gt = []
    laser_labels = frame.laser_labels

    centers = []
    dims = []
    angles = []
    class_inds = []
    for i in range(len(laser_labels)):

        box = laser_labels[i].box
        class_ind = laser_labels[i].type
        loc = [box.center_x, box.center_y, box.center_z]
        heading_angles.append(box.heading)
        obj_name.append(WAYMO_CLASSES[class_ind])
        if WAYMO_CLASSES[class_ind] not in use_classes:
            continue
        else:
            class_ind = use_classes.index(WAYMO_CLASSES[class_ind])

        num_lidar_points_in_box = laser_labels[i].num_lidar_points_in_box
        print('i',i,num_lidar_points_in_box)
        if num_lidar_points_in_box<=5:
            # print('i',i,num_lidar_points_in_box)
            continue


        # print('WAYMO_CLASSES[class_ind]',WAYMO_CLASSES[class_ind])
        difficulty.append(laser_labels[i].detection_difficulty_level)
        tracking_difficulty.append(laser_labels[i].tracking_difficulty_level)
        print('difficulty',i,laser_labels[i].tracking_difficulty_level)
        dimensions.append([box.length, box.width, box.height])  # lwh in unified coordinate of OpenPCDet
        locations.append(loc)
        obj_ids.append(laser_labels[i].id)
        num_points_in_gt.append(laser_labels[i].num_lidar_points_in_box)
        center = [box.center_x, box.center_y, box.center_z]
        
        # if box.center_x>x_range[1] or box.center_x<x_range[0]:
        #     print('dorop x',center)
        #     continue
            
        # if box.center_y>y_range[1] or box.center_y<y_range[0]:
        #     print('dorop y',center)
        #     continue
            
        # if box.center_z>z_range[1] or box.center_z<z_range[0]:
        #     print('dorop z',center)

        #     continue


        
        dim = [box.length, box.width, box.height]
        angle = box.heading
        # break 
        # print('center',center)
        # print('box',box)
        centers.append(center)
        dims.append(dim)
        angles.append(angle)
        class_inds.append(class_ind)



    return centers,dims,angles,class_inds



def gaussian2D(shape, sigma=1):
    m, n = [(ss - 1.) / 2. for ss in shape]
    y, x = np.ogrid[-m:m+1,-n:n+1]

    h = np.exp(-(x * x + y * y) / (2 * sigma * sigma))
    h[h < np.finfo(h.dtype).eps * h.max()] = 0
    return h



def gaussian_radius(det_size, min_overlap=0.5):
    height, width = det_size

    a1  = 1
    b1  = (height + width)
    c1  = width * height * (1 - min_overlap) / (1 + min_overlap)
    sq1 = np.sqrt(b1 ** 2 - 4 * a1 * c1)
    r1  = (b1 + sq1) / 2

    a2  = 4
    b2  = 2 * (height + width)
    c2  = (1 - min_overlap) * width * height
    sq2 = np.sqrt(b2 ** 2 - 4 * a2 * c2)
    r2  = (b2 + sq2) / 2

    a3  = 4 * min_overlap
    b3  = -2 * min_overlap * (height + width)
    c3  = (min_overlap - 1) * width * height
    sq3 = np.sqrt(b3 ** 2 - 4 * a3 * c3)
    r3  = (b3 + sq3) / 2
    return min(r1, r2, r3)


def draw_umich_gaussian(heatmap, center, radius, k=1):
    diameter = 2 * radius + 1
    gaussian = gaussian2D((diameter, diameter), sigma=diameter / 6)

    x, y = int(center[0]), int(center[1])

    height, width = heatmap.shape[0:2]

    left, right = min(x, radius), min(width - x, radius + 1)
    top, bottom = min(y, radius), min(height - y, radius + 1)


    masked_heatmap  = heatmap[y - top:y + bottom, x - left:x + right]
    masked_gaussian = gaussian[radius - top:radius + bottom, radius - left:radius + right]
    if min(masked_gaussian.shape) > 0 and min(masked_heatmap.shape) > 0: # TODO debug
        np.maximum(masked_heatmap, masked_gaussian * k, out=masked_heatmap)
    return heatmap



def _gather_feat(feat, ind, mask=None):
    # dim = B,M,C
    dim  = feat.size(2)

    ind  = ind.unsqueeze(2).expand(ind.size(0), ind.size(1), dim)
    
    feat = feat.gather(1, ind)
    if mask is not None:
        mask = mask.unsqueeze(2).expand_as(feat)
        feat = feat[mask]
        feat = feat.view(-1, dim)
    return feat

def _transpose_and_gather_feat(feat, ind):


    # B x C x H x W
    feat = feat.permute(0, 2, 3, 1).contiguous()
    # B x H x W x C
    feat = feat.view(feat.size(0), -1, feat.size(3))
    # M = H x W
    # B x M x C

    feat = _gather_feat(feat, ind)
    return feat


import cv2
import random
random.seed(1024)
from config import  max_objs
if __name__ == '__main__':
    
    save_path = '/workspace/input_ped'
    
    records = glob('/workspace/raw_data/raw_data/*.tfrecord')
    recors = glob('/workspace/raw_data/raw_data/segment-4690718861228194910_1980_000_2000_000_with_camera_labels.tfrecord')
    
    train_size = int(len(records)*0.8)
    
    train_records = records[:train_size]
    test_records = records[train_size:]
    
    
    count=0
    limit = 8000
    for frame_path in tqdm(train_records):
        iters = data_generator(frame_path,max_objs=max_objs)
        frame_name = frame_path.replace('.tfrecord','').split('/')[-1]
        print(frame_name)
        for i,(range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,reg_target,ind,mask,cat,reg_box,raw_hm,pc_in_box_in,centers,dims,angles) in tqdm(enumerate(iters)): 
                

            n_gt_boxes = np.sum(mask)

            print('range_image_cartesian',range_image_cartesian.shape)
            print('range_image_polar',range_image_polar.shape)

            rv = torch.FloatTensor(range_image_cartesian).view(64,2650,3)
            coords = torch.FloatTensor(range_image_polar).view(64,2650,3)

            raw_rv = rv.permute(2,0,1).reshape(1,3,64,2650).to(device)
            coords = coords.permute(2,0,1).reshape(1,3,64,2650).to(device)
            masks = torch.FloatTensor(range_image_mask).reshape(1,1,64,2650).to(device)
            
            sds = SmartDownSampling(32,32,kernel_size=output_stride,stride=output_stride)
            
            
            print('raw_hm',raw_hm.shape)
            ds_rv,ds_corrd,ds_mask = sds(raw_rv,coords,masks)
            # raw_hm_tensor = torch.FloatTensor(raw_hm).to(device)
            # ds_hm,ds_corrd,ds_mask = sds(raw_hm_tensor,coords,masks)
            # print('ds_hm',ds_hm.shape)
            # hm[0] = ds_hm.reshape(32,1325).detach().cpu().numpy()
            
            
            ind = torch.LongTensor(ind).view(-1,100)
            print('ind shape 4',ind.shape)
            rv = rv.detach().cpu().numpy()
            print('rv shape 2',rv.shape)
            # # 通过opencv进行降维，得到32,1325,3
            # rv = cv2.resize(rv,(1325,32),interpolation=cv2.INTER_NEAREST)
            # rv = torch.FloatTensor(rv).permute(2,0,1).reshape(1,3,32,1325).to(device)
            rv = ds_rv
            print('rv shape 4',rv.shape)
            # 1，100,3
            match_points = _transpose_and_gather_feat(rv.view(1,-1,32,1325).to(device),ind.to(device))
            print('match_points',match_points.shape)
            
            reg_labels = reg_box.reshape(-1,8)
            reg_labels[:,3:6] = np.exp(reg_labels[:,3:6])
            tmp = match_points[0].detach().cpu().numpy() - reg_labels[:,:3]
            
            reg_labels[:,:3] =  tmp


            cos_tgt = reg_labels[:,6]
            sin_tgt = reg_labels[:,7]
            heading_tgt = np.arctan(sin_tgt/(cos_tgt+1e-8))
            reg_labels[:,6] = heading_tgt
            reg_labels = reg_labels[:n_gt_boxes,:7]


            reg_labels = torch.FloatTensor(reg_labels).to(device)
            
            print('reg_labels',reg_labels.shape)

            
            boxes = reg_labels.reshape(-1,7).detach().cpu().numpy()
            points = raw_rv.reshape(3,-1).permute(1,0).detach().cpu().numpy()
            match_points = match_points.reshape(-1,3).detach().cpu().numpy()
            
            
            # cls_mask = hm[0]>0.1
            # hm_rv = rv.reshape(3,-1).permute(1,0)[cls_mask.reshape(-1)].reshape(-1,3).detach().cpu().numpy()

            # cls_mask = raw_hm>0.0
            # hm_rv = raw_rv.reshape(3,-1).permute(1,0)[cls_mask.reshape(-1)].reshape(-1,3).detach().cpu().numpy()
            # print('hm_rv',hm_rv.shape)

            cls_mask = hm[0]>0.0
            hm_rv = rv.reshape(3,-1).permute(1,0)[cls_mask.reshape(-1)].reshape(-1,3).detach().cpu().numpy()
            print('hm_rv',hm_rv.shape)


            points_tensor = raw_rv.reshape(3,-1).permute(1,0).detach().cpu().numpy()
            points_tensor = tf.convert_to_tensor(points_tensor.astype(np.float32))
            bb = reg_labels[:].detach().cpu().numpy()
            bb = tf.convert_to_tensor(bb.astype(np.float32))

            pc_in_box = box_utils.is_within_box_3d(points_tensor,bb).numpy().astype(np.float32)
            
            # diff_idx = np.argmax((pc_in_box_in.reshape(-1) - pc_in_box.reshape(-1))**2)
            # print('diff_idx',diff_idx,pc_in_box_in.reshape(-1)[diff_idx])
            
            print('pc_in_box out',pc_in_box.shape)
            pc_in_box_mask = pc_in_box.sum(-1)>0

            pc_in_bb = raw_rv.reshape(3,-1).permute(1,0)[pc_in_box_mask].reshape(-1,3).detach().cpu().numpy()


            


            # hm_rv =  hm_rv.reshape(-1,3).detach().cpu().numpy()
            fig = plt.figure(figsize=(16, 6))
            ax = fig.add_subplot(121)
            ax.set_aspect("equal")
            print('points[:, 1] max',points[:, 1].max())
            print('points[:, 1] min',points[:, 1].min())

            ax.plot(-points[:, 1], points[:, 0], 'b.', markersize=1)
            # ax.plot(-match_points[:, 1], match_points[:, 0], 'r.', markersize=1)
            ax.plot(-pc_in_bb[:, 1], pc_in_bb[:, 0], 'r.', markersize=0.8)
            # ax.plot(-mask_boxes[:, 1], mask_boxes[:, 0], 'g.', markersize=1)
            
            ax.plot(-hm_rv[:, 1], hm_rv[:, 0], 'y.', markersize=1)
            ax.plot(-boxes[:, 1], boxes[:, 0], 'g.', markersize=1)
            
            figure = plt.gcf() # get current figure

            ax = fig.add_subplot(122)
            ax.set_aspect("equal")
            print('points[:, 1] max',points[:, 1].max())
            print('points[:, 1] min',points[:, 1].min())

            ax.plot(-points[:, 1], points[:, 0], 'b.', markersize=1)
            # ax.plot(-match_points[:, 1], match_points[:, 0], 'r.', markersize=1)
            # ax.plot(-pc_in_bb[:, 1], pc_in_bb[:, 0], 'r.', markersize=0.8)
            ax.plot(-hm_rv[:, 1], hm_rv[:, 0], 'r.', markersize=0.8)

            ax.plot(-boxes[:, 1], boxes[:, 0], 'g.', markersize=1)
            centers = np.array(centers)
            ax.plot(-centers[:, 1], centers[:, 0], 'y.', markersize=1)
            # ax.plot(-mask_boxes[:, 1], mask_boxes[:, 0], 'g.', markersize=1)
            
            # ax.plot(-hm_rv[:, 1], hm_rv[:, 0], 'y.', markersize=1)
            # ax.plot(-boxes[:, 1], boxes[:, 0], 'g.', markersize=1)
            
            figure = plt.gcf() # get current figure
            plt.savefig('rv_to_pc.png',dpi=1000)
            #exit()




            resized_hm = cv2.resize(hm[0]*255, (2650,64), )
            cv2.imwrite('gt_overlaped_rv.png',range_image_tensor[:,:,0]+resized_hm)



            resized_rv = cv2.resize(range_image_tensor[:,:,0],(1325,32))
            cv2.imwrite('gt_overlaped_rv_resized.png',resized_rv+hm[0]*255)
            
            
            gt_hm = np.zeros(hm[0].flatten().shape)
            for idx,(i,m) in enumerate(zip(ind,mask)):
                gt_hm[i] = 255
                print('hm[class_ind].flatten()[ind[new_idx]]',hm[0].flatten()[i],i,m)

            gt_hm = gt_hm.reshape(32,1325)
            resized_gt_hm = cv2.resize(gt_hm*255, (2650,64), )



            cv2.imwrite('gt_overlaped_gt_hm.png',range_image_tensor[:,:,0]+resized_gt_hm)
            exit()            

            if count/5>limit:
                break
            try:
                if range_image_tensor.shape[0]!=64:
                    # print(range_image_tensor.shape)
                    continue
                if count%5!=0:
                    count+=1
                    continue    
                # print('(range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box)')
                # print((range_image_tensor.shape,range_image_cartesian.shape,range_image_polar.shape,range_image_mask.shape,hm.shape,ind.shape,mask.shape,cat.shape,reg_box.shape))
                # exit()
            except Exception as e:
                print('error',e,frame_path,i)




    # count=0
    # limit = 2000
    # for frame_path in tqdm(test_records):
    #     iters = data_generator(frame_path,max_objs=max_objs)
    #     frame_name = frame_path.replace('.tfrecord','').split('/')[-1]
    #     print(frame_name)
    #     for i,(range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,reg_target,ind,mask,cat,reg_box) in tqdm(enumerate(iters)): 
            


    #         resized_hm = cv2.resize(hm[0]*255, (2650,64), interpolation = cv2.INTER_AREA)
    #         cv2.imwrite('gt_overlaped_rv.png',range_image_tensor[:,:,0]+resized_hm)

            
    #         if count/5>limit:
    #             break
    #         try:
    #             if range_image_tensor.shape[0]!=64:
    #                 # print(range_image_tensor.shape)
    #                 continue
    #             if count%5!=0:
    #                 count+=1
    #                 continue    
    #             # print('(range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box)')
    #             # print((range_image_tensor.shape,range_image_cartesian.shape,range_image_polar.shape,range_image_mask.shape,hm.shape,ind.shape,mask.shape,cat.shape,reg_box.shape))
    #             exit()
    #         except Exception as e:
    #             print('error',e,frame_path,i)
