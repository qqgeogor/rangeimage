import os
import pickle
import numpy as np
from numpy import unravel_index
import pandas as pd

import torch
import torch.nn as nn
import tensorflow as tf
from waymo_open_dataset.utils import frame_utils, transform_utils, range_image_utils,box_utils
from waymo_open_dataset import dataset_pb2 as open_dataset
from waymo_open_dataset import dataset_pb2
from matplotlib import pylab as plt
import pandas as pd
import cv2
from glob import glob
from tqdm import tqdm
from src.models.edgeconv import SmartDownSampling

try:
    tf.enable_eager_execution()
except:
    pass

from config import *


output_stride = 2
min_overlap = 0.1
_min_radius = 2



def wrapper(gen):
    while True:
        try:
            yield next(gen)
        except StopIteration:
            break
        except Exception as e:
            # print('exception in wrapper',e)
            continue





def fwaymo(points_tensor,centers,dims,angles,range_image_size,coords=None,masks=None):

    if len(masks.shape)==3:
        masks = np.expand_dims(masks,0)


    bbox = np.concatenate([np.array(centers),np.array(dims),np.array(angles).reshape(-1,1)],1)
    bbox = tf.convert_to_tensor(bbox.astype(np.float32))


    points_tensor = points_tensor.reshape(range_image_size[0],range_image_size[1],3)
    points_tensor = torch.FloatTensor(points_tensor).permute(2,0,1).reshape(1,3,range_image_size[0],range_image_size[1])

    coords = torch.FloatTensor(coords).permute(0,3,1,2).view(1,3,range_image_size[0],range_image_size[1])
    masks = torch.FloatTensor(masks).view(1,1,range_image_size[0],range_image_size[1])
    sds = SmartDownSampling(32,32,kernel_size=output_stride,stride=output_stride)
    points_tensor,ds_corrd,ds_mask = sds(points_tensor,coords,masks)
    points_tensor = points_tensor.reshape(3,-1).permute(1,0).detach().cpu().numpy()
    


    num_pc_in_box = box_utils.compute_num_points_in_box_3d(points_tensor,bbox).numpy().astype(np.float32)

    new_centers = []
    new_dims = []
    new_angles = []
    for c,d,a,n in zip(centers,dims,angles,num_pc_in_box):
        if n>5:
            new_centers.append(c)
            new_dims.append(d)
            new_angles.append(a)

    centers = new_centers
    dims = new_dims
    angles = new_angles

    bbox = np.concatenate([np.array(centers),np.array(dims),np.array(angles).reshape(-1,1)],1)

    bbox = tf.convert_to_tensor(bbox.astype(np.float32))
    
    pc_in_box = box_utils.is_within_box_3d(points_tensor,bbox).numpy().astype(np.float32)

    center_repeat = np.array(centers)
    center_repeat = np.expand_dims(center_repeat,0)
    repeat = points_tensor.shape[0]
    center_repeat = np.repeat(center_repeat,repeat,axis=0)
    # print('center_repeat',center_repeat.shape)
    
    points_tensor_repeat = np.expand_dims(points_tensor,1)
    repeat = len(centers)
    points_tensor_repeat = np.repeat(points_tensor_repeat,repeat,axis=1)
    # print('points_tensor_repeat',points_tensor_repeat.shape)
    sigmas = 0.25 # set to 0.25 for pedestrains

    def gaussian(x,mu,sig):

        return (1/((np.pi**0.5)*sig))*np.exp(-(((x-mu)**2).sum(-1))**0.5/(2*sig**2))


    dga = gaussian(points_tensor_repeat,center_repeat,sigmas)

    distance_in_bbox = dga*pc_in_box
    distances = dga

    
    a = np.max(dga*pc_in_box,0)+1e-8

    raw_hm = dga*pc_in_box/a

    raw_hm_channel = torch.FloatTensor(raw_hm).permute(1,0).view(1,len(centers),range_image_size[0]//output_stride,range_image_size[1]//output_stride)

    raw_hm_centers = np.argmax(raw_hm,0)

    cx,cy = np.unravel_index(raw_hm_centers, (range_image_size[0]//2,range_image_size[1]//2))

    raw_hm = np.max(raw_hm,1)

    raw_hm = raw_hm.reshape(-1,range_image_size[0]//output_stride,range_image_size[1]//output_stride)

    raw_hm = torch.FloatTensor(raw_hm).view(1,1,range_image_size[0]//output_stride,range_image_size[1]//output_stride)

    hm = raw_hm.view(range_image_size[0]//2,range_image_size[1]//2).detach().numpy()
    
    hm_centers = np.zeros((len(centers),2),dtype=np.int)

    hm_centers[:,0] = cx
    hm_centers[:,1] = cy

    return hm,hm_centers,cx,cy,centers,dims,angles,points_tensor
    
def data_generator(frames_path,ri_index=0,max_objs=100):
    dataset = tf.data.TFRecordDataset(frames_path, compression_type='')
    gen = wrapper(enumerate(dataset))
    output_stride = 2
    for idx,data in gen:
        try:
            frame = open_dataset.Frame()
            frame.ParseFromString(bytearray(data.numpy()))      
            (range_images, camera_projections, range_image_top_pose) = (
              frame_utils.parse_range_image_and_camera_projection(frame))
            
            
            calibrations = sorted(frame.context.laser_calibrations, key=lambda c: c.name)
            points = []
            cp_points = []
            points_NLZ = []
            points_intensity = []
            points_elongation = []
            
            frame_pose = tf.convert_to_tensor(np.reshape(np.array(frame.pose.transform), [4, 4]))
            # [H, W, 6]
            range_image_top_pose_tensor = tf.reshape(
                tf.convert_to_tensor(range_image_top_pose.data), range_image_top_pose.shape.dims
            )
            # [H, W, 3, 3]
            range_image_top_pose_tensor_rotation = transform_utils.get_rotation_matrix(
                range_image_top_pose_tensor[..., 0], range_image_top_pose_tensor[..., 1],
                range_image_top_pose_tensor[..., 2])
            range_image_top_pose_tensor_translation = range_image_top_pose_tensor[..., 3:]
            range_image_top_pose_tensor = transform_utils.get_transform(
                range_image_top_pose_tensor_rotation,
                range_image_top_pose_tensor_translation)

            for c in calibrations:
                range_image = range_images[c.name][ri_index]
                if len(c.beam_inclinations) == 0:  # pylint: disable=g-explicit-length-test
                    beam_inclinations = range_image_utils.compute_inclination(
                        tf.constant([c.beam_inclination_min, c.beam_inclination_max]),
                        height=range_image.shape.dims[0])
                else:
                    beam_inclinations = tf.constant(c.beam_inclinations)

                beam_inclinations = tf.reverse(beam_inclinations, axis=[-1])
                extrinsic = np.reshape(np.array(c.extrinsic.transform), [4, 4])

                range_image_tensor = tf.reshape(
                    tf.convert_to_tensor(range_image.data), range_image.shape.dims)

                pixel_pose_local = None
                frame_pose_local = None
                if c.name == dataset_pb2.LaserName.TOP:
                    pixel_pose_local = range_image_top_pose_tensor
                    pixel_pose_local = tf.expand_dims(pixel_pose_local, axis=0)
                    frame_pose_local = tf.expand_dims(frame_pose, axis=0)
                range_image_mask = range_image_tensor[..., 0] > 0
                range_image_NLZ = range_image_tensor[..., 3]
                range_image_intensity = range_image_tensor[..., 1]
                range_image_elongation = range_image_tensor[..., 2]

                # print('tf.expand_dims(range_image_tensor[..., 0], axis=0)',tf.expand_dims(range_image_tensor[..., 0], axis=0).shape)
                range_image_polar = range_image_utils.compute_range_image_polar(
                    tf.expand_dims(range_image_tensor[..., 0], axis=0),
                    tf.expand_dims(extrinsic, axis=0),
                    tf.expand_dims(tf.convert_to_tensor(beam_inclinations), axis=0),
                    )
                range_image_cartesian = range_image_utils.compute_range_image_cartesian(
                range_image_polar, tf.expand_dims(extrinsic, axis=0),)
                

                B,h,w,_ = range_image_cartesian.shape

                points_tensor = range_image_cartesian.numpy().reshape(-1,3)

                range_image_size = range_image_polar.shape[1:3]
                if range_image_size[0]!=64:
                    continue

                centers,dims,angles,class_inds = get_bb_waymo(frame)
                if len(centers)==0:
                    continue


                


                '''

                get class heatmap
                '''

                
                hm = np.zeros((n_classes, range_image_size[0]//output_stride, range_image_size[1]//output_stride),
                                      dtype=np.float32)
                
                reg_target = np.zeros((7,range_image_size[0]//output_stride, range_image_size[1]//output_stride),
                                      dtype=np.float32)
                


                
                cls_hm,cls_hm_centers,cxs,cys,centers,dims,angles,ds_points_tensor = fwaymo(points_tensor,centers,dims,angles,range_image_size,range_image_polar.numpy(),range_image_mask.numpy())

                ind = np.zeros((max_objs), dtype=np.int64)
                mask = np.zeros((max_objs), dtype=np.uint8)
                cat = np.zeros((max_objs), dtype=np.int64)
                
                reg_box = np.zeros((max_objs,8), dtype=np.float32)
                
                
                num_objs = min(len(centers), max_objs) 

                # print('cx,cy',cx,cy)

                hm[0] = cls_hm
                # print('cls_hm_centers',cls_hm_centers)
                for new_idx,(center,dim,angle,class_ind,cx,cy) in enumerate(zip(centers,dims,angles,class_inds,cxs,cys)):
                    tx = cls_hm_centers[new_idx][0]
                    ty = cls_hm_centers[new_idx][1]

                    ind[new_idx] = tx * range_image_size[1]//output_stride + ty
                    mask[new_idx] = 1
                    cat[new_idx] = class_ind

                    l, w, h= dim
                    x,y,z = center
                    
                    sample = np.array(center).reshape(1,1,3)
                    
                    center  = tf.convert_to_tensor(sample)
                    
                    relative_displacement = np.squeeze((ds_points_tensor[ind[new_idx]]-np.squeeze(center.numpy(),0)),0)
                    
                    
                    reg_box[new_idx] = np.array([relative_displacement[0],relative_displacement[1],relative_displacement[2],np.log(l), np.log(w), np.log(h),np.cos(angle),np.sin(angle)])
                    
                    
                yield range_image_tensor.numpy(),range_image_cartesian.numpy(),range_image_polar.numpy(),range_image_mask.numpy(),hm,reg_target,ind,mask,cat,reg_box
        except Exception as e:
            print('Exception in iters',e)
            continue


def get_bb_waymo(frame):

    obj_name, difficulty, dimensions, locations, heading_angles = [], [], [], [], []
    tracking_difficulty, speeds, accelerations, obj_ids = [], [], [], []
    num_points_in_gt = []
    laser_labels = frame.laser_labels

    centers = []
    dims = []
    angles = []
    class_inds = []
    for i in range(len(laser_labels)):

        num_lidar_points_in_box = laser_labels[i].num_lidar_points_in_box
        if num_lidar_points_in_box<=5:
            continue

        box = laser_labels[i].box
        class_ind = laser_labels[i].type
        loc = [box.center_x, box.center_y, box.center_z]
        heading_angles.append(box.heading)
        obj_name.append(WAYMO_CLASSES[class_ind])
        if WAYMO_CLASSES[class_ind] not in use_classes:
            continue
        else:
            class_ind = use_classes.index(WAYMO_CLASSES[class_ind])

        # print('WAYMO_CLASSES[class_ind]',WAYMO_CLASSES[class_ind])
        difficulty.append(laser_labels[i].detection_difficulty_level)
        tracking_difficulty.append(laser_labels[i].tracking_difficulty_level)
        dimensions.append([box.length, box.width, box.height])  # lwh in unified coordinate of OpenPCDet
        locations.append(loc)
        obj_ids.append(laser_labels[i].id)
        num_points_in_gt.append(laser_labels[i].num_lidar_points_in_box)
        center = [box.center_x, box.center_y, box.center_z]
        
        # if box.center_x>x_range[1] or box.center_x<x_range[0]:
        #     print('dorop x',center)
        #     continue
            
        # if box.center_y>y_range[1] or box.center_y<y_range[0]:
        #     print('dorop y',center)
        #     continue
            
        # if box.center_z>z_range[1] or box.center_z<z_range[0]:
        #     print('dorop z',center)

        #     continue


        
        dim = [box.length, box.width, box.height]
        angle = box.heading
        # break 
        # print('center',center)
        # print('box',box)
        centers.append(center)
        dims.append(dim)
        angles.append(angle)
        class_inds.append(class_ind)



    return centers,dims,angles,class_inds



def gaussian2D(shape, sigma=1):
    m, n = [(ss - 1.) / 2. for ss in shape]
    y, x = np.ogrid[-m:m+1,-n:n+1]

    h = np.exp(-(x * x + y * y) / (2 * sigma * sigma))
    h[h < np.finfo(h.dtype).eps * h.max()] = 0
    return h



def gaussian_radius(det_size, min_overlap=0.5):
    height, width = det_size

    a1  = 1
    b1  = (height + width)
    c1  = width * height * (1 - min_overlap) / (1 + min_overlap)
    sq1 = np.sqrt(b1 ** 2 - 4 * a1 * c1)
    r1  = (b1 + sq1) / 2

    a2  = 4
    b2  = 2 * (height + width)
    c2  = (1 - min_overlap) * width * height
    sq2 = np.sqrt(b2 ** 2 - 4 * a2 * c2)
    r2  = (b2 + sq2) / 2

    a3  = 4 * min_overlap
    b3  = -2 * min_overlap * (height + width)
    c3  = (min_overlap - 1) * width * height
    sq3 = np.sqrt(b3 ** 2 - 4 * a3 * c3)
    r3  = (b3 + sq3) / 2
    return min(r1, r2, r3)


def draw_umich_gaussian(heatmap, center, radius, k=1):
    diameter = 2 * radius + 1
    gaussian = gaussian2D((diameter, diameter), sigma=diameter / 6)

    x, y = int(center[0]), int(center[1])

    height, width = heatmap.shape[0:2]

    left, right = min(x, radius), min(width - x, radius + 1)
    top, bottom = min(y, radius), min(height - y, radius + 1)


    masked_heatmap  = heatmap[y - top:y + bottom, x - left:x + right]
    masked_gaussian = gaussian[radius - top:radius + bottom, radius - left:radius + right]
    if min(masked_gaussian.shape) > 0 and min(masked_heatmap.shape) > 0: # TODO debug
        np.maximum(masked_heatmap, masked_gaussian * k, out=masked_heatmap)
    return heatmap




import cv2
import random
random.seed(1024)
from config import  max_objs
import argparse
def parse_args():
    parser = argparse.ArgumentParser(description="Train a detector")
    parser.add_argument("--save_path", type=str, default='/workspace/input_ped', help="save_path")
    parser.add_argument("--split", type=str, default='train', help="split")
    parser.add_argument("--limit", type=int, default=8000, help="limit")
    parser.add_argument("--skip", type=int, default=0, help="skip")
    parser.add_argument("--every", type=int, default=5, help="every")
    
    
    args = parser.parse_args()
    return args

if __name__ == '__main__':

    args = parse_args()
    
    save_path = args.save_path
    split = args.split
    skip = args.skip
    every = args.every

    records = glob('/workspace/raw_data/raw_data/*.tfrecord')

    print('total records',len(records))
    train_size = int(len(records)*0.8)

    train_records = records[:train_size]
    test_records = records[train_size:]
    
    
    if split =='train':
        count=0
        limit = args.limit
        for frame_idx,frame_path in tqdm(enumerate(train_records[skip:])):
            frame_idx +=skip
            iters = data_generator(frame_path,max_objs=max_objs)
            frame_name = frame_path.replace('.tfrecord','').split('/')[-1]
            print(frame_name)
            valid_cnt = 0
            for i,(range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,reg_target,ind,mask,cat,reg_box) in tqdm(enumerate(iters)): 
                
                
                
                
                if count>limit:
                    exit()
                    break
                try:
                    if range_image_tensor.shape[0]!=64:
                        # print(range_image_tensor.shape)
                        continue

                    if valid_cnt%every!=0:
                        valid_cnt+=1
                        continue
                    
                    # resized_hm = cv2.resize(hm[0]*255, (2650,64), )
                    # cv2.imwrite('gt_overlaped_rv.png',range_image_tensor[:,:,0]+resized_hm)
                    
                    
                    # gt_hm = np.zeros(hm[0].flatten().shape)
                    # for idx,(i,m) in enumerate(zip(ind,mask)):
                    #    gt_hm[i] = 255
                    #    print('hm[class_ind].flatten()[ind[new_idx]]',hm[0].flatten()[i],i,m)

                    # gt_hm = gt_hm.reshape(32,1325)
                    # resized_gt_hm = cv2.resize(gt_hm*255, (2650,64), )



                    # cv2.imwrite('gt_overlaped_gt_hm.png',range_image_tensor[:,:,0]+resized_gt_hm)
                    # exit()
                    # if count%3!=0:
                    #    count+=1
                    #    continue    
                    # print('(range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box)')
                    # print((range_image_tensor.shape,range_image_cartesian.shape,range_image_polar.shape,range_image_mask.shape,hm.shape,ind.shape,mask.shape,cat.shape,reg_box.shape))
                    # exit()
                    pd.to_pickle((range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box),save_path+'/train/%s_%s.pkl'%(frame_idx,valid_cnt))
                    count+=1
                    valid_cnt+=1
                    
                except Exception as e:
                    print('error',e,frame_path,i)
    elif split =='test':
        count=0
        limit = args.limit
        for frame_idx,frame_path in tqdm(enumerate(test_records[skip:])):
            frame_idx +=skip
            iters = data_generator(frame_path,max_objs=max_objs)
            frame_name = frame_path.replace('.tfrecord','').split('/')[-1]
            valid_cnt =0
            print(frame_name)
            for i,(range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,reg_target,ind,mask,cat,reg_box) in tqdm(enumerate(iters)): 
                
                if i<=skip:
                    count+=1
                    continue
                    
                if count/3>limit:
                    exit()
                    break
                try:
                    if range_image_tensor.shape[0]!=64:
                        # print(range_image_tensor.shape)
                        continue

                    if valid_cnt%every!=0:
                        valid_cnt+=1
                        continue

                    #if count%3!=0:
                    #    count+=1
                    #    continue
                    # print('(range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box)')
                    # print((range_image_tensor.shape,range_image_cartesian.shape,range_image_polar.shape,range_image_mask.shape,hm.shape,ind.shape,mask.shape,cat.shape,reg_box.shape))
                    
                    pd.to_pickle((range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box),save_path+'/test/%s_%s.pkl'%(frame_idx,valid_cnt))
                    count+=1
                    valid_cnt+=1

                except Exception as e:
                    print('error',e,frame_path,i)

