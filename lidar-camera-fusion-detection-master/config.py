# file config
import torch
if torch.cuda.is_available():
    torch.cuda.set_device(2)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') 
seed = 1024

WAYMO_CLASSES = ['unknown', 'Vehicle', 'Pedestrian', 'Sign', 'Cyclist']
use_classes =  ['Pedestrian']
use_classes =  ['Vehicle']
n_classes = len(use_classes)+1

merge_data=0

max_objs = 100

nrows = 1000
# feature config

# path config
full_path = 'workspace/raw_data' #

train_data_path = 'workspace/train' 
val_data_path = 'workspace/val' 

save_path = './' 

## global training config
train_model = 1 
load_model = 0 

base_learning_rate = 0.001

batch_size = 1 # batch_size

learning_rate = base_learning_rate*batch_size

require_improvement_epoch = 20 # 未提升早停轮数
num_epochs = 100  # 最大训练轮次
ealry_stop = True
loc_loss_weight = 2
score_threshold = 0.2


hidden_dim = 100 # 模型隐层神经元个数，控制模型复杂程度
target_dim = 1 # 输出size
num_heads = 4

window_size = 30 # 样本窗口大小，24个时间点，根据需求调整

x_range = [-74.88,74.88]
y_range = [-74.88,74.88]
z_range = [-2,4]


feature_norms = [
(16.564554,14.485749),
(10.341559,508.17743),
(-0.06470848,0.39775458),
(-0.97956353,0.20113358),
(1.7434629,16.920246),
(0.8516079,13.96611),
(1.2794306,1.110538),
]
