import os
import pickle
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import pandas as pd

import tensorflow as tf
from waymo_open_dataset.utils import frame_utils, transform_utils, range_image_utils,box_utils
from waymo_open_dataset import dataset_pb2 as open_dataset

from torchmetrics import AveragePrecision,AUC

from waymo_open_dataset import dataset_pb2
from matplotlib import pylab as plt
import config

from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler,Dataset
from glob import glob
from tqdm import tqdm
from src.models.edgeconv import SmartDownSampling
tf.compat.v1.enable_eager_execution()




import argparse
from lib.utils.iou3d_nms.iou3d_nms_utils import boxes_iou3d_gpu,nms_normal_gpu,nms_gpu
from config import n_classes
from src import utils

output_stride = 2
min_overlap = 0.1
_min_radius = 2

import cv2
import random
import IoU

random.seed(1024)


def get_dir(box, angle, axis_rot, length=4):
    import math
    # plane on x,y axis
    box_xy = box[:4, :2]  # [[x,y], [x,y], [x,y], [x,y]]
    x0, y0 = np.mean(box_xy, axis=0)  # [x, y]
    # rotate because the points drew are rotate from xy to yx
    dx, dy = - np.cos(
        angle+axis_rot) * length, - np.sin(angle+axis_rot) * length
    return x0, y0, dx, dy


def xyzwhl2eight(centers):
    """ Draw 3d bounding box in image
        qs: (8,3) array of vertices for the 3d box in following order:
            7 -------- 6
           /|         /|
          4 -------- 5 .
          | |        | |
          . 3 -------- 2
          |/         |/
          0 -------- 1
    """
    corners = []
    for center in centers:
        x, y, z, w, h, p = center[:6]
        corner = np.array(
            [
                [
                    x + w / 2,
                    x + w / 2,
                    x - w / 2,
                    x - w / 2,
                    x + w / 2,
                    x + w / 2,
                    x - w / 2,
                    x - w / 2,
                ],
                [
                    y - h / 2,
                    y + h / 2,
                    y + h / 2,
                    y - h / 2,
                    y - h / 2,
                    y + h / 2,
                    y + h / 2,
                    y - h / 2,
                ],
                [
                    z - p / 2,
                    z - p / 2,
                    z - p / 2,
                    z - p / 2,
                    z + p / 2,
                    z + p / 2,
                    z + p / 2,
                    z + p / 2,
                ],
            ]
        )
        corner = corner.T
        corners.append(corner)
    return corners


def gather(self, dim, index):
    """
    Gathers values along an axis specified by ``dim``.

    For a 3-D tensor the output is specified by:
        out[i][j][k] = input[index[i][j][k]][j][k]  # if dim == 0
        out[i][j][k] = input[i][index[i][j][k]][k]  # if dim == 1
        out[i][j][k] = input[i][j][index[i][j][k]]  # if dim == 2

    Parameters
    ----------
    dim:
        The axis along which to index
    index:
        A tensor of indices of elements to gather

    Returns
    -------
    Output Tensor
    """
    idx_xsection_shape = index.shape[:dim] + \
        index.shape[dim + 1:]
    self_xsection_shape = self.shape[:dim] + self.shape[dim + 1:]
    if idx_xsection_shape != self_xsection_shape:
        raise ValueError("Except for dimension " + str(dim) +
                         ", all dimensions of index and self should be the same size")
    if index.dtype != np.dtype('int_'):
        raise TypeError("The values of index must be integers")
    data_swaped = np.swapaxes(self, 0, dim)
    index_swaped = np.swapaxes(index, 0, dim)
    gathered = np.choose(index_swaped, data_swaped)
    return np.swapaxes(gathered, 0, dim)




def _gather_feat(feat, ind, mask=None):
    # dim = B,M,C
    dim  = feat.size(2)

    ind  = ind.unsqueeze(2).expand(ind.size(0), ind.size(1), dim)
    
    feat = feat.gather(1, ind)
    if mask is not None:
        mask = mask.unsqueeze(2).expand_as(feat)
        feat = feat[mask]
        feat = feat.view(-1, dim)
    return feat

def _transpose_and_gather_feat(feat, ind):


    # B x C x H x W
    feat = feat.permute(0, 2, 3, 1).contiguous()
    # B x H x W x C
    feat = feat.view(feat.size(0), -1, feat.size(3))
    # M = H x W
    # B x M x C

    feat = _gather_feat(feat, ind)
    return feat




# class IOULoss(nn.Module):
#   '''Regression loss for an output tensor
#     Arguments:
#       output (batch x dim x h x w)
#       mask (batch x max_objects)
#       ind (batch x max_objects)
#       target (batch x max_objects x dim)
#   '''

#   def __init__(self):
#     super(IOULoss, self).__init__()
    
#   def forward(self, output, mask, ind, target):

#     # print('target.size()',target.size())
#     # print('output.size()',output.size())


#     target = target.view(1,100,8)
#     output = output.view(1,8, 32, 1325)

#     # target[:,:,6] = torch.arccos(target[:,:,6])
#     # output[:,6,:,:] = torch.arccos(output[:,6,:,:])


#     cos_tgt = target[:,:,6]
#     cos_op = output[:,6,:,:]
    
#     sin_tgt = target[:,:,7]
#     sin_op = output[:,7,:,:]
    
#     # print('sin_tgt.size()',sin_tgt.size())

#     # print('cos_tgt.size()',cos_tgt.size())


#     heading_tgt = sin_tgt/(cos_tgt+1e-8)
#     heading_op = sin_op/(cos_op+1e-8)
    


# #   
#     # print('sin_op.size()',sin_op.size())
# #   
#     # print('cos_op.size()',cos_op.size())




#     # print('output',output.size())
#     # print('output[:,6,:,:]',output[:,6,:,:].size())
#     # print('heading_op',heading_op.size())

#     target[:,:,6] = heading_tgt
#     output[:,6,:,:] = heading_op




#     target = target[:,:,:7]
#     output = output[:,:7,:,:]

#     # target[:,:,6] = heading_tgt
#     # output[:,6,:,:] = heading_op
    
    
#     target[:,:,3:6] = torch.exp(target[:,:,3:6])
#     output[:,3:6,:,:] = torch.exp(output[:,3:6,:,:])

#     pred = _transpose_and_gather_feat(output, ind)
#     # print('pred',pred.size())
#     mask = mask.float().unsqueeze(2) 
#     # print('mask',mask.size())
#     loss_functioon =  IoU.IoU3D
    

#     # ty = ind // (2650/2)
#     # tx = ind % (2650/2)

#     # # shift_y = ct[1] -  ct_int[1]
#     # # shift_z = ct[0] -  ct_int[0]
#     # # reg_target[:,coor_x,coor_y] = np.array([x,shift_y,shift_z,np.log(w), np.log(l), np.log(h),np.cos(angle)])
    
#     # pred[:,:,1]  = pred[:,:,1] + ty
#     # pred[:,:,2]  = pred[:,:,2] + tx
    
#     # target[:,:,1]  = target[:,:,1] + ty
#     # target[:,:,2]  = target[:,:,2] + tx
    
#     loss_list = []
#     # loss = loss_functioon(target, target)*mask
#     for t,p,m in zip(target*mask,pred*mask,mask):
#         ans_iou = boxes_iou3d_gpu(t,p)
#         ans_iou = torch.diagonal(ans_iou)

#         max_ans_iou = ans_iou.max()

#         # if max_ans_iou>100:
#         #     idx = torch.argmax(ans_iou)
#         #     print('t max',t[idx])
#         #     print('p max',p[idx])
#         #     break
        
#         mean_ans_iou = ans_iou.sum()/(m.sum()+1e-8)
        
#         print('ans_iou',ans_iou,ans_iou.size())
#         print('max_ans_iou',max_ans_iou)
#         print('mean_ans_iou',mean_ans_iou)
        
#         loss_list.append(ans_iou)

#     # print('loss',loss)
#     # loss = loss / (mask.sum() + 1e-4)

#     loss = torch.cat(loss_list)
#     # loss[loss==torch.nan]=0
#     # loss = torch.nan_to_num(loss,0)

#     # m = (loss==0).sum()+1e-8
#     # print(loss)
#     # print('ind',ind)
#     # print('pred',pred)
#     # print('target',target)
#     # print('===========')
    
#     # loss = loss.sum(dim=1)
#     return loss.sum()/(mask.sum() + 1e-4)
#     # return (loss.sum()/(mask.sum() + 1e-4))/m




# def get_batch_statistics(selected_boxes,selected_scores,selected_labels, target_boxes,target_labels, iou_threshold=0.5):


#     true_positives = np.zeros(selected_boxes.shape[0])

    
#     if len(annotations):
#         detected_boxes = []
#         # target_boxes = annotations[:, 1:]
#         for pred_i, (pred_box, pre_label) in enumerate(zip(selected_boxes, selected_labels)):
#             if len(detected_boxes) == len(annotations):
#                 break

#             if pre_label not in target_labels:
#                 continue

#             ans_iou = boxes_iou3d_gpu(t,p)

#             iou, box_index = bbox_iou(pred_box.unsqueeze(0), target_boxes).max(0)
#             if iou >= iou_threshold and box_index not in detected_boxes:
#                 true_positives[pred_i] = 1
#                 detected_boxes+= [box_index]
#     batch_metrics.append([true_positives, selected_scores, selected_labels])
#     return batch_metrics



# class AP(nn.Module):
#   '''Regression loss for an output tensor
#     Arguments:
#       output (batch x dim x h x w)
#       mask (batch x max_objects)
#       ind (batch x max_objects)
#       target (batch x max_objects x dim)
#   '''

#   def __init__(self,threshold=0.5):
#     super(AP, self).__init__()
#     self.threshold = threshold
    
#   def forward(self, output, mask, ind, target,cls_target=None,cls_output=None):

#     # print('target.size()',target.size())
#     # print('output.size()',output.size())


#     target = target.view(1,100,8)
#     output = output.view(1,8, 32, 1325)

#     cls_output = (cls_output>score_threshold).float()
#     cls_target = cls_target.view(1,n_classes, 32, 1325)
#     cls_output = cls_output.view(1,n_classes, 32, 1325)

#     # target[:,:,6] = torch.arccos(target[:,:,6])
#     # output[:,6,:,:] = torch.arccos(output[:,6,:,:])


#     cos_tgt = target[:,:,6]
#     cos_op = output[:,6,:,:]
    
#     sin_tgt = target[:,:,7]
#     sin_op = output[:,7,:,:]
    
#     # print('sin_tgt.size()',sin_tgt.size())

#     # print('cos_tgt.size()',cos_tgt.size())


#     heading_tgt = sin_tgt/(cos_tgt+1e-8)
#     heading_op = sin_op/(cos_op+1e-8)
    


# #   
#     # print('sin_op.size()',sin_op.size())
# #   
#     # print('cos_op.size()',cos_op.size())




#     # print('output',output.size())
#     # print('output[:,6,:,:]',output[:,6,:,:].size())
#     # print('heading_op',heading_op.size())

#     target[:,:,6] = heading_tgt
#     output[:,6,:,:] = heading_op




#     target = target[:,:,:7]
#     output = output[:,:7,:,:]

#     # target[:,:,6] = heading_tgt
#     # output[:,6,:,:] = heading_op
    
    
#     target[:,:,3:6] = torch.exp(target[:,:,3:6])
#     output[:,3:6,:,:] = torch.exp(output[:,3:6,:,:])

#     pred = _transpose_and_gather_feat(output, ind)
#     # print('pred',pred.size())
#     mask = mask.float().unsqueeze(2) 
#     # print('mask',mask.size())
#     loss_functioon =  IoU.IoU3D
    

#     # ty = ind // (2650/2)
#     # tx = ind % (2650/2)

#     # # shift_y = ct[1] -  ct_int[1]
#     # # shift_z = ct[0] -  ct_int[0]
#     # # reg_target[:,coor_x,coor_y] = np.array([x,shift_y,shift_z,np.log(w), np.log(l), np.log(h),np.cos(angle)])
    
#     # pred[:,:,1]  = pred[:,:,1] + ty
#     # pred[:,:,2]  = pred[:,:,2] + tx
    
#     # target[:,:,1]  = target[:,:,1] + ty
#     # target[:,:,2]  = target[:,:,2] + tx
    
#     pos_pred_pix = _transpose_and_gather_feat(cls_output, ind) # B x M x C
#     target_pix = _transpose_and_gather_feat(cls_target, ind) # B x M x C

#     # print('pos_pred_pix',pos_pred_pix.size())
#     # print('target_pix',target_pix.size())
    
#     # pos_pred_pix = torch.argmax(pos_pred_pix,2)
    
    
#     ap = 0.0
#     loss_list = []

#     loss_score = 0.0
#     # loss = loss_functioon(target, target)*mask
#     for t,p,pp,tp,m in zip(target*mask,pred*mask,pos_pred_pix*mask,target_pix*mask,mask):
#         ans_iou = boxes_iou3d_gpu(t,p)
#         ans_iou = torch.diagonal(ans_iou)
#         # print('pp,tp',pp.size(),tp.size())

#         ap = (ans_iou>self.threshold).float().sum()/((ans_iou>0.0).float().sum()+1e-8)
#         print('ap',ap)

#         # predicted = ((ans_iou>self.threshold).float().sum()+1e-8)
#         # matched = ((pp[:,class_idx]==tp[:,class_idx]) & (ans_iou>self.threshold)).float().sum()
#         # ap = matched/predicted
#         # print('ap',ap)
#         # print('matched',matched)
#         # print('(pp[:,class_idx]==tp[:,class_idx]).float()*',(pp[:,class_idx]==tp[:,class_idx]).float().size())
#         # print('m*',m.size())
#         # acc = ((pp[:,class_idx]==tp[:,class_idx]).float()*m.view(-1)).sum()/(m.sum()+1e-8)
#         # print('acc',acc)
        
#         # max_ans_iou = ans_iou.max()

#         # if max_ans_iou>100:
#         #     idx = torch.argmax(ans_iou)
#         #     print('t max',t[idx])
#         #     print('p max',p[idx])
#         # #     break
        
#         mean_ans_iou = ans_iou.sum()#/(m.sum()+1e-8)
#         # print('ans_iou',ans_iou)
#         # print('map',ap)
#         # print('ans_iou',ans_iou,ans_iou.size())
#         # # print('max_ans_iou',max_ans_iou)
#         # print('mean_ans_iou',mean_ans_iou)
        
#         loss_list.append(ans_iou)
#         #loss_score = loss_score+mean_ans_iou
#         loss_score +=ap
        
#     # print('loss',loss)
#     # loss = loss / (mask.sum() + 1e-4)
    
#     loss = torch.cat(loss_list)
#     # loss[loss==torch.nan]=0
#     # loss = torch.nan_to_num(loss,0)
    
#     # m = (loss==0).sum()+1e-8
#     # print(loss)
#     # print('ind',ind)
#     # print('pred',pred)
#     # print('target',target)
#     # print('===========')
    
#     # loss = loss.sum(dim=1)
#     return loss_score#/len(target)
    





def _gather_feat(feat, ind, mask=None):
    # dim = B,M,C
    dim  = feat.size(2)

    ind  = ind.unsqueeze(2).expand(ind.size(0), ind.size(1), dim)
    
    feat = feat.gather(1, ind)
    if mask is not None:
        mask = mask.unsqueeze(2).expand_as(feat)
        feat = feat[mask]
        feat = feat.view(-1, dim)
    return feat

def _transpose_and_gather_feat(feat, ind):


    # B x C x H x W
    feat = feat.permute(0, 2, 3, 1).contiguous()
    # B x H x W x C
    feat = feat.view(feat.size(0), -1, feat.size(3))
    # M = H x W
    # B x M x C

    feat = _gather_feat(feat, ind)
    return feat




class RegLoss(nn.Module):
  '''Regression loss for an output tensor
    Arguments:
      output (batch x dim x h x w)
      mask (batch x max_objects)
      ind (batch x max_objects)
      target (batch x max_objects x dim)
  '''
  def __init__(self):
    super(RegLoss, self).__init__()
  
  def forward(self, output, mask, ind, target):
    output = torch.unsqueeze(output,0)
    target = torch.unsqueeze(target,0)

    pred = _transpose_and_gather_feat(output, ind)
    # print('pred',pred.size())
    mask = mask.float().unsqueeze(2) 
    # print('mask',mask.size())
    
    loss = F.l1_loss(pred*mask, target*mask, reduction='none')
    loss = loss / (mask.sum() + 1e-4)
    loss = loss.transpose(2 ,0).sum(dim=2).sum(dim=1)
    return loss.sum()

class FastFocalLoss(nn.Module):
  '''
  Reimplemented focal loss, exactly the same as the CornerNet version.
  Faster and costs much less memory.
  '''
  def __init__(self,alpha=2,beta=4):
    super(FastFocalLoss, self).__init__()
    self.alpha = alpha
    self.beta = beta

  def forward(self, out, target, ind, mask, cat):
    '''
    Arguments:
      out, target: B x C x H x W
      ind, mask: B x M
      cat (category id for peaks): B x M
    '''
    out = torch.unsqueeze(out,0)
    target = torch.unsqueeze(target,0)
    
    # out = F.softmax(out,1)
    # print('out',out.sum())
    mask = mask.float()
    gt = torch.pow(1 - target, self.beta)
    # print('gt',gt.sum())
    # print('torch.log(1 - out) ',(torch.log(1 - out)).sum())
    neg_loss = torch.log(1 - out) * torch.pow(out, self.alpha) * gt
    neg_loss = neg_loss.sum()
    # print('neg_loss',neg_loss)
    
    pos_pred_pix = _transpose_and_gather_feat(out, ind) # B x M x C
    target_pix = _transpose_and_gather_feat(target, ind) # B x M x C
    # print('target_pix',target_pix)

    # print('pos_pred_pix',pos_pred_pix.sum())
    
    pos_pred = pos_pred_pix.gather(2, cat.unsqueeze(2)) # B x M
    # print('pos_pred',pos_pred.sum())
    
    num_pos = mask.sum()
    pos_loss = torch.log(pos_pred) * torch.pow(1 - pos_pred, self.alpha) * \
               mask.unsqueeze(2)
    pos_loss = pos_loss.sum()
    # print('pos_loss',pos_loss)
    if num_pos == 0:
      return - neg_loss

    return - (pos_loss + neg_loss) / num_pos


def parse_args():
    parser = argparse.ArgumentParser(description="vis output")
    parser.add_argument("--test_idx", type=int, default=0, help="test_idx")
    parser.add_argument("--score_threshold", type=float, default=config.score_threshold, help="score_threshold")
    parser.add_argument("--class_idx", type=int, default=0, help="class_idx")
    parser.add_argument("--data_path", type=str, default='/workspace/input_ped_clip', help="data_path")
    parser.add_argument("--limit", type=int, default=100,help="limit")
    parser.add_argument("--device_id", type=int, default=1,help="device_id")
    parser.add_argument("--nms_iou_threshold", type=float, default=0.1,help="nms_iou_threshold")
    args = parser.parse_args()
    return args


args = parse_args()
test_idx = args.test_idx
score_threshold = args.score_threshold
class_idx = args.class_idx
data_path = args.data_path
limit = args.limit
device_id = args.device_id

if torch.cuda.is_available():
    torch.cuda.set_device(device_id)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') 

X_test = []
y_test = []


#global err 
#global cnt_error
#err = 0.0
#cnt_error = 0.0

def data_loader(data_path):
    range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box = pd.read_pickle(data_path)
    # mask_by_ind = (ind>0).astype(np.float32)
    mask_by_ind = mask
    # print('range_image_tensor',range_image_tensor.shape)
    #print('(mask-mask_by_ind).mean()',(mask-mask_by_ind).mean())

    #err+=(mask-mask_by_ind).sum()
    #mask.sum()
    
    range_image_tensor = np.expand_dims(range_image_tensor,0)
    range_image_mask = np.expand_dims(range_image_mask,-1)
    range_image_mask = np.expand_dims(range_image_mask,0)
    x = np.concatenate([range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask],axis=-1).transpose(0,3,1,2).squeeze(0)
    

    #range_mask = (-75<range_image_cartesian[:,:,:,0])&(range_image_cartesian[:,:,:,0]<75)
    #range_mask = range_mask&(-75<range_image_cartesian[:,:,:,1])&(range_image_cartesian[:,:,:,0]<75) 
    #range_mask = range_mask&(-2<range_image_cartesian[:,:,:,2])&(range_image_cartesian[:,:,:,2]<4) 


    #range_image_tensor = range_image_tensor*range_mask.astype(float).reshape(1,64,2650,1)


    #print('range_mask',range_mask.shape)
    #print('range_image_tensor',range_image_tensor.shape)

    # print('x.shape',x.shape)
    # print('hm.shape',hm.shape)
    # print('ind.shape',ind.shape)
    # print('mask.shape',mask.shape)
    # print('cat.shape',cat.shape)

    return torch.FloatTensor(x),torch.FloatTensor(hm),torch.FloatTensor(reg_box),torch.LongTensor(ind),torch.LongTensor(mask_by_ind),torch.LongTensor(cat)


class CustomDataset(Dataset):

    def __init__(self,files = '/workspace/input/train/*.pkl',loader=data_loader,limit=100000000000):
        print('files',files)
        self.file_list = glob(files)[:limit]
        self.loader = loader


    def __getitem__(self,index):

        return self.loader(self.file_list[index])

    def __len__(self):
        return len(self.file_list)




class_gt,reg_gt,class_res,reg_res = pd.read_pickle(data_path+'/pred_test.pkl')

# print('reg_gt',reg_gt.shape)
# print('reg_res',reg_res.shape)
# print('reg_res',reg_res[0])



for idx,dp in enumerate(tqdm(glob(data_path+'/test/*.pkl')[:limit])):
    try:
        range_image_tensor,range_image_cartesian,range_image_polar,range_image_mask,hm,ind,mask,cat,reg_box = pd.read_pickle(dp)
    except Exception as e:
        print(e)
        continue
    mask = (ind>0).astype(int)
    
    print('range_image_tensor',range_image_tensor.shape)
    print('range_image_cartesian',range_image_cartesian.shape)
    print('range_image_polar',range_image_polar.shape)
    print('range_image_mask',range_image_mask.shape)
    
    print('hm',hm.shape)
    print('reg_box',reg_box.shape)
    
    if idx ==test_idx:
        break

print('limit',limit)
dev_dataset = CustomDataset(data_path+'/test/*.pkl',limit=limit)
dev_iter = DataLoader(dataset=dev_dataset,batch_size=1,shuffle=False)
X_test_ind = []
X_test_mask = []
X_test_cat = []

X_test_rv = []

for trains,labels,reg_labels,test_ind,test_mask,test_cat in dev_iter: 
    X_test_rv.append(trains.numpy())
    X_test_ind.append(test_ind.numpy())
    X_test_mask.append(test_mask.numpy())
    X_test_cat.append(test_cat.numpy())
 


tmp = np.array(X_test_rv).reshape(len(X_test_rv),-1,64,2650)

for i in range(X_test_rv[0].shape[1]):
    mean,std = np.mean(tmp[:,i]),np.std(tmp[:,i])
    print('i,mean,std',i,mean,std)



X_test_rv = torch.FloatTensor(X_test_rv)
print('X_test_rv',X_test_rv.size())

reg_gt = torch.FloatTensor(reg_gt)
reg_res = torch.FloatTensor(reg_res)

class_gt = torch.FloatTensor(class_gt)
class_res = torch.FloatTensor(class_res)

print('class_res',class_res.size())
print('class_gt',class_gt.size())
X_test_ind = torch.LongTensor(X_test_ind)
X_test_mask = torch.LongTensor(X_test_mask)
X_test_cat = torch.LongTensor(X_test_cat)
print('X_test_ind',X_test_ind.size())

tmp_class_res =  _transpose_and_gather_feat(class_res,X_test_ind.view(-1,100))
tmp_class_gt =  _transpose_and_gather_feat(class_gt,X_test_ind.view(-1,100))
#.view(class_res.shape[0],2,class_res.shape[2]
average_precision = AveragePrecision(pos_label=1)
ap = average_precision(tmp_class_res, tmp_class_gt)

print('ap',ap)

auc = AUC(reorder=True)
auc_score = auc(tmp_class_res.view(-1), tmp_class_gt.view(-1))

print('auc_score',auc_score)



# X_test_ind = torch.unsqueeze(X_test_ind,1)
# X_test_mask = torch.unsqueeze(X_test_mask,1)
# X_test_cat = torch.unsqueeze(X_test_cat,1)


print('X_test_ind.shape',X_test_ind.shape)




class AP(nn.Module):
  '''Regression loss for an output tensor
    Arguments:
      output (batch x dim x h x w)
      mask (batch x max_objects)
      ind (batch x max_objects)
      target (batch x max_objects x dim)
  '''

  def __init__(self,threshold=0.5):
    super(AP, self).__init__()
    self.threshold = threshold
    
  def forward(self, output, mask, ind, target,cls_target=None,cls_output=None):

    target = target.view(1,100,-1)
    #output = output.view(1,-1, 32, 1325)

    #cls_output = (cls_output>score_threshold).float()
    cls_target = cls_target.view(1,n_classes, 32, 1325)
    cls_output = cls_output.view(1,n_classes, 32, 1325)



    #pred = _transpose_and_gather_feat(output, ind)
    pred = output
    #print('pred',pred.size())
    mask = mask.float().unsqueeze(2) 
    # print('mask',mask.size())
    loss_functioon =  IoU.IoU3D
    
    pos_pred_pix = _transpose_and_gather_feat(cls_output, ind) # B x M x C
    target_pix = _transpose_and_gather_feat(cls_target, ind) # B x M x C


    ap = 0.0
    loss_list = []

    loss_score = 0.0
    loss_score = torch.zeros(1).to(ind.device)
    #loss_score = torch.FloatTensor(loss_score).to(ind.device)
    # loss = loss_functioon(target, target)*mask
    valid_box = mask.sum().to(torch.long)
    #print('valid_box',valid_box)
    target = target[:,:valid_box,:]
    for t,p,pp,tp,m in zip(target,pred,pos_pred_pix*mask,target_pix*mask,mask):
        print('t,p',t.size(),p.size()[0])
        
        if p.size()[0]==0:
            continue
        #print('check skip',p.size())
        ans_iou = boxes_iou3d_gpu(t,p)
        print('ans_iou',ans_iou,ans_iou.size())

        max_ans_iou,iou_index = torch.max(ans_iou,0)
        print('max_ans_iou',max_ans_iou,max_ans_iou.size())
        print('iou_index',iou_index)
        
        # max_ans_iou,iou_idx = torch.max(ans_iou)
        # print('max_ans_iou,iou_idx',max_ans_iou,iou_idx)
        ans_iou = torch.diagonal(ans_iou)
        #print('dia ans_iou',ans_iou.size(),ans_iou)
        # max_ans_iou,iou_idx = torch.max(ans_iou)
        # print('max_ans_iou,iou_idx',max_ans_iou,iou_idx)
        ap = (ans_iou>self.threshold).float().sum()/((ans_iou>0.0).float().sum()+1e-8)
        #print('ap',ap)

        mean_ans_iou = ans_iou.sum()#/(m.sum()+1e-8)

        loss_list.append(ans_iou)

        loss_score +=ap

    #loss = torch.cat(loss_list)

    return loss_score


iou_loss = AP()
cls_loss_function = FastFocalLoss()
reg_loss_function = RegLoss()

iou_score = 0.0
cls_score = 0.0
reg_score = 0.0

count = 0
predicted_boxes = [] 
predicted_types = []
predicted_frame_ids = []
predicted_scores = []
gt_boxes = []
gt_types = [] 
gt_frame_ids = []

with torch.no_grad():
    for t_idx,(reg_labels,reg_outputs,test_ind,test_mask,test_cat,cls_labels,cls_outputs,rv) in enumerate(zip(reg_gt,reg_res,X_test_ind,X_test_mask,X_test_cat,class_gt,class_res,X_test_rv)):
        # print('rv',rv.shape)
        # 获取range image 的cartesian坐标x,y,z 得到的维度为64,2650,3
        # print('rv shape 1',rv.shape)
        coords = torch.FloatTensor(rv[:,7:10,:,:]).view(3,64,2650).permute(1,2,0).view(64,2650,3)
        masks = torch.FloatTensor(rv[:,10:,:,:]).view(1,64,2650).permute(1,2,0).view(64,2650,1)
        rv = torch.FloatTensor(rv[:,4:7,:,:]).view(3,64,2650)#.permute(1,2,0)
        
        #rv = nn.functional.max_pool2d(rv,2,2)
        #rv = rv.detach().cpu().numpy()
        # print('rv shape 2',rv.shape)
        #print('rv.shape',rv.shape)
        # 通过opencv进行降维，得到32,1325,3
        # rv = cv2.resize(rv,(1325,32))
        raw_rv = rv.reshape(1,3,64,2650).to(device)
        coords = coords.permute(2,0,1).reshape(1,3,64,2650).to(device)
        masks = masks.reshape(1,1,64,2650).to(device)
        
        sds = SmartDownSampling(32,32,kernel_size=output_stride,stride=output_stride)
        
        
        ds_rv,ds_corrd,ds_mask = sds(raw_rv,coords,masks)
        rv = ds_rv

         

        # print('rv shape 3',rv.shape)

        # print('rv 0',rv[:,:,0].max())
        # print('rv 1',rv[:,:,1].max())
        # print('rv 2',rv[:,:,2].max())

        #print(rv.shape)
        # 1，3，32，1325
        #rv = torch.FloatTensor(rv).permute(2,0,1).reshape(1,3,32,1325).to(device)
        # print('rv shape 4',rv.shape)
        # 1，100,3
        match_points = _transpose_and_gather_feat(rv.view(1,-1,32,1325).to(device),test_ind.to(device))
        
        #print('match_points',match_points.shape)


        cls_mask = cls_labels[0]>0
        # print('cls_mask',cls_mask.shape)
        # exit()


        frame_id = count
        n_gt_boxes = test_mask.sum()
        gt_frame_ids += [np.array([frame_id]*n_gt_boxes).astype(int)]
        gt_types += [np.array([2]*n_gt_boxes).astype(int)]


        tx,ty = np.unravel_index(test_ind, (32,1350))



        #reg_outputs = reg_outputs.to(device)
        #reg_labels = reg_labels.to(device)

        reg_outputs[3:6,:,:] = np.exp(reg_outputs[3:6,:,:])

        tmp = rv[0].detach().cpu().numpy() - reg_outputs[:3,:,:].detach().cpu().numpy()
        tmp = torch.FloatTensor(tmp)
        reg_outputs[:3,:,:] =  tmp


        # print('reg_outputs',reg_outputs)

        
        reg_labels[:,3:6] = np.exp(reg_labels[:,3:6])
        tmp = match_points[0].detach().cpu().numpy() - reg_labels[:,:3].detach().cpu().numpy()
        tmp = torch.FloatTensor(tmp)
        reg_labels[:,:3] =  tmp

        cos_tgt = reg_labels[:,6]
        cos_op = reg_outputs[6,:,:]
        
        sin_tgt = reg_labels[:,7]
        sin_op = reg_outputs[7,:,:]
        

        heading_tgt = sin_tgt/(cos_tgt+1e-8)
        heading_op = sin_op/(cos_op+1e-8)


        # print('cos_tgt',cos_tgt)
        # print('cos_op',cos_op)
        
        reg_labels[:,6] = torch.atan(heading_tgt)
        reg_outputs[6,:,:] = torch.atan(heading_op)
        
        reg_labels = reg_labels[:,:7]
        reg_outputs = reg_outputs[:7,:,:]


        gt_boxes += [reg_labels[:n_gt_boxes]]

        hm_rv = rv.reshape(3,-1).permute(1,0)[cls_mask.reshape(-1)].reshape(-1,3)
        print('hm_rv',hm_rv.shape)

        # cx,cy = tx*2,ty*2
        # print('cx,cy',cx,cy)
        # print('rv',rv.shape)

        # range_image_cartesian = rv[0,:3,cx[0],cy[0]]

        # print('range_image_cartesian',range_image_cartesian.shape)
        #print('reg_outputs',reg_outputs.shape)
        #print('reg_labels',reg_labels.shape)
        
        # reg_labels[:,3:6] = torch.exp(reg_labels[:,3:6])+range_image_cartesian
        # reg_outputs[3:6,:,:] = torch.exp(reg_outputs[3:6,:,:])+range_image_cartesian

        # reg_labels[:,3:6] = torch.log(reg_labels[:,3:6])
        # reg_outputs[3:6,:,:] = torch.log(reg_outputs[3:6,:,:])

        # # reg_outputs torch.Size([8, 32, 1325])
        # print('reg_outputs',reg_outputs.shape)
        # print('rv',rv.shape)
        # # print('reg_outputs',reg_outputs.shape)
        # exit()

        # target[:,:,3:6] = torch.exp(target[:,:,3:6])
        # output[:,3:6,:,:] = torch.exp(output[:,3:6,:,:])

        # print('reg_labels.shape',reg_labels.shape)
        # print('reg_outputs.shape',reg_outputs.shape)

        # print('cls_labels.shape',cls_labels.shape)
        # print('cls_outputs.shape',cls_outputs.shape)

        # ind[new_idx] = ty * featre_map_size[1] + tx
        # tx, ty = ct_int[0], ct_int[1]
        
        

        # ##### directly get from inds

        # cls_score = _transpose_and_gather_feat(cls_outputs.view(1,-1,32,1325).to(device),test_ind.to(device))
        # # B L C
        # cls_score = cls_score[:,:,0]
        # # B L C_box
        # candidate_boxes = _transpose_and_gather_feat(reg_outputs.view(1,-1,32,1325).to(device),test_ind.to(device))
        
        
        # score_mask = cls_score>score_threshold
        # # post_center_range = [-80, -80, -10.0, 80, 80, 10.0]
        # # post_center_range = torch.FloatTensor(
        # #         post_center_range).to(candidate_boxes.device)
        
        # # distance_mask = (candidate_boxes[..., :3] >= post_center_range[:3]).all(-1) & (candidate_boxes[..., :3] <= post_center_range[3:]).all(-1)
        # # score_mask = distance_mask & score_mask 

        # cls_score = cls_score[score_mask]
        # candidate_boxes = candidate_boxes[score_mask]

        # selected_boxes = candidate_boxes
        # n = candidate_boxes.detach().cpu().numpy().shape[0]
        # predicted_scores += [cls_score.view(-1).detach().cpu().numpy()]
        # predicted_boxes += [candidate_boxes.view(-1,7).detach().cpu().numpy()]
        # predicted_frame_ids += [np.array([frame_id]*n).astype(int)]
        # predicted_types += [np.array([2]*n).astype(int)]

        # ##### finish directly get from inds

        points_tensor = rv.reshape(3,-1).permute(1,0).detach().cpu().numpy()
        points_tensor = tf.convert_to_tensor(points_tensor.astype(np.float32))
        bb = reg_labels[:n_gt_boxes].detach().cpu().numpy()
        bb = tf.convert_to_tensor(bb.astype(np.float32))
        
        pc_in_box = box_utils.is_within_box_3d(points_tensor,bb).numpy().astype(np.float32)

        pc_in_box_mask = pc_in_box.sum(-1)>0

        pc_in_bb = rv.reshape(3,-1).permute(1,0)[pc_in_box_mask].reshape(-1,3).detach().cpu().numpy()
        
        print('pc_in_bb',pc_in_bb.shape)

        # exit()

        ###### using nms
        cls_score = cls_outputs.reshape(1,-1,32*1325).to(device).permute(0,2,1)[:,:,0]
        candidate_boxes = reg_outputs.reshape(1,-1,32*1325).to(device).permute(0,2,1)


        score_mask = cls_score>score_threshold
        # post_center_range = [-80, -80, -10.0, 80, 80, 10.0]
        # post_center_range = torch.FloatTensor(
        #         post_center_range).to(candidate_boxes.device)

        # distance_mask = (candidate_boxes[..., :3] >= post_center_range[:3]).all(-1) & (candidate_boxes[..., :3] <= post_center_range[3:]).all(-1)
        # score_mask = distance_mask & score_mask 
        cls_score = cls_score[score_mask]
        candidate_boxes = candidate_boxes[score_mask]
        print('score_mask',score_mask.sum())

        boxes = candidate_boxes.view(-1,7)
        scores = cls_score.view(-1)
        selected = nms_gpu(boxes,scores,0.15,768,100)
        print('selected',selected.shape[0])
        print('gt size',n_gt_boxes)
        
        
        # selected = rotate_nms_pcdet(boxes,scores,0.7,4096)
        selected_boxes = boxes[selected].view(1,-1,7)
        selected_scores = scores[selected].view(1,-1,1)

        print('selected_boxes',selected_boxes.view(-1,7))


        raw_outputs = reg_outputs.reshape(1,-1,32*1325).to(device).permute(0,2,1)
        print('raw_outputs',raw_outputs.shape)
        print('selected_boxes',selected_boxes.shape)
        
        
        try:
            fm = selected_boxes.view(-1,7)[0]
            fm_mask = ((raw_outputs.view(-1,7)-fm)==0).all(-1)
            for i in range(1,len(selected)):
                fm = selected_boxes.view(-1,7)[i]
                fm_mask = fm_mask | ((raw_outputs.view(-1,7)-fm)==0).all(-1)
                # print('fm_mask',fm_mask.shape)
            fm_mask = fm_mask.view(1,32,1325).detach().cpu().numpy().astype(float)
        
        except Exception as e:
            fm_mask = np.zeros((1,32,1325))
            print('e',e)

        print('fm_mask',fm_mask.sum())

        '''
        if t_idx == test_idx:
            mask_hm = fm_mask.reshape(32,1325)
            mask_boxes = selected_boxes.view(-1,7).detach().cpu().numpy()
            boxes = boxes.reshape(-1,7).detach().cpu().numpy()
            points = rv.reshape(3,-1).permute(1,0).detach().cpu().numpy()
            match_points = match_points.reshape(-1,3).detach().cpu().numpy()
            hm_rv =  hm_rv.reshape(-1,3).detach().cpu().numpy()
            fig = plt.figure(figsize=(64, 24))
            ax = fig.add_subplot(111)
            ax.set_aspect("equal")
            print('points[:, 1] max',points[:, 1].max())
            print('points[:, 1] min',points[:, 1].min())



            box3d_corners_in_lidar = xyzwhl2eight(mask_boxes[:,:6])

            ax.plot(-points[:, 1], points[:, 0], 'b.', markersize=0.5)
            # ax.plot(-match_points[:, 1], match_points[:, 0], 'r.', markersize=0.5)
            ax.plot(-pc_in_bb[:, 1], pc_in_bb[:, 0], 'r.', markersize=0.5)
            ax.plot(-mask_boxes[:, 1], mask_boxes[:, 0], 'g.', markersize=0.5)
            # #ax.plot(-boxes[:, 1], boxes[:, 0], 'y.', markersize=0.5)
            # ax.plot(-hm_rv[:, 1], hm_rv[:, 0], 'y.', markersize=0.5)
            

            num = len(box3d_corners_in_lidar)
            for n in range(num):
                b = box3d_corners_in_lidar[n][:, :2]
                # lines on bottom plane
                for k in range(4):
                    i, j = k, (k+1) % 4
                    ax.plot((-b[i, 1], -b[j, 1]), (b[i, 0], b[j, 0]), 'g-')

                angle = mask_boxes[n][-1]
                x0, y0, dx, dy = get_dir(b, angle, axis_rot=-np.pi/2)
                ax.arrow(-y0, x0, dy, dx,
                         width=0.01,
                         length_includes_head=True,
                         head_width=0.25,
                         head_length=1,
                         fc='r', ec='r')
                
                
            figure = plt.gcf() # get current figure
            plt.savefig('rv_to_pc.png',dpi=1000)
            #exit()
        '''
            

            
        predicted_boxes += [selected_boxes.view(-1,7).detach().cpu().numpy()]
        predicted_scores += [selected_scores.view(-1).detach().cpu().numpy()]

        predicted_frame_ids += [np.array([frame_id]*len(selected)).astype(int)]
        predicted_types += [np.array([2]*len(selected)).astype(int)]
        print('='*20)
        ###### finish using nms


        #print('selected',selected.size())
        #print('selected_boxes',selected_boxes.size())
        #print('selected_scores',selected_scores.shape)

         

#        print('selected',selected.shape)
#        print('selected_boxes',selected_boxes.size())
        
#        print('selected_scores',selected_scores.shape)
#        print('selected_labels',selected_labels.shape)
        
        # exit()
        # if mask.sum()==0:
        #     continue
        # if len(selected)==0:
        #     continue
        

        # cos_op = selected_boxes[:,6]
        # sin_op = selected_boxes[:,7]

        # target = reg_labels
        # # print('target',target.size())
        # cos_tgt = target[:,6]
        # sin_tgt = target[:,7]


        # heading_tgt = sin_tgt/(cos_tgt+1e-8)
        # heading_op = sin_op/(cos_op+1e-8)
        

        # target[:,6] = heading_tgt
        # selected_boxes[:,6] = heading_op


        # target[:,3:6] = torch.exp(target[:,3:6])
        # selected_boxes[:,3:6] = torch.exp(selected_boxes[:,3:6])


        # ans_iou = boxes_iou3d_gpu(target[:,:7].to(device),selected_boxes[:,:7].to(device))
        # # ans_iou = torch.diagonal(ans_iou)
        # print('ans_iou',ans_iou.size())
        # max_ans_iou,_ = torch.max(ans_iou,0)
        # print('max ans_iou',max_ans_iou)
        # print('max ans_iou',max_ans_iou.mean())

        # reg_res[count][~selected]=0
        # class_res[count][~selected]=0

        # print('cat',cat)

        # iou_score += iou_loss(selected_boxes.to(device),test_mask.to(device),test_ind.to(device),reg_labels.to(device),cls_labels.to(device),cls_outputs.to(device)).item()
        
        cls_score += cls_loss_function(cls_outputs.to(device),cls_labels.to(device),test_ind.to(device),test_mask.to(device),test_cat.to(device))
        
        reg_score += reg_loss_function(reg_outputs.to(device),test_mask.to(device),test_ind.to(device),reg_labels.to(device))
        
        count+=1


from detection_metrics_test import DetectionMetricsEstimatorTest


predicted_boxes = np.concatenate(predicted_boxes)
predicted_types = np.concatenate(predicted_types)
predicted_frame_ids =  np.concatenate(predicted_frame_ids)
predicted_scores = np.concatenate(predicted_scores)
gt_boxes = np.concatenate(gt_boxes)
gt_types = np.concatenate(gt_types)
gt_frame_ids = np.concatenate(gt_frame_ids)

tf.compat.v1.disable_eager_execution()
test = DetectionMetricsEstimatorTest()
test.testAPBasic(pd_bbox=predicted_boxes, pd_type=predicted_types, pd_frameid=predicted_frame_ids, pd_score=predicted_scores,gt_bbox=gt_boxes, gt_type=gt_types, gt_frameid=gt_frame_ids)
# test.testAPBasic(pd_bbox=predicted_boxes, pd_type=predicted_types, pd_frameid=predicted_frame_ids, pd_score=predicted_scores,gt_bbox=predicted_boxes, gt_type=predicted_types, gt_frameid=predicted_frame_ids)


#iou_score/=(X_test_mask.sum()+1e-8)
iou_score/=(X_test_mask.shape[0])
print('iou_score',iou_score)
cls_score/=count
print('cls_score',cls_score)
reg_score/=count
print('reg_score',reg_score)





class_res_0 = class_res[test_idx]
def sigmoid(x):
    return 1 / (1 + np.exp(-x))




from scipy.special import softmax


# class_res_0 = softmax(class_res_0,axis=0)

# class_res_0 = sigmoid(class_res_0)
print('class_res_0 max',class_res_0.max())
print('class_res_0 mean',class_res_0.mean())
print('class_res_0 mean',class_res_0.min())


post_center_limit_range=[-80, -80, -10.0, 80, 80, 10.0]

# class_res_0[class_res_0<score_threshold]=0

# class_res_0[class_res_0>=score_threshold]=1



t1 = (class_res>=score_threshold).float()


# print('recall',((t1 - class_gt.float())==0).float().sum()/(class_gt.float().sum()))
# print('precision',((t1 - class_gt.float())==0).float().sum()/(t1.float().sum()))


# class_res_0 = np.max(class_res_0,-1)


# class_res_0 = (class_res_0<0.1).astype(int)

# class_res_0 = class_res_0.reshape(n_classes,32*1325)
# class_res_0 = np.argmax(class_res_0,0)


# class_res_0_onehot = np.identity(n_classes)[class_res_0].reshape(n_classes,32,1325)

# class_res_0 = class_res_0_onehot

print('class_res_0.shape',class_res_0.shape)

plt.imshow(range_image_tensor[:,:,0])
figure = plt.gcf() # get current figure
plt.savefig('frame_range_img_plt.png',dpi=1000)




plt.imshow(range_image_mask)
figure = plt.gcf() # get current figure
plt.savefig('range_image_mask_plt.png',dpi=1000)

# image_output = Image.fromarray(range_image_tensor[:,:,0])

# image_output.save('frame_range_img.jpg')

n = 3000

# cv2.imwrite('frame_range_img.png',range_image_tensor[:,:n,0])

print(range_image_tensor.shape)
print(range_image_polar.shape)
print(range_image_mask.shape)
print(hm.shape)
print('ind',ind.shape)


cv2.imwrite('hm.png',hm[class_idx][:,:n]*255)
plt.imshow(hm[class_idx][:,:n]*255)
figure = plt.gcf() # get current figure
plt.savefig('hm_plt.png',dpi=1000)


# cv2.imwrite('ped_hm.png',hm[class_idx][:,:n]*255)
# plt.imshow(hm[class_idx][:,:n]*255)
# figure = plt.gcf() # get current figure
# plt.savefig('ped_hm_plt.png',dpi=1000)

# print('class_res_0',class_res_0)

class_res_0 = class_res_0.numpy()
cv2.imwrite('class_res_0.png',class_res_0[class_idx,:,:]*255)
cv2.imwrite('class_res_0_threshold.png',(class_res_0[class_idx,:,:]>score_threshold).astype(float)*255)

# cv2.imwrite('ped_class_res_0.png',class_res_0[class_idx,:,:]*255)
# plt.imshow(class_res_0[class_idx,:,:]*255)
# figure = plt.gcf() # get current figure
# plt.savefig('ped_class_res_0_plt.png',dpi=1000)


resized_hm = cv2.resize(hm[class_idx][:,:n]*255, (2650,64), interpolation = cv2.INTER_AREA)

gt_hm = np.zeros(hm[class_idx].flatten().shape)
for idx,(i,m) in enumerate(zip(ind,mask)):
    # if hm[class_idx].flatten()[i]!=0 and i>0:
    #     continue
    # tx,ty = np.unravel_index(i, hm[class_idx][:,:n].shape)
    gt_hm[i] = 255
    # print('hm[class_ind].flatten()[ind[new_idx]]',hm[class_idx].flatten()[i],i,m)



gt_hm = gt_hm.reshape(32,1325)
resized_gt_hm = cv2.resize(gt_hm[:,:n]*255, (2650,64), interpolation = cv2.INTER_AREA)



cv2.imwrite('gt_overlaped_gt_hm.png',range_image_tensor[:,:,0]+resized_gt_hm)


plt.imshow(range_image_tensor[:,:,0]+resized_hm)
figure = plt.gcf() # get current figure
plt.savefig('gt_overlaped_rv.png',dpi=1000)


resized_range_image_tensor = cv2.resize(range_image_tensor[:,:,0], (1325,32))
cv2.imwrite('gt_overlaped_resized_mask_hm.png',resized_range_image_tensor+mask_hm*255)



mask_hm = cv2.resize(mask_hm*255, (2650,64), interpolation = cv2.INTER_AREA)
cv2.imwrite('gt_overlaped_mask_hm.png',range_image_tensor[:,:,0]+mask_hm)



cv2.imwrite('gt_overlaped_rv.png',range_image_tensor[:,:,0]+resized_hm)
cv2.imwrite('gt_resized_hm.png',resized_hm)
plt.imshow(resized_hm)
figure = plt.gcf() # get current figure
plt.savefig('gt_resized_hm_plt.png',dpi=1000)

plt.imshow((range_image_tensor[:,:,0]+resized_hm)*range_image_mask)
figure = plt.gcf() # get current figure
plt.savefig('gt_overlaped_rv_masked.png',dpi=1000)




resized_hm = cv2.resize(class_res_0[class_idx,:,:]*255, (2650,64), interpolation = cv2.INTER_AREA)

# print('resized_hm',resized_hm)

# mask_x = -75<range_image_cartesian[0,:,:]<75
# mask_y = -75<range_image_cartesian[1,:,:]<75
# mask_z = -2<range_image_cartesian[2,:,:]<4


# final_mask = mask_x&mask_y&mask_z

# tmp_range_image_tensor = range_image_tensor[:,:,0]
# tmp_range_image_tensor[final_mask] = 0 

plt.imshow(range_image_tensor[:,:,0]+resized_hm)
figure = plt.gcf() # get current figure
cv2.imwrite('pred_overlaped_rv.png',range_image_tensor[:,:,0]+resized_hm)
# plt.savefig('pred_overlaped_rv.png',dpi=1000)
cv2.imwrite('pred_resized_hm.png',resized_hm)
plt.imshow(resized_hm)
figure = plt.gcf() # get current figure
plt.savefig('pred_resized_hm_plt.png',dpi=1000)
