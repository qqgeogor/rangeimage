
import torch
import torch.nn as nn
import torch.nn.functional as F


from src.models.edgeconv import EdgeConv,SmartDownSampling,Dynamic_conv2d,LightweightConv2d,BNReLUConv2d,PolarFeatureExtractor,PolarMeanFeatureExtractor
from src.models.rqconv2d import RangeQuantizedConv2d
# from src.models.dla import FeatureAggregatorBlock

# kernel_func = Dynamic_conv2d
# kernel_func = LightweightConv2d
# kernel_func = EdgeConv
# kernel_func = BNReLUConv2d
# kernel_func = nn.Conv2d

class FeatureAggregatorBlock(nn.Module):

    def __init__(self, in_channels_fine, in_channels_coarse, out_channels,kernel_size=3,kernel_func=Dynamic_conv2d):
        super(FeatureAggregatorBlock, self).__init__()

        self.in_channels_fine = in_channels_fine
        self.in_channels_coarse = in_channels_coarse
        self.out_channels = out_channels
        
        self.coarse_deconv = nn.ConvTranspose2d(in_channels=self.in_channels_coarse,
                                                out_channels=self.in_channels_coarse,
                                                kernel_size=(2, 2),
                                                stride=(2, 2))
        
        self.first_conv_block_a = kernel_func(in_channels=in_channels_fine + in_channels_coarse, out_channels=out_channels,
                           kernel_size=kernel_size,
                           padding=1,
                           stride=1
                           )

        self.first_conv_block_b = kernel_func(in_channels=out_channels, out_channels=out_channels,
                           kernel_size=kernel_size,
                           padding=1,
                           stride=1
                           )

        
        self.transformed_x = kernel_func(in_channels=in_channels_fine + in_channels_coarse,
                                        out_channels=out_channels,kernel_size=kernel_size)
        self.elu = nn.ELU()
        
        self.residual_block2 = kernel_func(in_channels=out_channels,
                           out_channels=out_channels,
                           kernel_size=kernel_size,
                           padding=1,
                           stride=1)
        
    def forward(self, fine_x, coarse_x,input_coord,input_mask):
        coarse_x = self.coarse_deconv(coarse_x)

        x = torch.cat((fine_x, coarse_x), 1) 
        
        out = self.first_conv_block_a(x,input_coord,input_mask)
        out = self.first_conv_block_b(out,input_coord,input_mask)

        transformed_x = self.transformed_x(x,input_coord,input_mask)
        out += transformed_x
        out = self.elu(out)
        
        out = self.residual_block2(out,input_coord,input_mask)
        
        return out



class FeatureExtractorBlock(nn.Module):

    def __init__(self, in_channels, out_channels, stride=1,kernel_size=3,padding=1, n = 6,num_heads=4,kernel_func=Dynamic_conv2d):
        super(FeatureExtractorBlock, self).__init__()
        self.padding=1
        
        self.first_conv_block = kernel_func(in_channels=in_channels, out_channels=out_channels, stride=stride,kernel_size=kernel_size,
            # num_heads=num_heads
            )

        self.second_conv_block = kernel_func(in_channels=out_channels, out_channels=out_channels,kernel_size=kernel_size,
            # num_heads=num_heads,
                           padding=1,
                           stride=1
                           )

        self.transformed_x = kernel_func(in_channels=in_channels, out_channels=out_channels,kernel_size=kernel_size,
            # num_heads=num_heads,
                           )

        self.residual_block = kernel_func(in_channels=out_channels, out_channels=out_channels,kernel_size=kernel_size,
            # num_heads=num_heads,
                           padding=1,
                           stride=1
                           )
        
        self.number_of_blocks = n

        residual_block = [kernel_func(in_channels=out_channels,
                           out_channels=out_channels,
                           kernel_size= kernel_size,
                           ) for i in range(n)]
        self.residual_block = nn.ModuleList(residual_block)
        
        
    def forward(self, x,input_coord,input_mask):
        
        out = self.first_conv_block(x,input_coord,input_mask)
        out = self.second_conv_block(out,input_coord,input_mask)
        transformed_x = self.transformed_x(x,input_coord,input_mask)
        # print('transformed_x',transformed_x.size())
        # print('out',out.size())
        out = out+transformed_x

        for res_layer in self.residual_block:
            out = res_layer(out,input_coord,input_mask)

        return out



class FeatureAggregatorBlock(nn.Module):

    def __init__(self, in_channels_fine, in_channels_coarse, out_channels,kernel_size=3,n=4,kernel_func=Dynamic_conv2d):
        super(FeatureAggregatorBlock, self).__init__()

        self.in_channels_fine = in_channels_fine
        self.in_channels_coarse = in_channels_coarse
        self.out_channels = out_channels
        
        self.coarse_deconv = nn.ConvTranspose2d(in_channels=self.in_channels_coarse,
                                                out_channels=self.in_channels_coarse,
                                                kernel_size=(2, 2),
                                                stride=(2, 2))
        
        self.first_conv_block_a = kernel_func(in_channels=in_channels_fine + in_channels_coarse, out_channels=out_channels//2,
                           kernel_size=kernel_size,
                           padding=1,
                           stride=1
                           )

        self.first_conv_block_b = kernel_func(in_channels=out_channels//2, out_channels=out_channels,
                           kernel_size=kernel_size,
                           padding=1,
                           stride=1
                           )

        
        self.transformed_x = kernel_func(in_channels=in_channels_fine + in_channels_coarse,
                                        out_channels=out_channels,kernel_size=kernel_size)
        self.elu = nn.ELU()
        
        residual_block = [kernel_func(in_channels=out_channels,
                           out_channels=out_channels,
                           kernel_size= kernel_size,
                           ) for i in range(n)]
        self.residual_block2 = nn.ModuleList(residual_block)
        
        
    def forward(self, fine_x, coarse_x,input_coord,input_mask):
        coarse_x = self.coarse_deconv(coarse_x)

        x = torch.cat((fine_x, coarse_x), 1) 
        
        out = self.first_conv_block_a(x,input_coord,input_mask)
        out = self.first_conv_block_b(out,input_coord,input_mask)

        transformed_x = self.transformed_x(x,input_coord,input_mask)
        out = out + transformed_x
        out = self.elu(out)
        
        # out = self.residual_block2(out,input_coord,input_mask)
        for res_layer in self.residual_block2:
            out = res_layer(out,input_coord,input_mask)

        return out


class ResidualBlock(nn.Module):

    def __init__(self, in_channels, out_channels, stride=1,kernel_size=3,padding=1, n = 6,num_heads=4,kernel_func = nn.Conv2d):
        super(ResidualBlock, self).__init__()
        self.padding=1
        
        self.first_conv_block = kernel_func(in_channels=in_channels, out_channels=out_channels, stride=stride,kernel_size=kernel_size,
                # num_heads=num_heads
                )


        self.second_conv_block = kernel_func(in_channels=out_channels, out_channels=out_channels, stride=stride,kernel_size=kernel_size,
                # num_heads=num_heads
                )

        

        self.use_res_connect = stride == 1 and in_channels == out_channels

    def forward(self, x,input_coord,input_mask):

        out = self.first_conv_block(x,input_coord,input_mask)

        out = self.second_conv_block(out,input_coord,input_mask)


        if self.use_res_connect:
            out=out+x

        return out



class SimpleFeatureExtractorBlock(nn.Module):

    def __init__(self, in_channels, out_channels, stride=1,kernel_size=3,padding=1, n = 5,num_heads=4,kernel_func = nn.Conv2d):
        super(SimpleFeatureExtractorBlock, self).__init__()
        self.padding=1
        self.first_conv_block = ResidualBlock(in_channels=in_channels,
                           out_channels=out_channels,
                           kernel_size= kernel_size,
                           kernel_func = kernel_func
                           )

        residual_block = [ResidualBlock(in_channels=out_channels,
                           out_channels=out_channels,
                           kernel_size= kernel_size,
                           kernel_func = kernel_func
                           ) for i in range(n-1)]
        self.residual_block = nn.ModuleList(residual_block)


    def forward(self, x,input_coord,input_mask):
        
        x = self.first_conv_block(x,input_coord,input_mask)
        for res_layer in self.residual_block:
            x = res_layer(x,input_coord,input_mask)
        return x


# class DeepLayerAggregation(nn.Module):
#     def __init__(self, in_channels):
#         super(DeepLayerAggregation, self).__init__()


#         # self.fe1 = [kernel_func(in_channels=in_channels if i ==0 else 32,
#         #                    out_channels=32,
#         #                    kernel_size=3,
#         #                    padding=1,
#         #                    stride=1) for i in range(10)]
#         # self.fe1 = nn.ModuleList(self.fe1)
        
#         self.fe1 = FeatureExtractorBlock(in_channels=in_channels, out_channels=32,num_heads=1)
        
        
#         # self.fe2 = [kernel_func(in_channels=32,
#         #                                    out_channels=32,
#         #                                    kernel_size=3,
#         #                                    padding=1,
#         #                                    stride=1) for i in range(10)]
#         # self.fe2 = nn.ModuleList(self.fe2)
        
        
#         self.fe2 = FeatureExtractorBlock(in_channels=32, out_channels=32)
        
#         self.down_f2 = SmartDownSampling(32,32,kernel_size=2,stride=2)



#         # self.fe3 = [kernel_func(in_channels=32 if i ==0 else 64,
#         #                                    out_channels=64,
#         #                                    kernel_size=3,
#         #                                    padding=1,
#         #                                    stride=1) for i in range(10)]
#         # self.fe3 = nn.ModuleList(self.fe3)
        
        
#         self.fe3 = FeatureExtractorBlock(in_channels=32, out_channels=64)
        
        
        
#         self.down_f3 = SmartDownSampling(64,64,kernel_size=2,stride=2)
        
        
#         self.fa1 = FeatureAggregatorBlock(in_channels_fine=32,
#                                             in_channels_coarse=64,
#                                             out_channels=64)



#         # self.fe4 = [kernel_func(in_channels=64,
#         #                                    out_channels=64,
#         #                                    kernel_size=3,
#         #                                    padding=1,
#         #                                    stride=1) for i in range(10)]

#         # self.fe4 = nn.ModuleList(self.fe4)

#         self.fe4 = FeatureExtractorBlock(in_channels=64, out_channels=64)

#         # self.fe_2a = kernel_func(in_channels=64,
#         #                                    out_channels=64,
#         #                                    kernel_size=3,
#         #                                    padding=1,
#         #                                    stride=1)

#         # self.down_2a = SmartDownSampling(64,64,stride=2)
        
        
#         # self.fa_1b = FeatureAggregatorBlock(in_channels_fine=64,
#         #                                     in_channels_coarse=64,
#         #                                     out_channels=64)

#         # self.fe_3a = kernel_func(in_channels=64,
#         #                                    out_channels=64,
#         #                                    stride=1)


#     def forward(self, input,input_coord,input_mask):


#         input = F.pad(input,(0,2))
#         input_coord = F.pad(input_coord,(0,2))
#         input_mask = F.pad(input_mask,(0,2))
        
        
#         # print('input.size()',input.size())
#         # print('input_coord.size()',input_coord.size())
#         # print('input_mask.size()',input_mask.size())

#         x = input
#         # for f1_layer in self.fe1:
#         #     x = f1_layer(x,input_coord,input_mask)
#         x = self.fe1(x,input_coord,input_mask)


#         # print('out_1a.size()',out_1a.size())
#         # print('input_coord.size()',input_coord.size())
#         # print('input_mask.size()',input_mask.size())


#         # out2 = out1
#         # for f2_layer in self.fe2:
#         #     x = f2_layer(x,input_coord,input_mask)
#         x = self.fe2(x,input_coord,input_mask)

#         x,out2_coord,out2_mask = self.down_f2(x,input_coord,input_mask)


#         # x = avg_pool2d(x, kernel_size, stride=None, padding=0, ceil_mode=False, count_include_pad=True, divisor_override=None)


#         out2_down = x

#         # out3 = out2_down
#         # for f3_layer in self.fe3:
#         #     x = f3_layer(x,out2_coord,out2_mask)

#         x = self.fe3(x,out2_coord,out2_mask)
#         out3_down,out3_coord,out3_mask = self.down_f3(x,out2_coord,out2_mask)
        
        
#         # print('out2_down',out2_down.size())
#         # print('out3_down',out3_down.size())

#         x = self.fa1(out2_down, out3_down,out2_coord,out2_mask)

#         # print('out_fa1',x.size())

#         # out4 = out_fa1
#         # for f4_layer in self.fe4:
#         #     x = f4_layer(x,out2_coord,out2_mask)
#         x = self.fe4(x,out2_coord,out2_mask)
#         out4 = x[:,:,:,:-1]
#         # print('out4',out4.size())

#         # # print('out_1a_down.size()',out_1a_down.size())
#         # # print('out_1a_coord.size()',out_1a_coord.size())
#         # # print('out_1a_mask.size()',out_1a_mask.size())


#         # out_2a = self.fe_2a(out_1a_down,out_1a_coord,out_1a_mask)

#         # # out_2a_down = self.down_2a(out_2a)
#         # # print('out_2a_down.size()',out_2a_down.size())
        


#         # # if out_2a.size(-1)%2!=0:
#         # #     out_2a_crop,out_1a_coord_crop,out_1a_mask_crop = out_2a[:,:,:,:-1],out_1a_coord[:,:,:,:-1],out_1a_mask[:,:,:-1]
#         # #     # print('croped')
#         # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a_crop,out_1a_coord_crop,out_1a_mask_crop)
#         # # else:
#         # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        

#         # out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
#         # # print('out_2a_down.size()',out_2a_down.size())
#         # # print('out_2a_corrd.size()',out_2a_corrd.size())
#         # # print('out_2a_mask.size()',out_2a_mask.size())
        
#         # out_1b = self.fa_1b(out_1a_down, out_2a_down)
        
#         # # print('out_1b.size()',out_1b.size())
        
#         # # out_3a = self.fe_3a(out_2a,out_1a_coord,out_1a_mask)

#         # out_3a = self.fe_3a(out_1b,out_1a_coord,out_1a_mask)
#         # out_3a = out_3a[:,:,:,:-1]
#         # # print('out_3a.size()',out_3a.size())

#         return out4


class DeepLayerAggregation(nn.Module):
    def __init__(self, in_channels,kernel = Dynamic_conv2d):
        super(DeepLayerAggregation, self).__init__()

        self.kernel = Dynamic_conv2d
        # self.fe1 = [kernel_func(in_channels=in_channels if i ==0 else 32,
        #                    out_channels=32,
        #                    kernel_size=3,
        #                    padding=1,
        #                    stride=1) for i in range(10)]
        # self.fe1 = nn.ModuleList(self.fe1)
        
        self.fe1 = FeatureExtractorBlock(in_channels=in_channels, out_channels=32,num_heads=1,kernel_func=kernel)
        
        
        # self.fe2 = [kernel_func(in_channels=32,
        #                                    out_channels=32,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]
        # self.fe2 = nn.ModuleList(self.fe2)
        
        
        self.fe2 = FeatureExtractorBlock(in_channels=32, out_channels=32,kernel_func=kernel)
        
        self.down_f2 = SmartDownSampling(32,32,kernel_size=2,stride=2)



        # self.fe3 = [kernel_func(in_channels=32 if i ==0 else 64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]
        # self.fe3 = nn.ModuleList(self.fe3)
        
        
        self.fe3 = FeatureExtractorBlock(in_channels=32, out_channels=64,kernel_func=kernel)
        
        
        
        self.down_f3 = SmartDownSampling(64,64,kernel_size=2,stride=2)
        
        
        self.fa1 = FeatureAggregatorBlock(in_channels_fine=32,
                                            in_channels_coarse=64,
                                            out_channels=64,kernel_func=kernel)



        # self.fe4 = [kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]

        # self.fe4 = nn.ModuleList(self.fe4)

        self.fe4 = FeatureExtractorBlock(in_channels=64, out_channels=64,kernel_func=kernel)

        # self.fe_2a = kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1)

        # self.down_2a = SmartDownSampling(64,64,stride=2)
        
        
        # self.fa_1b = FeatureAggregatorBlock(in_channels_fine=64,
        #                                     in_channels_coarse=64,
        #                                     out_channels=64)

        # self.fe_3a = kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    stride=1)


    def forward(self, input,input_coord,input_mask):


        input = F.pad(input,(0,2))
        input_coord = F.pad(input_coord,(0,2))
        input_mask = F.pad(input_mask,(0,2))
        
        
        # print('input.size()',input.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())

        x = input
        # for f1_layer in self.fe1:
        #     x = f1_layer(x,input_coord,input_mask)
        x = self.fe1(x,input_coord,input_mask)


        # print('out_1a.size()',out_1a.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())


        # out2 = out1
        # for f2_layer in self.fe2:
        #     x = f2_layer(x,input_coord,input_mask)
        x = self.fe2(x,input_coord,input_mask)

        x,out2_coord,out2_mask = self.down_f2(x,input_coord,input_mask)
        # x_,out2_coord,out2_mask = None,None,None
        # x = F.avg_pool2d(x, 2, stride=2)
        # x = F.max_pool2d(x, 2, stride=2)


        # x = avg_pool2d(x, kernel_size, stride=None, padding=0, ceil_mode=False, count_include_pad=True, divisor_override=None)


        out2_down = x

        # out3 = out2_down
        # for f3_layer in self.fe3:
        #     x = f3_layer(x,out2_coord,out2_mask)

        x = self.fe3(x,out2_coord,out2_mask)
        out3_down,out3_coord,out3_mask = self.down_f3(x,out2_coord,out2_mask)
        
        # out3_down_,out3_coord,out3_mask = None,None,None
        # out3_down = F.avg_pool2d(x, 2, stride=2)
        # out3_down = F.max_pool2d(x, 2, stride=2)
        
        # print('out2_down',out2_down.size())
        # print('out3_down',out3_down.size())
        
        x = self.fa1(out2_down, out3_down,out2_coord,out2_mask)

        # print('out_fa1',x.size())

        # out4 = out_fa1
        # for f4_layer in self.fe4:
        #     x = f4_layer(x,out2_coord,out2_mask)
        x = self.fe4(x,out2_coord,out2_mask)
        out4 = x[:,:,:,:-1]
        # print('out4',out4.size())

        # # print('out_1a_down.size()',out_1a_down.size())
        # # print('out_1a_coord.size()',out_1a_coord.size())
        # # print('out_1a_mask.size()',out_1a_mask.size())


        # out_2a = self.fe_2a(out_1a_down,out_1a_coord,out_1a_mask)

        # # out_2a_down = self.down_2a(out_2a)
        # # print('out_2a_down.size()',out_2a_down.size())
        


        # # if out_2a.size(-1)%2!=0:
        # #     out_2a_crop,out_1a_coord_crop,out_1a_mask_crop = out_2a[:,:,:,:-1],out_1a_coord[:,:,:,:-1],out_1a_mask[:,:,:-1]
        # #     # print('croped')
        # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a_crop,out_1a_coord_crop,out_1a_mask_crop)
        # # else:
        # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        

        # out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        # # print('out_2a_down.size()',out_2a_down.size())
        # # print('out_2a_corrd.size()',out_2a_corrd.size())
        # # print('out_2a_mask.size()',out_2a_mask.size())
        
        # out_1b = self.fa_1b(out_1a_down, out_2a_down)
        
        # # print('out_1b.size()',out_1b.size())
        
        # # out_3a = self.fe_3a(out_2a,out_1a_coord,out_1a_mask)

        # out_3a = self.fe_3a(out_1b,out_1a_coord,out_1a_mask)
        # out_3a = out_3a[:,:,:,:-1]
        # # print('out_3a.size()',out_3a.size())

        return out4



class DeepLayerAggregationEdge(nn.Module):
    def __init__(self, in_channels,kernel = Dynamic_conv2d):
        super(DeepLayerAggregationEdge, self).__init__()


        self.kernel = Dynamic_conv2d
        # self.fe1 = [kernel_func(in_channels=in_channels if i ==0 else 32,
        #                    out_channels=32,
        #                    kernel_size=3,
        #                    padding=1,
        #                    stride=1) for i in range(10)]
        # self.fe1 = nn.ModuleList(self.fe1)
        
        self.fe1 = SimpleFeatureExtractorBlock(in_channels=in_channels, out_channels=32,num_heads=1,kernel_func=kernel)
        self.pfe1 = PolarMeanFeatureExtractor(in_channels,4, kernel_size=3,stride=1, padding=1,dilation=1)
        
        self.merge_conv = nn.Sequential(
          nn.BatchNorm2d(32+8),
          nn.Conv2d(32+8,32,kernel_size=1,stride=1, padding=0,dilation=1),
          nn.BatchNorm2d(32),
          nn.ReLU(inplace=True),
          )
        
        # self.fe2 = [kernel_func(in_channels=32,
        #                                    out_channels=32,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]
        # self.fe2 = nn.ModuleList(self.fe2)
        
        
        self.fe2 = SimpleFeatureExtractorBlock(in_channels=32, out_channels=32,kernel_func=kernel)
        
        self.down_f2 = SmartDownSampling(32,32,kernel_size=2,stride=2)
        
        # self.pfe2 = PolarFeatureExtractor(in_channels,8, kernel_size=3,stride=1, padding=1,dilation=1)
        
        # self.merge_conv2 = nn.Sequential(
        #   nn.BatchNorm2d(32+8),
        #   nn.Conv2d(32+8,32,kernel_size=1,stride=1, padding=0,dilation=1),
        #   nn.BatchNorm2d(32),
        #   nn.ReLU(inplace=True),
        #   )

        
        # self.fe3 = [kernel_func(in_channels=32 if i ==0 else 64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]
        # self.fe3 = nn.ModuleList(self.fe3)
        
        
        self.fe3 = SimpleFeatureExtractorBlock(in_channels=32, out_channels=64,kernel_func=kernel)
        
        
        
        self.down_f3 = SmartDownSampling(64,64,kernel_size=2,stride=2)
        
        # self.pfe3 = PolarFeatureExtractor(in_channels,8, kernel_size=3,stride=1, padding=1,dilation=1)
        
        # self.merge_conv3 = nn.Sequential(
        #   nn.BatchNorm2d(64+8),
        #   nn.Conv2d(64+8,64,kernel_size=1,stride=1, padding=0,dilation=1),
        #   nn.BatchNorm2d(64),
        #   nn.ReLU(inplace=True),
        #   )


        self.fa1 = FeatureAggregatorBlock(in_channels_fine=32,
                                            in_channels_coarse=64,
                                            out_channels=64,kernel_func=kernel)



        # self.fe4 = [kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]

        # self.fe4 = nn.ModuleList(self.fe4)
        
        self.fe4 = SimpleFeatureExtractorBlock(in_channels=64, out_channels=64,kernel_func=EdgeConv)
        
        # self.fe_2a = kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1)

        # self.down_2a = SmartDownSampling(64,64,stride=2)
        
        
        # self.fa_1b = FeatureAggregatorBlock(in_channels_fine=64,
        #                                     in_channels_coarse=64,
        #                                     out_channels=64)

        # self.fe_3a = kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    stride=1)


    def forward(self, input,input_coord,input_mask):


        input = F.pad(input,(0,2))
        input_coord = F.pad(input_coord,(0,2))
        input_mask = F.pad(input_mask,(0,2))
        
        
        # print('input.size()',input.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())

        x = input


        # for f1_layer in self.fe1:
        #     x = f1_layer(x,input_coord,input_mask)
        x = self.fe1(x,input_coord,input_mask)
        # x = x+self.pfe1(x,input_coord,input_mask)
        x_pfe = self.pfe1(x,input_coord,input_mask)
        x = torch.cat([x,x_pfe],1)
        
        x = self.merge_conv(x)
        
        
        # print('out_1a.size()',out_1a.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())


        # out2 = out1
        # for f2_layer in self.fe2:
        #     x = f2_layer(x,input_coord,input_mask)
        x = self.fe2(x,input_coord,input_mask)
        
        x,out2_coord,out2_mask = self.down_f2(x,input_coord,input_mask)
        # x_,out2_coord,out2_mask = None,None,None
        # x = F.avg_pool2d(x, 2, stride=2)
        # x = F.max_pool2d(x, 2, stride=2)

        # x_pfe2 = self.pfe2(x,out2_coord,out2_mask)
        # x = torch.cat([x,x_pfe2],1)
        
        # x = self.merge_conv2(x)

        
        
        # x = avg_pool2d(x, kernel_size, stride=None, padding=0, ceil_mode=False, count_include_pad=True, divisor_override=None)
        
        
        out2_down = x

        # out3 = out2_down
        # for f3_layer in self.fe3:
        #     x = f3_layer(x,out2_coord,out2_mask)

        x = self.fe3(x,out2_coord,out2_mask)
        out3_down,out3_coord,out3_mask = self.down_f3(x,out2_coord,out2_mask)
        
        # out3_down_,out3_coord,out3_mask = None,None,None
        # out3_down = F.avg_pool2d(x, 2, stride=2)
        # out3_down = F.max_pool2d(x, 2, stride=2)
        
        # print('out2_down',out2_down.size())
        # print('out3_down',out3_down.size())
        
        x = self.fa1(out2_down, out3_down,out2_coord,out2_mask)

        # print('out_fa1',x.size())

        # out4 = out_fa1
        # for f4_layer in self.fe4:
        #     x = f4_layer(x,out2_coord,out2_mask)
        x = self.fe4(x,out2_coord,out2_mask)

        # x = torch.cat([x,x_pfe2],1)
        
        # x = self.merge_conv3(x)


        out4 = x[:,:,:,:-1]
        # print('out4',out4.size())

        # # print('out_1a_down.size()',out_1a_down.size())
        # # print('out_1a_coord.size()',out_1a_coord.size())
        # # print('out_1a_mask.size()',out_1a_mask.size())


        # out_2a = self.fe_2a(out_1a_down,out_1a_coord,out_1a_mask)

        # # out_2a_down = self.down_2a(out_2a)
        # # print('out_2a_down.size()',out_2a_down.size())
        


        # # if out_2a.size(-1)%2!=0:
        # #     out_2a_crop,out_1a_coord_crop,out_1a_mask_crop = out_2a[:,:,:,:-1],out_1a_coord[:,:,:,:-1],out_1a_mask[:,:,:-1]
        # #     # print('croped')
        # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a_crop,out_1a_coord_crop,out_1a_mask_crop)
        # # else:
        # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        

        # out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        # # print('out_2a_down.size()',out_2a_down.size())
        # # print('out_2a_corrd.size()',out_2a_corrd.size())
        # # print('out_2a_mask.size()',out_2a_mask.size())
        
        # out_1b = self.fa_1b(out_1a_down, out_2a_down)
        
        # # print('out_1b.size()',out_1b.size())
        
        # # out_3a = self.fe_3a(out_2a,out_1a_coord,out_1a_mask)

        # out_3a = self.fe_3a(out_1b,out_1a_coord,out_1a_mask)
        # out_3a = out_3a[:,:,:,:-1]
        # # print('out_3a.size()',out_3a.size())

        return out4


class DeepLayerAggregation(nn.Module):
    def __init__(self, in_channels,kernel = Dynamic_conv2d):
        super(DeepLayerAggregation, self).__init__()


        #self.kernel = Dynamic_conv2d
        # self.fe1 = [kernel_func(in_channels=in_channels if i ==0 else 32,
        #                    out_channels=32,
        #                    kernel_size=3,
        #                    padding=1,
        #                    stride=1) for i in range(10)]
        # self.fe1 = nn.ModuleList(self.fe1)
        

        self.fe1 = FeatureExtractorBlock(in_channels=in_channels, out_channels=32,num_heads=1,kernel_func=kernel)
        self.pfe1 = PolarMeanFeatureExtractor(in_channels,4, kernel_size=3,stride=1, padding=1,dilation=1)
        
        self.merge_conv = nn.Sequential(
          nn.BatchNorm2d(32+8),
          nn.Conv2d(32+8,32,kernel_size=1,stride=1, padding=0,dilation=1),
          nn.BatchNorm2d(32),
          nn.ReLU(inplace=True),
          )
        
        # self.fe2 = [kernel_func(in_channels=32,
        #                                    out_channels=32,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]
        # self.fe2 = nn.ModuleList(self.fe2)
        
        
        self.fe2 = FeatureExtractorBlock(in_channels=32, out_channels=32,kernel_func=kernel)
        
        self.down_f2 = SmartDownSampling(32,32,kernel_size=2,stride=2)
        
        # self.pfe2 = PolarFeatureExtractor(in_channels,8, kernel_size=3,stride=1, padding=1,dilation=1)
        
        # self.merge_conv2 = nn.Sequential(
        #   nn.BatchNorm2d(32+8),
        #   nn.Conv2d(32+8,32,kernel_size=1,stride=1, padding=0,dilation=1),
        #   nn.BatchNorm2d(32),
        #   nn.ReLU(inplace=True),
        #   )

        
        # self.fe3 = [kernel_func(in_channels=32 if i ==0 else 64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]
        # self.fe3 = nn.ModuleList(self.fe3)
        
        
        self.fe3 = FeatureExtractorBlock(in_channels=32, out_channels=64,kernel_func=kernel)
        
        
        
        self.down_f3 = SmartDownSampling(64,64,kernel_size=2,stride=2)
        
        # self.pfe3 = PolarFeatureExtractor(in_channels,8, kernel_size=3,stride=1, padding=1,dilation=1)
        
        # self.merge_conv3 = nn.Sequential(
        #   nn.BatchNorm2d(64+8),
        #   nn.Conv2d(64+8,64,kernel_size=1,stride=1, padding=0,dilation=1),
        #   nn.BatchNorm2d(64),
        #   nn.ReLU(inplace=True),
        #   )


        self.fa1 = FeatureAggregatorBlock(in_channels_fine=32,
                                            in_channels_coarse=64,
                                            out_channels=64,kernel_func=kernel)



        # self.fe4 = [kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1) for i in range(10)]

        # self.fe4 = nn.ModuleList(self.fe4)

        self.fe4 = FeatureExtractorBlock(in_channels=64, out_channels=64,kernel_func=kernel)

        # self.fe_2a = kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    kernel_size=3,
        #                                    padding=1,
        #                                    stride=1)

        # self.down_2a = SmartDownSampling(64,64,stride=2)
        
        
        # self.fa_1b = FeatureAggregatorBlock(in_channels_fine=64,
        #                                     in_channels_coarse=64,
        #                                     out_channels=64)

        # self.fe_3a = kernel_func(in_channels=64,
        #                                    out_channels=64,
        #                                    stride=1)


    def forward(self, input,input_coord,input_mask):


        input = F.pad(input,(0,2))
        input_coord = F.pad(input_coord,(0,2))
        input_mask = F.pad(input_mask,(0,2))
        
        
        # print('input.size()',input.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())

        x = input


        # for f1_layer in self.fe1:
        #     x = f1_layer(x,input_coord,input_mask)
        x = self.fe1(x,input_coord,input_mask)
        x_pfe = self.pfe1(x,input_coord,input_mask)
        x = torch.cat([x,x_pfe],1)
        
        x = self.merge_conv(x)
        
        
        # print('out_1a.size()',out_1a.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())


        # out2 = out1
        # for f2_layer in self.fe2:
        #     x = f2_layer(x,input_coord,input_mask)
        x = self.fe2(x,input_coord,input_mask)
        
        x,out2_coord,out2_mask = self.down_f2(x,input_coord,input_mask)
        # x_,out2_coord,out2_mask = None,None,None
        # x = F.avg_pool2d(x, 2, stride=2)
        # x = F.max_pool2d(x, 2, stride=2)

        # x_pfe2 = self.pfe2(x,out2_coord,out2_mask)
        # x = torch.cat([x,x_pfe2],1)
        
        # x = self.merge_conv2(x)

        
        
        # x = avg_pool2d(x, kernel_size, stride=None, padding=0, ceil_mode=False, count_include_pad=True, divisor_override=None)
        
        
        out2_down = x

        # out3 = out2_down
        # for f3_layer in self.fe3:
        #     x = f3_layer(x,out2_coord,out2_mask)

        x = self.fe3(x,out2_coord,out2_mask)
        out3_down,out3_coord,out3_mask = self.down_f3(x,out2_coord,out2_mask)
        
        # out3_down_,out3_coord,out3_mask = None,None,None
        # out3_down = F.avg_pool2d(x, 2, stride=2)
        # out3_down = F.max_pool2d(x, 2, stride=2)
        
        # print('out2_down',out2_down.size())
        # print('out3_down',out3_down.size())
        
        x = self.fa1(out2_down, out3_down,out2_coord,out2_mask)

        # print('out_fa1',x.size())

        # out4 = out_fa1
        # for f4_layer in self.fe4:
        #     x = f4_layer(x,out2_coord,out2_mask)
        x = self.fe4(x,out2_coord,out2_mask)

        # x = torch.cat([x,x_pfe2],1)
        
        # x = self.merge_conv3(x)


        out4 = x[:,:,:,:-1]
        # print('out4',out4.size())

        # # print('out_1a_down.size()',out_1a_down.size())
        # # print('out_1a_coord.size()',out_1a_coord.size())
        # # print('out_1a_mask.size()',out_1a_mask.size())


        # out_2a = self.fe_2a(out_1a_down,out_1a_coord,out_1a_mask)

        # # out_2a_down = self.down_2a(out_2a)
        # # print('out_2a_down.size()',out_2a_down.size())
        


        # # if out_2a.size(-1)%2!=0:
        # #     out_2a_crop,out_1a_coord_crop,out_1a_mask_crop = out_2a[:,:,:,:-1],out_1a_coord[:,:,:,:-1],out_1a_mask[:,:,:-1]
        # #     # print('croped')
        # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a_crop,out_1a_coord_crop,out_1a_mask_crop)
        # # else:
        # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        

        # out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        # # print('out_2a_down.size()',out_2a_down.size())
        # # print('out_2a_corrd.size()',out_2a_corrd.size())
        # # print('out_2a_mask.size()',out_2a_mask.size())
        
        # out_1b = self.fa_1b(out_1a_down, out_2a_down)
        
        # # print('out_1b.size()',out_1b.size())
        
        # # out_3a = self.fe_3a(out_2a,out_1a_coord,out_1a_mask)

        # out_3a = self.fe_3a(out_1b,out_1a_coord,out_1a_mask)
        # out_3a = out_3a[:,:,:,:-1]
        # # print('out_3a.size()',out_3a.size())

        return out4





class DeepLayerSimpleAggregation(nn.Module):
    def __init__(self, in_channels,kernel = Dynamic_conv2d):
        super(DeepLayerSimpleAggregation, self).__init__()


        self.kernel = Dynamic_conv2d


        self.fe1 = SimpleFeatureExtractorBlock(in_channels=in_channels, out_channels=32,num_heads=1,kernel_func=kernel)
        
        self.pfe1 = PolarMeanFeatureExtractor(in_channels,4, kernel_size=3,stride=1, padding=1,dilation=1)
        
        self.merge_conv = nn.Sequential(
          nn.BatchNorm2d(32+8),
          nn.Conv2d(32+8,32,kernel_size=1,stride=1, padding=0,dilation=1),
          nn.BatchNorm2d(32),
          nn.ReLU(inplace=True),
          )

        
        
        self.fe2 = SimpleFeatureExtractorBlock(in_channels=32, out_channels=32,kernel_func=kernel)
        
        self.down_f2 = SmartDownSampling(32,32,kernel_size=2,stride=2)
 
        
        self.fe3 = SimpleFeatureExtractorBlock(in_channels=32, out_channels=64,kernel_func=kernel)
        
        
        
        self.down_f3 = SmartDownSampling(64,64,kernel_size=2,stride=2)
        
 
        self.fa1 = FeatureAggregatorBlock(in_channels_fine=32,
                                            in_channels_coarse=64,
                                            out_channels=64,kernel_func=kernel)


        self.fe4 = SimpleFeatureExtractorBlock(in_channels=64, out_channels=64,kernel_func=kernel)


    def forward(self, input,input_coord,input_mask):


        input = F.pad(input,(0,2))
        input_coord = F.pad(input_coord,(0,2))
        input_mask = F.pad(input_mask,(0,2))
        

        x = input
        

        x = self.fe1(x,input_coord,input_mask)
        x_pfe = self.pfe1(x,input_coord,input_mask)
        x = torch.cat([x,x_pfe],1)
        
        x = self.merge_conv(x)
        
        
        # print('out_1a.size()',out_1a.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())


        # out2 = out1
        # for f2_layer in self.fe2:
        #     x = f2_layer(x,input_coord,input_mask)
        x = self.fe2(x,input_coord,input_mask)
        
        x,out2_coord,out2_mask = self.down_f2(x,input_coord,input_mask)
        # x_,out2_coord,out2_mask = None,None,None
        # x = F.avg_pool2d(x, 2, stride=2)
        # x = F.max_pool2d(x, 2, stride=2)

        # x_pfe2 = self.pfe2(x,out2_coord,out2_mask)
        # x = torch.cat([x,x_pfe2],1)
        
        # x = self.merge_conv2(x)

        
        
        # x = avg_pool2d(x, kernel_size, stride=None, padding=0, ceil_mode=False, count_include_pad=True, divisor_override=None)
        
        
        out2_down = x

        # out3 = out2_down
        # for f3_layer in self.fe3:
        #     x = f3_layer(x,out2_coord,out2_mask)

        x = self.fe3(x,out2_coord,out2_mask)
        out3_down,out3_coord,out3_mask = self.down_f3(x,out2_coord,out2_mask)
        
        # out3_down_,out3_coord,out3_mask = None,None,None
        # out3_down = F.avg_pool2d(x, 2, stride=2)
        # out3_down = F.max_pool2d(x, 2, stride=2)
        
        # print('out2_down',out2_down.size())
        # print('out3_down',out3_down.size())
        
        x = self.fa1(out2_down, out3_down,out2_coord,out2_mask)

        # print('out_fa1',x.size())

        # out4 = out_fa1
        # for f4_layer in self.fe4:
        #     x = f4_layer(x,out2_coord,out2_mask)
        x = self.fe4(x,out2_coord,out2_mask)

        # x = torch.cat([x,x_pfe2],1)
        
        # x = self.merge_conv3(x)


        out4 = x[:,:,:,:-1]
        # print('out4',out4.size())

        # # print('out_1a_down.size()',out_1a_down.size())
        # # print('out_1a_coord.size()',out_1a_coord.size())
        # # print('out_1a_mask.size()',out_1a_mask.size())


        # out_2a = self.fe_2a(out_1a_down,out_1a_coord,out_1a_mask)

        # # out_2a_down = self.down_2a(out_2a)
        # # print('out_2a_down.size()',out_2a_down.size())
        


        # # if out_2a.size(-1)%2!=0:
        # #     out_2a_crop,out_1a_coord_crop,out_1a_mask_crop = out_2a[:,:,:,:-1],out_1a_coord[:,:,:,:-1],out_1a_mask[:,:,:-1]
        # #     # print('croped')
        # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a_crop,out_1a_coord_crop,out_1a_mask_crop)
        # # else:
        # #     out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        

        # out_2a_down,out_2a_corrd,out_2a_mask = self.down_2a(out_2a,out_1a_coord,out_1a_mask)
        # # print('out_2a_down.size()',out_2a_down.size())
        # # print('out_2a_corrd.size()',out_2a_corrd.size())
        # # print('out_2a_mask.size()',out_2a_mask.size())
        
        # out_1b = self.fa_1b(out_1a_down, out_2a_down)
        
        # # print('out_1b.size()',out_1b.size())
        
        # # out_3a = self.fe_3a(out_2a,out_1a_coord,out_1a_mask)

        # out_3a = self.fe_3a(out_1b,out_1a_coord,out_1a_mask)
        # out_3a = out_3a[:,:,:,:-1]
        # # print('out_3a.size()',out_3a.size())

        return out4
