import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F






def conv_bn(inp, oup, stride, conv_layer=nn.Conv2d, norm_layer=nn.BatchNorm2d, nlin_layer=nn.ReLU):
    return nn.Sequential(
        conv_layer(inp, oup, 3, stride, 1, bias=False),
        norm_layer(oup),
        nlin_layer(inplace=True)
    )


def conv_1x1_bn(inp, oup, conv_layer=nn.Conv2d, norm_layer=nn.BatchNorm2d, nlin_layer=nn.ReLU):
    return nn.Sequential(
        conv_layer(inp, oup, 1, 1, 0, bias=False),
        norm_layer(oup),
        nlin_layer(inplace=True)
    )


class Hswish(nn.Module):
    def __init__(self, inplace=True):
        super(Hswish, self).__init__()
        self.inplace = inplace
        
    def forward(self, x):
        return x * F.relu6(x + 3., inplace=self.inplace) / 6.


class Hsigmoid(nn.Module):
    def __init__(self, inplace=True):
        super(Hsigmoid, self).__init__()
        self.inplace = inplace

    def forward(self, x):
        return F.relu6(x + 3., inplace=self.inplace) / 6.


class SEModule(nn.Module):
    def __init__(self, channel, reduction=4):
        super(SEModule, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(
            nn.Linear(channel, channel // reduction, bias=False),
            nn.ReLU(inplace=True),
            nn.Linear(channel // reduction, channel, bias=False),
            Hsigmoid()
            # nn.Sigmoid()
        )

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y.expand_as(x)


class Identity(nn.Module):
    def __init__(self, channel):
        super(Identity, self).__init__()

    def forward(self, x):
        return x


def make_divisible(x, divisible_by=8):
    import numpy as np
    return int(np.ceil(x * 1. / divisible_by) * divisible_by)


class LightweightConv2d(nn.Module):
    '''Lightweight Convolution assuming the input is BxCxhxw
    This is just an example that explains LightConv clearer than the TBC version.
    We don't use this module in the model.
    Args:
        input_size: # of channels of the input and output
        kernel_size: convolution channels
        padding: padding
        num_heads: number of heads used. The weight is of shape
            `(num_heads, 1, kernel_size)`
        weight_softmax: normalize the weight with softmax before the convolution
    Shape:
        Input: BxCxhxw, i.e. (batch_size, input_size, h,w)
        Output: BxCxhxw, i.e. (batch_size, input_size, h,w)
    Attributes:
        weight: the learnable weights of the module of shape
            `(num_heads, 1, kernel_size)`
        bias: the learnable bias of the module of shape `(input_size)`
    '''
    
    def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,padding_mode='zeros',
                 dilation = 1,groups=1, bias=True,num_heads=4,weight_softmax=True, weight_dropout=0.,):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size

        if in_channels%num_heads!=0:
            num_heads = 1

        self.num_heads = num_heads
        self.padding = padding
        self.stride = stride
        self.weight_softmax = weight_softmax
        self.weight = nn.Parameter(torch.Tensor(num_heads, 1, kernel_size,kernel_size))
        
        self.dilation = dilation
        if bias:
            self.bias = nn.Parameter(torch.Tensor(in_channels))
        else:
            self.bias = None
        self.weight_dropout = weight_dropout
        
        
        self.out = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=1,padding=0),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            # nn.Conv2d(out_channels, out_channels, kernel_size=1,padding=0),
            # nn.ReLU(inplace=True),
            )
        self.use_res_connect = stride == 1 and in_channels == out_channels
        
        self.reset_parameters()
        
        
    def reset_parameters(self):
        nn.init.xavier_uniform_(self.weight)
        if self.bias is not None:
            nn.init.constant_(self.bias, 0.)

    def forward(self, input,input_coord=None,input_mask=None):
        '''
        input size: B x C x h x w
        output size: B x C x h x w
        '''

        B, C, h, w = input.size()


        # print('input',input.size())

        H = self.num_heads

        weight = self.weight
        if self.weight_softmax:
            weight = weight.view((self.num_heads, 1, self.kernel_size*self.kernel_size))
            weight = F.softmax(weight,dim=2)
            weight = weight.view((self.num_heads, 1, self.kernel_size,self.kernel_size))
            
        # print('bias',self.bias.size())
        weight = F.dropout(weight, self.weight_dropout, training=self.training)
        # Merge every C/H entries into the batch dimension (C = self.in_channels)
        # B x C x h x w -> (B * C/H) x H x h x w
        # One can also expand the weight to C x 1 x K by a factor of C/H
        # and do not reshape the input instead, which is slow though
        input = input.view(-1, H, h,w)

        output = F.conv2d(input, weight, padding=self.padding, groups=self.num_heads,stride=self.stride,dilation=self.dilation)
        

        _,_,new_h,new_w = output.size()
                
        output = output.view(B, C, new_h,new_w)

        if self.bias is not None:
            output = output + self.bias.view(1, -1, 1, 1)

        output = self.out(output)

        if len(input_mask.size())==3:
            input_mask = input_mask.unsqueeze(1)


        if self.use_res_connect:
            input = input.view(B, C, h, w )
            output = input+output
        
        
        return output



from torch import Tensor
from typing import Optional, List, Tuple, Union

class BNReLUConv2d(nn.Module):

    def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,padding_mode='zeros',
                 dilation = 1,groups=1, bias=True,num_heads=4,weight_softmax=True, weight_dropout=0.,):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.num_heads = num_heads
        self.padding = padding
        self.stride = stride
        self.weight_softmax = weight_softmax
        self.weight = nn.Parameter(torch.Tensor(num_heads, 1, kernel_size,kernel_size))
        
        self.dilation = dilation
        if bias:
            self.bias = nn.Parameter(torch.Tensor(in_channels))
        else:
            self.bias = None
        self.weight_dropout = weight_dropout
        
        
        self.out = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size,padding=padding,stride=stride,dilation=dilation),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            )
        self.use_res_connect = stride == 1 and in_channels == out_channels
        


    def forward(self, input,input_coord=None,input_mask=None):
        out = self.out(input)
        # if self.use_res_connect:
        #     out = out+input
        return out



class attention2d(nn.Module):
    def __init__(self, in_channels, ratios, K, temperature, init_weight=True):
        super(attention2d, self).__init__()
        assert temperature%3==1
        self.avgpool = nn.AdaptiveAvgPool2d(1)
        if in_channels!=3:
            hidden_planes = int(in_channels*ratios)+1
        else:
            hidden_planes = K
        self.fc1 = nn.Conv2d(in_channels, hidden_planes, 1, bias=False)
        # self.bn = nn.BatchNorm2d(hidden_planes)
        self.fc2 = nn.Conv2d(hidden_planes, K, 1, bias=True)
        self.relu = nn.ReLU(inplace=True)
        self.temperature = temperature
        if init_weight:
            self._initialize_weights()


    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            if isinstance(m ,nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def updata_temperature(self):
        if self.temperature!=1:
            self.temperature -=3
            print('Change temperature to:', str(self.temperature))


    def forward(self, x):
        x = self.avgpool(x)
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x).view(x.size(0), -1)
        return F.softmax(x/self.temperature, 1)


class Dynamic_conv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1, padding=1, dilation=1, groups=1, bias=True, K=2, ratio=0.25,temperature=34, init_weight=True):
        super(Dynamic_conv2d, self).__init__()
        assert in_channels%groups==0
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.dilation = dilation
        self.groups = groups
        self.bias = bias
        self.K = K
        self.attention = attention2d(in_channels, ratio, K, temperature)

        self.attention_window = attention2d(in_channels, ratio, kernel_size*kernel_size, temperature)

        self.out = nn.Sequential(
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
            )

        if in_channels%groups!=0:
            groups = 1
            
        self.weight = nn.Parameter(torch.randn(K, out_channels, in_channels//groups, kernel_size, kernel_size), requires_grad=True)
        if bias:
            self.bias = nn.Parameter(torch.Tensor(K, out_channels))
        else:
            self.bias = None
        if init_weight:
            self._initialize_weights()

        self.use_res_connect = stride == 1 and in_channels == out_channels

        #TODO 初始化
    def _initialize_weights(self):
        for i in range(self.K):
            nn.init.kaiming_uniform_(self.weight[i])
            # nn.init.kaiming_uniform_(self.weight[i], mode='fan_out', nonlinearity='relu')



    def update_temperature(self):
        self.attention.updata_temperature()

    def forward(self, input,input_coord=None,input_mask=None):#将batch视作维度变量，进行组卷积，因为组卷积的权重是不同的，动态卷积的权重也是不同的

        
        softmax_attention = self.attention(input)
        softmax_attention_window = self.attention_window(input)

        batch_size, in_channels, height, width = input.size()
        # print('x',x.size())
        
        x = input.view(1, -1, height, width)# 变化成一个维度进行组卷积
        weight = self.weight.view(self.K, -1)
        

        
        # 动态卷积的权重的生成， 生成的是batch_size个卷积参数（每个参数不同）
        aggregate_weight = torch.mm(softmax_attention, weight)

        aggregate_weight = aggregate_weight.view(batch_size,-1,self.in_channels, self.kernel_size*self.kernel_size)
        # print('aggregate_weight reshape',aggregate_weight.size())
        aggregate_weight = torch.einsum('abcd,ad->abcd',[aggregate_weight, softmax_attention_window])#.view(B,-1,self.kernel_size*self.kernel_size,h,w)
        # print('aggregate_weight softmaxed',aggregate_weight.size())
        
        aggregate_weight = aggregate_weight.view(-1, self.in_channels, self.kernel_size, self.kernel_size)

        
        if self.bias is not None:
            aggregate_bias = torch.mm(softmax_attention, self.bias).view(-1)
            output = F.conv2d(x, weight=aggregate_weight, bias=aggregate_bias, stride=self.stride, padding=self.padding,
                              dilation=self.dilation, groups=self.groups*batch_size)
        else:
            output = F.conv2d(x, weight=aggregate_weight, bias=None, stride=self.stride, padding=self.padding,
                              dilation=self.dilation, groups=self.groups * batch_size)

        output = output.view(batch_size, self.out_channels, output.size(-2), output.size(-1))

        output = self.out(output)


        center_size = self.kernel_size//2


        if self.use_res_connect:
            input = input.view(batch_size, in_channels, height, width)
            output = input+output
        
        return output

class PFNLayer(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 use_norm=True,
                 last_layer=False):
        super().__init__()
        
        self.last_vfe = last_layer
        self.use_norm = use_norm
        if not self.last_vfe:
            out_channels = out_channels // 2

        if self.use_norm:
            self.linear = nn.Linear(in_channels, out_channels, bias=False)
            self.norm = nn.BatchNorm1d(out_channels, eps=1e-3, momentum=0.01)
        else:
            self.linear = nn.Linear(in_channels, out_channels, bias=True)

        self.part = 50000

    def forward(self, inputs):
        if inputs.shape[0] > self.part:
            # nn.Linear performs randomly when batch size is too large
            num_parts = inputs.shape[0] // self.part
            part_linear_out = [self.linear(inputs[num_part*self.part:(num_part+1)*self.part])
                               for num_part in range(num_parts+1)]
            x = torch.cat(part_linear_out, dim=0)
        else:
            x = self.linear(inputs)
        torch.backends.cudnn.enabled = False

        x = self.norm(x.permute(0, 2, 1)).permute(0, 2, 1) if self.use_norm else x
        torch.backends.cudnn.enabled = True
        x = F.relu(x)
        x_max = torch.max(x, dim=1, keepdim=True)[0]

        if self.last_vfe:
            return x_max
        else:
            x_repeat = x_max.repeat(1, inputs.shape[1], 1)
            x_concatenated = torch.cat([x, x_repeat], dim=2)
            return x_concatenated

# class EdgeConv(nn.Module):
#     def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,dilation=1
#                      ):
        
#         super().__init__()
#         self.in_channels = in_channels
#         self.out_channels = out_channels
#         self.kernel_size = kernel_size
#         self.padding = padding
#         self.stride = stride
#         self.dilation = dilation

#         exp = 3+self.in_channels+self.in_channels
#         self.exp = exp
#         self.mlp = nn.Sequential(
#             nn.Linear(exp,out_channels),
#             nn.ReLU(inplace=True),
#             # nn.Linear(out_channels,out_channels),
#             # nn.ReLU(inplace=True),
#             )

#         self.use_res_connect = stride == 1 and in_channels == out_channels




#     def reset_parameters(self):
#         nn.init.xavier_uniform_(self.weight)
#         if self.bias is not None:
#             nn.init.constant_(self.bias, 0.)





#     def forward(self, input,input_coord,input_mask=None):
        
#         '''
#         input size: B x D x h x w
#         input_coord size: B x 3 x h x w 
#         input_mask size: B x h x w 
#         output size: B x D' x h x w
#         '''
        
#         B, C, w, h = input.size()
#         center_size = int(self.kernel_size-1/2) 
        
#         input_unfold = torch.nn.functional.unfold(input,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation).view(B,C,self.kernel_size,self.kernel_size,-1)
#         _,_,_,_,n_windows = input_unfold.size()
#         # print('input_unfold.size()',input_unfold.size())
#         input_centers = input_unfold[:,:,center_size:center_size+1,center_size:center_size+1,:]
#         input_centers = input_centers.repeat(1,1,self.kernel_size,self.kernel_size, 1)
#         # print('input_centers.size()',input_centers.size())


#         # unfold to perform neighourhoods calculation
#         input_coord_unfold = torch.nn.functional.unfold(input_coord,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)

#         # print('input_coord_unfold.size()',input_coord_unfold.size())

#         input_coord_unfold = input_coord_unfold.view(B,3,self.kernel_size,self.kernel_size,-1)
#         # print('input_coord_unfold_expend.size() ',input_coord_unfold.size())
        
        
#         centers = input_coord_unfold[:,:,center_size:center_size+1,center_size:center_size+1,:]
#         # print('centers.size()',centers.size())
        
#         # theta_neighbour = input_coord_unfold[:,0,:,:]
#         # phi_neighbour = input_coord_unfold[:,1,:,:]
#         # r_neighbour = input_coord_unfold[:,2,:,:]


#         # theta_center = centers[:,0,:,:]
#         # phi_center = centers[:,1,:,:]
#         # r_center = centers[:,2,:,:]


#         # delta_theta = input_coord_unfold[:,0,:,:]-centers[:,0,:,:]
#         # delta_phi = input_coord_unfold[:,1,:,:]-centers[:,1,:,:]


#         delta = (input_coord_unfold-centers)
#         delta_theta = delta[:,0,:,:]
#         delta_phi = delta[:,1,:,:]
        
        
#         # asymmetric positional encoding
#         ase_0 = input_coord_unfold[:,2,:,:]*torch.cos(delta_theta)*torch.cos(delta_phi)-centers[:,2,:,:]
#         ase_1 = input_coord_unfold[:,2,:,:]*torch.cos(delta_theta)*torch.sin(delta_phi)
#         ase_2 = input_coord_unfold[:,2,:,:]*torch.sin(delta_theta)
        
#         # print('ase_0.size()',ase_0.size())
#         # print('ase_1.size()',ase_0.size())
#         # print('ase_2.size()',ase_0.size())
        
#         features = torch.cat([
#             ase_0.view(B,-1,self.kernel_size*self.kernel_size*n_windows).permute(0,2,1),
#             ase_1.view(B,-1,self.kernel_size*self.kernel_size*n_windows).permute(0,2,1),
#             ase_2.view(B,-1,self.kernel_size*self.kernel_size*n_windows).permute(0,2,1),
#             input_unfold.view(B,-1,self.kernel_size*self.kernel_size*n_windows).permute(0,2,1),
#             input_centers.view(B,-1,self.kernel_size*self.kernel_size*n_windows).permute(0,2,1),
#             ],-1)
        
#         # print('features.size()',features.size())
        
#         out = self.mlp(features).contiguous().permute(0,2,1).view(B,-1,self.kernel_size*self.kernel_size,w,h)
        
#         # merge mask for valid pixels
#         if input_mask is not None:
#             input_mask = input_mask.view(B,1,w,h) # B x 1 x h x w
#             # print('input_mask.size()',input_mask.size())
#             # unfold to perform neighourhoods calculation
#             input_mask_unfold = torch.nn.functional.unfold(input_mask,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)
            
#             # print('input_mask_unfold.size()',input_mask_unfold.size())
            
#             input_mask_unfold = input_mask_unfold.view(B,self.kernel_size,self.kernel_size,-1)
#             # print('input_mask_unfold_expend.size() ',input_mask_unfold.size())
            
#             centers = input_mask_unfold[:,center_size:center_size+1,center_size:center_size+1,:]
#             # print('centers.size()',centers.size())
            
#             input_mask_unfold = input_mask_unfold * centers
#             input_mask_unfold = input_mask_unfold.view(B,-1,self.kernel_size*self.kernel_size,w,h)
#             # print('input_mask_unfold.size()',input_mask_unfold.size())
#             out = out*input_mask_unfold
            
#         # print('out.size()',out.size())    
#         out,_ = torch.max(out,2)
#         # print('out_max.size()',out.size())
        
#         if self.use_res_connect:
#             input = input.view(B, C, w, h)
#             out = input+out
#         return out








# class EdgeConv(nn.Module):
#     def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,dilation=1
#                      ):
        
#         super().__init__()
#         self.in_channels = in_channels
#         self.out_channels = out_channels
#         self.kernel_size = kernel_size
#         self.padding = padding
#         self.stride = stride
#         self.dilation = dilation




#         self.unfold1 = torch.nn.Unfold(kernel_size=kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)


        


#         exp = 3+self.in_channels#+self.in_channels


#         # self.weight = nn.Parameter(torch.Tensor(exp,out_channels))

#         self.exp = exp
#         self.mlp = nn.Sequential(
#             nn.Conv1d(exp,out_channels,kernel_size=1,padding=0),
#             # nn.BatchNorm1d(out_channels),
#             nn.ReLU(inplace=True),            
#             # nn.Linear(out_channels,out_channels),
#             # nn.ReLU(inplace=True),
#             )
        
#         # self.relu = nn.ReLU(inplace=True)
#         # self.bn = nn.BatchNorm2d(out_channels)


#         self.use_res_connect = stride == 1 and in_channels == out_channels
#         # nn.init.xavier_uniform_(self.weight)



#     def forward(self, input,input_coord,input_mask=None):
        
#         '''
#         input size: B x D x h x w
#         input_coord size: B x 3 x h x w 
#         input_mask size: B x h x w 
#         output size: B x D' x h x w
#         '''
#         B, C, h, w = input.size()
#         # print('input.size()',input.size())
#         # print('input_coord.size()',input_coord.size())
#         # print('input_mask.size()',input_mask.size())

#         if len(input_mask.size())==3:
#             input_mask = input_mask.unsqueeze(1)


#         x = torch.cat([input,input_coord,input_mask],1)
#         B, D, h, w = x.size()
#         # print('x.size()',x.size())
#         # B x D x h x w
#         # print('0 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))


#         x = F.pad(x,(self.padding,self.padding,self.padding,self.padding))
#         x_unfold = x.unfold(2,self.kernel_size,1)

#         # print('x_unfold h',x_unfold.size())
#         x_unfold = x_unfold.unfold(3,self.kernel_size,1)
#         # print('x_unfold w',x_unfold.size())

#         # x_unfold = x_unfold.permute(0,1,4,5,2,3)#.contiguous().view(B,-1,h*w)
#         # print('x_unfold permute',x_unfold.size())

#         input_coord_unfold = x_unfold[:,C:C+3,:,:,:,:]
#         input_mask_unfold = x_unfold[:,C+3:C+4,:,:,:,:]

#         # print('input_coord_unfold',input_coord_unfold.size())
#         # print('input_mask_unfold',input_mask_unfold.size())
#         n_windows = h*w
        
#         center_size = int((self.kernel_size-1)//2)
        
#         # coord_center = input_coord_unfold[:,:,:,:,center_size:center_size+1,center_size:center_size+1]
#         # print('coord_center.size()',coord_center.size())
        
#         delta = (input_coord_unfold - input_coord_unfold[:,:,:,:,center_size:center_size+1,center_size:center_size+1])
#         delta_theta = delta[:,0,:,:,:,:]
#         delta_phi = delta[:,1,:,:,:,:]
#         cos_theta = torch.cos(delta_theta)
#         sin_theta = torch.sin(delta_theta)
#         cos_phi = torch.cos(delta_phi)
#         sin_phi = torch.sin(delta_phi)

#         # print('delta',delta.size())
#         # print('delta_theta',delta_theta.size())
#         # print('delta_phi',delta_phi.size())
        

#         # print('0.5 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))

#         x_unfold = torch.cat([
#             (input_coord_unfold[:,2,:,:,:,:]*cos_theta*cos_phi - input_coord_unfold[:,:,:,:,center_size:center_size+1,center_size:center_size+1][:,2,:,:,:,:]).view(B,1,h,w,self.kernel_size,self.kernel_size),
#             (input_coord_unfold[:,2,:,:,:,:]*cos_theta*sin_phi).view(B,1,h,w,self.kernel_size,self.kernel_size),
#             (input_coord_unfold[:,2,:,:,:,:]*sin_theta).view(B,1,h,w,self.kernel_size,self.kernel_size),
#             (x_unfold[:,:C,:,:,:,:]),
#             # (x[:,:C,center_size:center_size+1,:].repeat(1,1,self.kernel_size*self.kernel_size, 1)).view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#             ],1,
#             # allocation="shared"
#             )#.permute(0,2,1)
        
#         # print('x_unfold cat',x_unfold.size())
#         # x_unfold = torch.einsum('abcdef,bg->agcdef',[x_unfold, self.weight])#.view(B,-1,self.kernel_size*self.kernel_size,h,w)
        
#         x_unfold = x_unfold.view(B,-1,h*w*self.kernel_size*self.kernel_size)#.permute(0,2,1)

#         x_unfold = self.mlp(x_unfold)

#         # x_unfold = self.bn(x_unfold)

#         # x_unfold = self.relu(x_unfold)

#         # print('x_unfold mlp',x_unfold.size())
#         x_unfold = x_unfold.view(B,self.out_channels,h,w,-1)

        
#         # print('x agg',x.size())
        
#         # print('1 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))

#         # exit()

#         # x = self.unfold1(x)
#         # print('x_unfold unfold',x.size())

#         # print('1.5 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))
        

#         # x = x.view(B,C+3+1,self.kernel_size*self.kernel_size,-1)
#         # # print('x.size()',x.size())
        
#         # print('2 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))
        
#         # # input_unfold = x[:,:C,:,:]
#         # # print('input_unfold',input_unfold.size())
#         # input_coord_unfold = x[:,C:C+3,:,:]
#         # # print('input_coord_unfold',input_coord_unfold.size())
#         # input_mask_unfold = x[:,C+3:C+4,:,:]
#         # # print('input_mask_unfold',input_mask_unfold.size())


#         # print('3 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))

#         # # center_size = int(self.kernel_size-1/2) 
        
#         # center_size = int(self.kernel_size*self.kernel_size-1/2) 
        
        
#         # # input_unfold = torch.nn.functional.unfold(input,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation).view(B,C,self.kernel_size,self.kernel_size,-1)
#         # _,_,_,n_windows = x[:,:C,:,:].size()
#         # # print('input_unfold.size()',input_unfold.size())
#         # # input_centers = input_unfold[:,:,center_size:center_size+1,:]
#         # # input_centers = input_centers.repeat(1,1,self.kernel_size*self.kernel_size, 1)
#         # # print('input_centers.size()',input_centers.size())

#         # # input_centers = x[:,:C,center_size:center_size+1,:].repeat(1,1,self.kernel_size*self.kernel_size, 1)
#         # print('4 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))
        
        
#         # # unfold to perform neighourhoods calculation
#         # # input_coord_unfold = torch.nn.functional.unfold(input_coord,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)

#         # # print('input_coord_unfold.size()',input_coord_unfold.size())

#         # # input_coord_unfold = input_coord_unfold.view(B,3,self.kernel_size,self.kernel_size,-1)
#         # # print('input_coord_unfold_expend.size() ',input_coord_unfold.size())
        
        
#         # # centers = input_coord_unfold[:,:,center_size:center_size+1,:]
#         # # print('centers.size()',centers.size())
        
#         # # theta_neighbour = input_coord_unfold[:,0,:,:]
#         # # phi_neighbour = input_coord_unfold[:,1,:,:]
#         # # r_neighbour = input_coord_unfold[:,2,:,:]


#         # # theta_center = centers[:,0,:,:]
#         # # phi_center = centers[:,1,:,:]
#         # # r_center = centers[:,2,:,:]


#         # # delta_theta = input_coord_unfold[:,0,:,:]-centers[:,0,:,:]
#         # # delta_phi = input_coord_unfold[:,1,:,:]-centers[:,1,:,:]
        
        
#         # delta = (input_coord_unfold-input_coord_unfold[:,:,center_size:center_size+1,:])
#         # delta_theta = delta[:,0,:,:]
#         # delta_phi = delta[:,1,:,:]
        
        
#         # # # asymmetric positional encoding
#         # # ase_0 = input_coord_unfold[:,2,:,:]*torch.cos(delta_theta)*torch.cos(delta_phi)-input_coord_unfold[:,2,center_size:center_size+1,:]
#         # # ase_1 = input_coord_unfold[:,2,:,:]*torch.cos(delta_theta)*torch.sin(delta_phi)
#         # # ase_2 = input_coord_unfold[:,2,:,:]*torch.sin(delta_theta)
        
#         # # print('ase_0.size()',ase_0.size())
#         # # print('ase_1.size()',ase_0.size())
#         # # print('ase_2.size()',ase_0.size())
        
#         # x = torch.cat([
#         #     (input_coord_unfold[:,2,:,:]*torch.cos(delta_theta)*torch.cos(delta_phi)-input_coord_unfold[:,2,center_size:center_size+1,:]).view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#         #     (input_coord_unfold[:,2,:,:]*torch.cos(delta_theta)*torch.sin(delta_phi)).view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#         #     (input_coord_unfold[:,2,:,:]*torch.sin(delta_theta)).view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#         #     (x[:,:C,:,:]).view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#         #     # (x[:,:C,center_size:center_size+1,:].repeat(1,1,self.kernel_size*self.kernel_size, 1)).view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#         #     ],1)#.permute(0,2,1)
        
#         # # print('out.size()',out.size())
#         # print('5 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))
        
#         # # out = self.mlp(out).view(B,-1,self.kernel_size*self.kernel_size,h,w)

#         # B ,c ,d - > B, c_out,d
#         # x = torch.einsum('abc,bd->adc',[x, self.weight]).view(B,-1,self.kernel_size*self.kernel_size,h,w)
#         # x = self.relu(x)

#         # print('6 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))
        

#         # print('out.size()',out.size())
#         # out = out.permute(0,2,1).view(B,-1,self.kernel_size*self.kernel_size,h,w)
#         # print('out.size()',out.size())

#         # merge mask for valid pixels
#         # if input_mask is not None:
#         # input_mask = input_mask.view(B,1,h,w) # B x 1 x h x w
#         # # print('input_mask.size()',input_mask.size())
#         # # unfold to perform neighourhoods calculation
#         # input_mask_unfold = torch.nn.functional.unfold(input_mask,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)
        
#         # # print('input_mask_unfold.size()',input_mask_unfold.size())
        
#         # input_mask_unfold = input_mask_unfold.view(B,self.kernel_size*self.kernel_size,-1)
#         # # print('input_mask_unfold_expend.size() ',input_mask_unfold.size())
        
#         # # centers = input_mask_unfold[:,center_size:center_size+1,:]
#         # # print('centers.size()',centers.size())
        
#         # input_mask_unfold = input_mask_unfold * input_mask_unfold[:,center_size:center_size+1,:]
#         # input_mask_unfold = input_mask_unfold.view(B,-1,w*h)
#         # # print('input_mask_unfold.size()',input_mask_unfold.size())
#         # # out = out*input_mask_unfold
        
#         # input_mask_unfold = torch.nn.functional.fold(input_mask_unfold,output_size=input_mask.size()[2:],kernel_size=self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)
#         # print('input_mask_unfold',input_mask_unfold.size())
#         # print('7 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))
        

#         # input_mask_unfold = input_mask_unfold.reshape(B,self.kernel_size,self.kernel_size,-1)
#         # # print('input_mask_unfold_expend.size() ',input_mask_unfold.size())
        
#         # centers = input_mask_unfold[:,center_size:center_size+1,center_size:center_size+1,:]
#         # # print('centers.size()',centers.size())
        
#         # input_mask_unfold = input_mask_unfold * centers
#         # input_mask_unfold = input_mask_unfold.view(B,-1,self.kernel_size*self.kernel_size,h,w)
#         # # print('input_mask_unfold.size()',input_mask_unfold.size())
#         # x_unfold = x_unfold*input_mask_unfold

#         # print('out.size()',out.size())    
#         x_unfold,_ = torch.max(x_unfold,-1)
#         # x_unfold = self.bn(x_unfold)
#         # print('out_max.size()',out.size())
#         # out = out*input_mask_unfold
#         # print('8 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=input.device))
#         # 
#         if self.use_res_connect:
#             input = input.view(B, C, h, w)
#             # print(x.size(),input.size)
#             x_unfold = input+x_unfold
#         # print('=='*12)
#         return x_unfold





# class EdgeConv(nn.Module):
#     def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,dilation=1
#                      ):
        
#         super().__init__()
#         self.in_channels = in_channels
#         self.out_channels = out_channels
#         self.kernel_size = kernel_size
#         self.padding = padding
#         self.stride = stride
#         self.dilation = dilation

#         exp = 3+self.in_channels#+self.in_channels
#         self.exp = exp
#         self.mlp = nn.Sequential(
#             # nn.Linear(exp,out_channels),
#             nn.Conv1d(exp,out_channels,kernel_size=1,stride=1,padding=0),
#             # nn.LayerNorm(out_channels),
#             nn.BatchNorm1d(out_channels),
#             nn.ReLU(inplace=True),
#             # nn.Linear(out_channels,out_channels),
#             # nn.ReLU(inplace=True),
#             )
        
#         self.use_res_connect = stride == 1 and in_channels == out_channels




#     def reset_parameters(self):
#         nn.init.xavier_uniform_(self.weight)
#         if self.bias is not None:
#             nn.init.constant_(self.bias, 0.)





#     def forward(self, input,input_coord,input_mask=None):
        
#         '''
#         input size: B x D x h x w
#         input_coord size: B x 3 x h x w 
#         input_mask size: B x h x w 
#         output size: B x D' x h x w
#         '''
#         B, C, h, w = input.size()
#         # print('input.size()',input.size())
#         # print('input_coord.size()',input_coord.size())
#         # print('input_mask.size()',input_mask.size())

#         if len(input_mask.size())==3:
#             input_mask = input_mask.unsqueeze(1)


#         x = torch.cat([input,input_coord,input_mask],1)
#         # print('x.size()',x.size())

#         x_unfold = torch.nn.functional.unfold(x,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation).view(B,C+3+1,self.kernel_size,self.kernel_size,-1)
#         # print('x_unfold.size()',x_unfold.size())


#         input_unfold = x_unfold[:,:C,:,:]
#         # print('input_unfold',input_unfold.size())
#         input_coord_unfold = x_unfold[:,C:C+3,:,:]
#         # print('input_coord_unfold',input_coord_unfold.size())
#         input_mask_unfold = x_unfold[:,C+3:C+4,:,:]
#         # print('input_mask_unfold',input_mask_unfold.size())


#         center_size = int(self.kernel_size-1/2) 
        
#         # input_unfold = torch.nn.functional.unfold(input,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation).view(B,C,self.kernel_size,self.kernel_size,-1)
#         _,_,_,_,n_windows = input_unfold.size()
#         # print('input_unfold.size()',input_unfold.size())
#         input_centers = input_unfold[:,:,center_size:center_size+1,center_size:center_size+1,:]
#         input_centers = input_centers.repeat(1,1,self.kernel_size,self.kernel_size, 1)
#         # print('input_centers.size()',input_centers.size())
        
        
#         # unfold to perform neighourhoods calculation
#         # input_coord_unfold = torch.nn.functional.unfold(input_coord,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)

#         # print('input_coord_unfold.size()',input_coord_unfold.size())

#         # input_coord_unfold = input_coord_unfold.view(B,3,self.kernel_size,self.kernel_size,-1)
#         # print('input_coord_unfold_expend.size() ',input_coord_unfold.size())
        
        
#         centers = input_coord_unfold[:,:,center_size:center_size+1,center_size:center_size+1,:]
#         # print('centers.size()',centers.size())
        
#         # theta_neighbour = input_coord_unfold[:,0,:,:]
#         # phi_neighbour = input_coord_unfold[:,1,:,:]
#         # r_neighbour = input_coord_unfold[:,2,:,:]


#         # theta_center = centers[:,0,:,:]
#         # phi_center = centers[:,1,:,:]
#         # r_center = centers[:,2,:,:]


#         # delta_theta = input_coord_unfold[:,0,:,:]-centers[:,0,:,:]
#         # delta_phi = input_coord_unfold[:,1,:,:]-centers[:,1,:,:]
        
        
#         delta = (input_coord_unfold-centers)
#         delta_theta = delta[:,0,:,:]
#         delta_phi = delta[:,1,:,:]
        
        
#         # asymmetric positional encoding
#         ase_0 = input_coord_unfold[:,2,:,:]*torch.cos(delta_theta)*torch.cos(delta_phi)-centers[:,2,:,:]
#         ase_1 = input_coord_unfold[:,2,:,:]*torch.cos(delta_theta)*torch.sin(delta_phi)
#         ase_2 = input_coord_unfold[:,2,:,:]*torch.sin(delta_theta)
        
#         # print('ase_0.size()',ase_0.size())
#         # print('ase_1.size()',ase_0.size())
#         # print('ase_2.size()',ase_0.size())
        
#         features = torch.cat([
#             ase_0.view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#             ase_1.view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#             ase_2.view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#             input_unfold.view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#             # input_centers.view(B,-1,self.kernel_size*self.kernel_size*n_windows),
#             ],1)#.permute(0,2,1)
        
#         # res: batch_size,channel_size,kernel_size,kernel_size,n_windows

#         # print('features.size()',features.size())
        
#         out = self.mlp(features).view(B,-1,self.kernel_size*self.kernel_size,h,w)
        
#         # merge mask for valid pixels
#         # if input_mask is not None:
#         # input_mask = input_mask.view(B,1,h,w) # B x 1 x h x w
#         # # print('input_mask.size()',input_mask.size())
#         # # unfold to perform neighourhoods calculation
#         # input_mask_unfold = torch.nn.functional.unfold(input_mask,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)
        
#         # # print('input_mask_unfold.size()',input_mask_unfold.size())
        
#         # input_mask_unfold = input_mask_unfold.view(B,self.kernel_size,self.kernel_size,-1)
#         # # print('input_mask_unfold_expend.size() ',input_mask_unfold.size())
        
#         # centers = input_mask_unfold[:,center_size:center_size+1,center_size:center_size+1,:]
#         # # print('centers.size()',centers.size())
        
#         # input_mask_unfold = input_mask_unfold * centers
#         # input_mask_unfold = input_mask_unfold.view(B,-1,self.kernel_size*self.kernel_size,h,w)
#         # # print('input_mask_unfold.size()',input_mask_unfold.size())
#         # out = out*input_mask_unfold
        
#         # print('out.size()',out.size())    
#         out,_ = torch.max(out,2)
#         # print('out_max.size()',out.size())
        
#         if self.use_res_connect:
#             input = input.view(B, C, h, w)
#             out = input+out
#         return out


class EdgeConv(nn.Module):
    def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,dilation=1
                     ):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.stride = stride
        self.dilation = dilation




        self.unfold1 = torch.nn.Unfold(kernel_size=kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)


        


        exp = self.in_channels#+self.in_channels


        # self.weight = nn.Parameter(torch.Tensor(exp,out_channels))

        self.exp = exp
        self.mlp = nn.Sequential(
            nn.Conv1d(exp,out_channels,kernel_size=1,padding=0),
            #nn.BatchNorm1d(out_channels),
            nn.ReLU(inplace=True),            
            # nn.Linear(out_channels,out_channels),
            # nn.ReLU(inplace=True),
            )
        
        # self.relu = nn.ReLU(inplace=True)
        # self.bn = nn.BatchNorm2d(out_channels)


        self.use_res_connect = stride == 1 and in_channels == out_channels
        # nn.init.xavier_uniform_(self.weight)



    def forward(self, input,input_coord,input_mask=None):
        
        '''
        input size: B x D x h x w
        input_coord size: B x 3 x h x w 
        input_mask size: B x h x w 
        output size: B x D' x h x w
        '''
        B, C, h, w = input.size()
        # print('input.size()',input.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())

        if len(input_mask.size())==3:
            input_mask = input_mask.unsqueeze(1)


        x = input
        B, D, h, w = x.size()


        x = F.pad(x,(self.padding,self.padding,self.padding,self.padding))
        x_unfold = x.unfold(2,self.kernel_size,1)

        x_unfold = x_unfold.unfold(3,self.kernel_size,1)

        
        
        x_unfold = x_unfold.reshape(B,-1,h*w*self.kernel_size*self.kernel_size)

        x_unfold = self.mlp(x_unfold)

        x_unfold = x_unfold.view(B,self.out_channels,h,w,-1)
 
        x_unfold,_ = torch.max(x_unfold,-1)

        if self.use_res_connect:
            input = input.view(B, C, h, w)
            # print(x.size(),input.size)
            x_unfold = input+x_unfold
        # print('=='*12)
        return x_unfold






class EdgeConv(nn.Module):
    def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,dilation=1
                     ):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.stride = stride
        self.dilation = dilation



        #print(kernel_size,stride,padding,dilation)
        self.unfold1 = nn.Unfold(kernel_size=kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)


        


        exp = self.in_channels#+self.in_channels
        #exp = out_channels//8 if out_channels%8==0 else in_channels
        # print('exp',exp,out_channels)
        # self.weight = nn.Parameter(torch.Tensor(exp,out_channels))
        #self.dr = nn.Sequential(
        #    nn.Conv2d(in_channels,exp,kernel_size=1,padding=0),
        #    #nn.BatchNorm2d(exp),
        #    nn.ReLU(inplace=True),
        #    # nn.Linear(out_channels,out_channels),
        #    # nn.ReLU(inplace=True),
        #)



        self.norm = nn.BatchNorm1d(out_channels//2,eps=1e-3,momentum=0.01)
        self.exp = exp
        self.linear = nn.Linear(exp,out_channels//2)
        self.relu = nn.ReLU(inplace=True)


        self.norm2 = nn.BatchNorm1d(out_channels,eps=1e-3,momentum=0.01)
        self.linear2 = nn.Linear(out_channels//2,out_channels)
        self.relu2 = nn.ReLU(inplace=True)
        #self.mlp = nn.Sequential(
        #    nn.Conv1d(exp,out_channels,kernel_size=1,padding=0),
        #    #nn.BatchNorm1d(out_channels),
        #    nn.ReLU(inplace=True),            
        #    # nn.Linear(out_channels,out_channels),
        #    # nn.ReLU(inplace=True),
        #    )
        
        # self.relu = nn.ReLU(inplace=True)
        # self.bn = nn.BatchNorm2d(out_channels)


        self.use_res_connect = stride == 1 and in_channels == out_channels
        # nn.init.xavier_uniform_(self.weight)



    def forward(self, input,input_coord,input_mask=None):
        
        '''
        input size: B x D x h x w
        input_coord size: B x 3 x h x w 
        input_mask size: B x h x w 
        output size: B x D' x h x w
        '''
        B, C, h, w = input.size()
        # print('input.size()',input.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())

        if len(input_mask.size())==3:
            input_mask = input_mask.unsqueeze(1)


        #x = self.dr(input)
        x = input
        B, D, h, w = x.size()


        # print('input,size()',input.size())
        # print('dr x.size()',x.size(),self.out_channels)

        #xx = self.unfold1(x).view(B,D,-1)
        #print('------------')
        #print('xx.size()',xx.size())
        #xx = self.mlp(xx).view(B,self.out_channels,self.kernel_size*self.kernel_size,-1)
        xx = x.view(B,D,h*w).permute(0,2,1).contiguous()
        #print('0 xx.size()',xx.size())
        
        
        xx = self.linear(xx)
        #print('1 xx.size()',xx.size())
        
        torch.backends.cudnn.enabled = False
        xx = self.norm(xx.permute(0,2,1).contiguous()).permute(0,2,1).contiguous()
        #print('2 xx.size()',xx.size())
        
        torch.backends.cudnn.enabled = True
        xx = self.relu(xx)



        xx = self.linear2(xx)
        #print('1 xx.size()',xx.size())
        
        torch.backends.cudnn.enabled = False
        xx = self.norm2(xx.permute(0,2,1).contiguous()).permute(0,2,1).contiguous()
        #print('2 xx.size()',xx.size())
        
        torch.backends.cudnn.enabled = True
        xx = self.relu2(xx)
        
        xx = xx.view(B,self.out_channels,h,w)
        #print('3 xx.size()',xx.size())
        xx = self.unfold1(xx).view(B,self.out_channels,-1)
        #print('4 xx.size()',xx.size())
        xx = xx.reshape(B,self.out_channels,self.kernel_size*self.kernel_size,-1)
        #print('5 xx.size()',xx.size())
	    
        xx,_ = torch.max(xx,2)
        xx = xx.view(B,self.out_channels,h,w)
		
        #print('6 xx.size()',xx.size())
        x_unfold = xx
        
        
        
        #x = F.pad(x,(self.padding,self.padding,self.padding,self.padding))


        #x_unfold = x.unfold(2,self.kernel_size,1)

        #x_unfold = x_unfold.unfold(3,self.kernel_size,1)

        
        
        #x_unfold = x_unfold.reshape(B,-1,h*w*self.kernel_size*self.kernel_size)

        #x_unfold = self.mlp(x_unfold)

        #x_unfold = x_unfold.view(B,self.out_channels,h,w,-1)
 
        #x_unfold,_ = torch.max(x_unfold,-1)

        #if self.use_res_connect:
        #   input = input.view(B, C, h, w)
           # print(x.size(),input.size)
        #   x_unfold = input+x_unfold
        #print('=='*12)
        return x_unfold





class EdgeConv(nn.Module):
    def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,dilation=1,K=2
                     ):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.stride = stride
        self.dilation = dilation

        self.K = 2



        #print(kernel_size,stride,padding,dilation)
        self.weight = nn.Parameter(torch.randn(out_channels, in_channels, kernel_size*kernel_size), requires_grad=True)


        self.weight_transform = nn.Sequential(
            nn.Conv1d(in_channels,in_channels//4,kernel_size=1,padding=0),
            nn.ReLU(inplace=True),
            nn.Conv1d(in_channels//4,1,kernel_size=1,padding=0),
            nn.ReLU(inplace=True),
            )



        self.unfold1 = nn.Unfold(kernel_size=kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)
        self.out = nn.Sequential(
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),            
            )
        
        
    def forward(self, input,input_coord,input_mask=None):
        
        '''
        input size: B x D x h x w
        input_coord size: B x 3 x h x w 
        input_mask size: B x h x w 
        output size: B x D' x h x w
        '''
        B, C, h, w = input.size()
        # print('input.size()',input.size())
        # print('input_coord.size()',input_coord.size())
        # print('input_mask.size()',input_mask.size())

        if len(input_mask.size())==3:
            input_mask = input_mask.unsqueeze(1)


        #x = self.dr(input)
        x = input
        B, D, h, w = x.size()

        x = self.unfold1(x).view(B,self.in_channels,self.kernel_size*self.kernel_size*h*w)
        #print('x.size()',x.size())

        att = self.weight_transform(x).view(B,self.kernel_size*self.kernel_size,h*w)
        att = torch.softmax(att,2)
        #print('att.size()',att.size())
        '''
        a bs
        b in
        c k*k
        d h*w
        e out
        bs in out k*k h*w -> abecd
        '''
        # att_weight = torch.einsum('acd,ebc->abecd',[att,self.weight])
        # print('att_weight.size()',att_weight.size())
        
        
        
        # input_mask = input_mask.view(B,1,h,w) # B x 1 x h x w
        # # print('input_mask.size()',input_mask.size())
        # # unfold to perform neighourhoods calculation
        # input_mask_unfold = torch.nn.functional.unfold(input_mask,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation)
        
        # # print('input_mask_unfold.size()',input_mask_unfold.size())
        
        # input_mask_unfold = input_mask_unfold.view(B,self.kernel_size*self.kernel_size,-1)
        # # print('input_mask_unfold_expend.size() ',input_mask_unfold.size())
        
        # center_size = int(self.kernel_size*self.kernel_size-1/2) 
        # centers = input_mask_unfold[:,center_size:center_size+1,:]
        # # print('centers.size()',centers.size())
        
        # input_mask_unfold = input_mask_unfold * input_mask_unfold[:,center_size:center_size+1,:]
        # input_mask_unfold = input_mask_unfold.view(B,-1,w*h)

        # print('input_mask_unfold.size()',input_mask_unfold.size())
        # out = out*input_mask_unfold



        # x_unfold = x.view(B,self.in_channels,self.kernel_size*self.kernel_size,h*w)
        
        '''
        a bs
        b in
        c k*k
        d h*w
        e out
        bs in out k*k h*w -> abecd
        ''' 
        # 1,7,9,169728,1, 32, 7, 9, 169728
        # x_unfold = torch.einsum('abcd,abecd->aed',[x_unfold,att_weight]).view(B,self.out_channels,h,w)
        # print('x_unfold.size()',x_unfold.size())
        
        # x_unfold = torch.einsum('abcd,abecd,acd->aed',[x_unfold,att_weight,input_mask_unfold]).view(B,self.out_channels,h,w)
        # print('x_unfold.size()',x_unfold.size())
        #print('att',att.size())
        #print('x',x.size())
        #print('self.weight,self.weight.size())
        # x_unfold = torch.einsum('abcd,acd,ebc,acd->aed',[x.view(B,self.in_channels,self.kernel_size*self.kernel_size,h*w),att,self.weight,input_mask_unfold]).view(B,self.out_channels,h,w)
        # x_unfold = torch.einsum('abcd,ebc->aed',[x.view(B,self.in_channels,self.kernel_size*self.kernel_size,h*w),self.weight]).view(B,self.out_channels,h,w)
        x_unfold = torch.einsum('acd,ebc,abcd->aed',[att,self.weight,x.view(B,self.in_channels,self.kernel_size*self.kernel_size,h*w)]).view(B,self.out_channels,h,w)
        # x_unfold = torch.einsum('abcd,acd,ebc->aed',[x.view(B,self.in_channels,self.kernel_size*self.kernel_size,h*w),att,self.weight]).view(B,self.out_channels,h,w)
        
        #print('x_unfold.size()',x_unfold.size())
        x_unfold = self.out(x_unfold)

        return x_unfold



class PolarFeatureExtractor(nn.Module):
    def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,dilation=1
                     ):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.stride = stride
        self.dilation = dilation
        exp = 3
        self.exp = exp
        self.mlp = nn.Sequential(
            nn.Conv1d(exp,out_channels,kernel_size=1,padding=0),
            nn.BatchNorm1d(out_channels),
            nn.ReLU(inplace=True),            
            )

    def forward(self, input,input_coord,input_mask=None):
        
        '''
        input size: B x D x h x w
        input_coord size: B x 3 x h x w 
        input_mask size: B x h x w 
        output size: B x D' x h x w
        '''
        B, C, h, w = input.size()

        if len(input_mask.size())==3:
            input_mask = input_mask.unsqueeze(1)

        x = torch.cat([input_coord,input_mask],1)
        B, D, h, w = x.size()
        
        x = F.pad(x,(self.padding,self.padding,self.padding,self.padding))
        x_unfold = x.unfold(2,self.kernel_size,1)
        
        x_unfold = x_unfold.unfold(3,self.kernel_size,1)
        
        input_coord_unfold = x_unfold[:,:3,:,:,:,:]
        input_mask_unfold = x_unfold[:,3:4,:,:,:,:]

        n_windows = h*w
        
        center_size = int((self.kernel_size-1)//2)

        delta = (input_coord_unfold - input_coord_unfold[:,:,:,:,center_size:center_size+1,center_size:center_size+1])
        delta_theta = delta[:,0,:,:,:,:]
        delta_phi = delta[:,1,:,:,:,:]
        cos_theta = torch.cos(delta_theta)
        sin_theta = torch.sin(delta_theta)
        cos_phi = torch.cos(delta_phi)
        sin_phi = torch.sin(delta_phi)

        x_unfold = torch.cat([
            (input_coord_unfold[:,2,:,:,:,:]*cos_theta*cos_phi - input_coord_unfold[:,:,:,:,center_size:center_size+1,center_size:center_size+1][:,2,:,:,:,:]).view(B,1,h,w,self.kernel_size,self.kernel_size),
            (input_coord_unfold[:,2,:,:,:,:]*cos_theta*sin_phi).view(B,1,h,w,self.kernel_size,self.kernel_size),
            (input_coord_unfold[:,2,:,:,:,:]*sin_theta).view(B,1,h,w,self.kernel_size,self.kernel_size),
            ],1,
            )
        
        x_unfold = x_unfold.view(B,-1,h*w*self.kernel_size*self.kernel_size)
        
        x_unfold = self.mlp(x_unfold)
        
        x_unfold = x_unfold.view(B,self.out_channels,h,w,-1)
        
        
        x_unfold,_ = torch.max(x_unfold,-1)
        
        return x_unfold




class PolarMeanFeatureExtractor(nn.Module):
    def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,dilation=1
                     ):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.stride = stride
        self.dilation = dilation
        # self.unfold  = nn.Unfold(kernel_size=kernel_size,stride=stride,padding=padding,dilation=dilation)
        
        exp = 3
        self.exp = exp
        self.mlp = nn.Sequential(
            nn.Conv1d(exp,out_channels,kernel_size=1,padding=0),
            nn.BatchNorm1d(out_channels),
            nn.ReLU(inplace=True),            
            )

    def forward(self, input,input_coord,input_mask=None):
        
        '''
        input size: B x D x h x w
        input_coord size: B x 3 x h x w 
        input_mask size: B x h x w 
        output size: B x D' x h x w
        '''
        B, C, h, w = input.size()

        if len(input_mask.size())==3:
            input_mask = input_mask.unsqueeze(1)

        x = torch.cat([input_coord,input_mask],1)


        # x_unfold = self.unfold(x)
        # print('x_unfold',x_unfold.size())


        B, D, h, w = x.size()
        
        x = F.pad(x,(self.padding,self.padding,self.padding,self.padding))
        x_unfold = x.unfold(2,self.kernel_size,1)
        
        x_unfold = x_unfold.unfold(3,self.kernel_size,1)
        
        input_coord_unfold = x_unfold[:,:3,:,:,:,:]
        input_mask_unfold = x_unfold[:,3:4,:,:,:,:]

        n_windows = h*w
        
        center_size = int((self.kernel_size-1)//2)

        delta = (input_coord_unfold - input_coord_unfold[:,:,:,:,center_size:center_size+1,center_size:center_size+1])
        delta_theta = delta[:,0,:,:,:,:]
        delta_phi = delta[:,1,:,:,:,:]
        cos_theta = torch.cos(delta_theta)
        sin_theta = torch.sin(delta_theta)
        cos_phi = torch.cos(delta_phi)
        sin_phi = torch.sin(delta_phi)

        x_unfold = torch.cat([
            (input_coord_unfold[:,2,:,:,:,:]*cos_theta*cos_phi - input_coord_unfold[:,:,:,:,center_size:center_size+1,center_size:center_size+1][:,2,:,:,:,:]).view(B,1,h,w,self.kernel_size,self.kernel_size),
            (input_coord_unfold[:,2,:,:,:,:]*cos_theta*sin_phi).view(B,1,h,w,self.kernel_size,self.kernel_size),
            (input_coord_unfold[:,2,:,:,:,:]*sin_theta).view(B,1,h,w,self.kernel_size,self.kernel_size),
            ],1,
            )
        
        x_unfold = x_unfold.view(B,-1,h*w*self.kernel_size*self.kernel_size)
        
        x_unfold = self.mlp(x_unfold)
        
        x_unfold = x_unfold.view(B,self.out_channels,h,w,-1)
        
        
        x_unfold_mean = torch.mean(x_unfold,-1)
        x_unfold_max,_ = torch.max(x_unfold,-1)

        x_unfold = torch.cat([x_unfold_mean,x_unfold_max],1)
        
        return x_unfold

class SmartDownSampling(nn.Module):
    def __init__(self, in_channels,out_channels, kernel_size=2,stride=2, padding=0,dilation=1
                     ):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.stride = stride
        self.dilation = dilation

    def forward(self, input,input_coord,input_mask):
        
        '''
        input size: B x D x h x w
        input_coord size: B x 3 x h x w 
        input_mask size: B x h x w 
        output size: B x D' x h x w
        '''
        
        B, C_input, w, h = input.size()
        B, C_corrd, w, h = input_coord.size()
        
        new_h = int(h/self.stride)
        new_w = int(w/self.stride)

        # t = torch.nn.functional.unfold(input_coord,self.kernel_size,stride=self.stride,
        #     padding=self.padding,dilation = self.dilation)
        # # print('t',t.size())
        
        # unfold to perform neighourhoods calculation
        input_coord_unfold = torch.nn.functional.unfold(input_coord[:,2:,:,:],self.kernel_size,stride=self.stride,
            padding=self.padding,dilation = self.dilation).view(B,C_corrd,-1,new_w*new_h)
        
        # print('input_coord_unfold.size()',input_coord_unfold.size())
        
        # input_coord_unfold = input_coord_unfold.view(B,C_corrd,self.kernel_size*self.kernel_size,self.stride,
        #     new_w,self.stride,new_h).permute(0,1,2,3,5,4,6).contiguous().view(B,C_corrd,-1,new_w*new_h)
        # # print('input_coord_unfold_expend.size() ',input_coord_unfold.size())
        
        input_mask = input_mask.view(B,1,w,h) # B x 1 x h x w
        # print('input_mask.size()',input_mask.size())
        # unfold to perform neighourhoods calculation
        input_mask_unfold = torch.nn.functional.unfold(input_mask,self.kernel_size,stride=self.stride,
            padding=self.padding,dilation = self.dilation).view(B,-1,new_w*new_h)
        
        # print('input_mask_unfold.size()',input_mask_unfold.size())

        # input_mask_unfold = input_mask_unfold.view(B,1,self.kernel_size*self.kernel_size,self.stride,
        #     new_w,self.stride,new_h).permute(0,1,2,3,5,4,6).contiguous().view(B,-1,new_w*new_h)
        # # print('input_mask_unfold_expend.size() ',input_mask_unfold.size())
        
        
        # theta_neighbour = input_coord_unfold[:,0,:,:]
        # phi_neighbour = input_coord_unfold[:,1,:,:]
        
        # print('r_neighbour.size()',r_neighbour.size())

        
        u = torch.sum(input_coord_unfold[:,0,:,:] * input_mask_unfold,1)/torch.sum(input_mask_unfold,1)
        u = u.view(B,1,-1)
        # print('input_coord_unfold[:,0,:,:]',input_coord_unfold[:,0,:,:].size())
        # print('u',u.size())
        
        # print('(input_coord_unfold[:,0,:,:] - u)**2',((input_coord_unfold[:,0,:,:] - u)**2).size())
        
        index = torch.argmin((input_coord_unfold[:,0,:,:] - u)**2,1).view(B,1,-1)
        # print('index.size()',index.size())
        
        # index_selector = index.permute(0,2,1).view(-1,1)
        # print('index_selector.size()',index_selector.size())
        
        # downsample_mask = torch.zeros(B*new_w*new_h,self.stride*self.stride, dtype=torch.float).to(device)
        
        # downsample_mask = downsample_mask.scatter_(1,index_selector,1).to(device)
        # downsample_mask = downsample_mask.view(B,new_w*new_h,self.stride*self.stride).permute(0,2,1).contiguous()
        
        # print('downsample_mask.size()',downsample_mask.size())
        
         # unfold to perform neighourhoods calculation
        input_unfold = torch.nn.functional.unfold(input,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation).view(B,C_input,-1,new_w*new_h)
        
        # print('input_unfold.size()',input_unfold.size())
        
        
        dim = input_unfold.size(1)
        index_expand = index.unsqueeze(1).expand(index.size(0),dim,index.size(1), index.size(2))
        # print('index_expand',index_expand.size())
        
        downsample_input = input_unfold.gather(2,index_expand).view(B,C_input,new_w,new_h)
        # print('downsample_input.size()',downsample_input.size())

        
        dim = input_coord_unfold.size(1)
        index_expand = index.unsqueeze(1).expand(index.size(0),dim,index.size(1), index.size(2))
        downsample_input_coord = input_coord_unfold.gather(2,index_expand).view(B,C_corrd,new_w,new_h)
        # print('downsample_input_coord.size()',downsample_input_coord.size())
        
        
        
        downsample_input_mask = input_mask_unfold.gather(2,index).view(B,new_w,new_h)
        # print('downsample_input_mask.size()',downsample_input_mask.size())
        
        # print('==========')


        return downsample_input,downsample_input_coord,downsample_input_mask







class SmartDownSampling(nn.Module):
    def __init__(self, in_channels,out_channels, kernel_size=2,stride=2, padding=0,dilation=1
                     ):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.stride = stride
        self.dilation = dilation

    def forward(self, input,input_coord,input_mask):
        
        '''
        input size: B x D x h x w
        input_coord size: B x 3 x h x w 
        input_mask size: B x h x w 
        output size: B x D' x h x w
        '''
            
        B, C_input, h, w = input.size()
        B, C_corrd, h, w = input_coord.size()
        
        new_h = int(h/self.stride)
        new_w = int(w/self.stride)
        
        # unfold to perform neighourhoods calculation
        input_coord_unfold = torch.nn.functional.unfold(input_coord,self.kernel_size,stride=self.stride,
            padding=self.padding,dilation = self.dilation).view(B,C_corrd,-1,new_h*new_w)
        


        input_mask = input_mask.view(B,1,h,w) # B x 1 x h x w

        # unfold to perform neighourhoods calculation
        input_mask_unfold = torch.nn.functional.unfold(input_mask,self.kernel_size,stride=self.stride,
            padding=self.padding,dilation = self.dilation).view(B,-1,new_h*new_w)

        
        theta_neighbour = input_coord_unfold[:,0,:,:]
        phi_neighbour = input_coord_unfold[:,1,:,:]
        r_neighbour = input_coord_unfold[:,2,:,:]
        
 
        u = torch.sum(r_neighbour * input_mask_unfold,1)/(torch.sum(input_mask_unfold,1)+1e-8)
        u = u.view(B,1,-1)
  
        
        # find index of pixel which is relatively closer to majority of the neighbour pixels
        index = torch.argmin((r_neighbour * input_mask_unfold - u)**2**0.5,1).view(B,1,-1)

        input_unfold = torch.nn.functional.unfold(input,self.kernel_size,stride=self.stride,padding=self.padding,dilation = self.dilation).view(B,C_input,-1,new_w*new_h)

        dim = input_unfold.size(1)
        index_expand = index.unsqueeze(1).expand(index.size(0),dim,index.size(1), index.size(2))
 
        downsample_input = input_unfold.gather(2,index_expand).view(B,C_input,new_h,new_w)
  
        
        dim = input_coord_unfold.size(1)
        index_expand = index.unsqueeze(1).expand(index.size(0),dim,index.size(1), index.size(2))
        downsample_input_coord = input_coord_unfold.gather(2,index_expand).view(B,C_corrd,new_h,new_w)
 
        
        downsample_input_mask = input_mask_unfold.gather(2,index).view(B,new_h,new_w)


        return downsample_input,downsample_input_coord,downsample_input_mask


if __name__ == '__main__':
        
    if torch.cuda.is_available():
        torch.cuda.set_device(0)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') 

    layer = EdgeConv(5,128,kernel_size=3).to(device)
    
    sample = torch.rand(8,5,128,128).to(device)
    sample_coord = torch.rand(8,3,128,128).to(device)
    sample_mask = torch.rand(8,128,128).to(device)
    res = layer(sample,sample_coord,sample_mask)
    print('res.size()',res.size())
    
    # sampling = SmartDownSampling(64,128,kernel_size=1,padding=0,stride=2).to(device)
    # sampling(res,sample_coord,sample_mask)

