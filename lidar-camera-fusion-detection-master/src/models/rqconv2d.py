import torch
import torch.nn as nn



class attention2d(nn.Module):
    def __init__(self, in_planes, ratios, K, temperature, init_weight=True):
        super(attention2d, self).__init__()
        assert temperature%3==1
        self.avgpool = nn.AdaptiveAvgPool2d(1)
        if in_planes!=3:
            hidden_planes = int(in_planes*ratios)+1
        else:
            hidden_planes = K
        self.fc1 = nn.Conv2d(in_planes, hidden_planes, 1, bias=False)
        # self.bn = nn.BatchNorm2d(hidden_planes)
        self.fc2 = nn.Conv2d(hidden_planes, K, 1, bias=True)
        self.temperature = temperature
        if init_weight:
            self._initialize_weights()


    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            if isinstance(m ,nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def updata_temperature(self):
        if self.temperature!=1:
            self.temperature -=3
            print('Change temperature to:', str(self.temperature))


    def forward(self, x):
        x = self.avgpool(x)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x).view(x.size(0), -1)
        return F.softmax(x/self.temperature, 1)


class Dynamic_conv2d(nn.Module):
    def __init__(self, in_planes, out_planes, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True, K=4, ratio=0.25,temperature=34, init_weight=True):
        super(Dynamic_conv2d, self).__init__()
        assert in_planes%groups==0
        self.in_planes = in_planes
        self.out_planes = out_planes
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.dilation = dilation
        self.groups = groups
        self.bias = bias
        self.K = K
        self.attention = attention2d(in_planes, ratio, K, temperature)

        self.weight = nn.Parameter(torch.randn(K, out_planes, in_planes//groups, kernel_size, kernel_size), requires_grad=True)
        if bias:
            self.bias = nn.Parameter(torch.Tensor(K, out_planes))
        else:
            self.bias = None
        if init_weight:
            self._initialize_weights()

        #TODO 初始化
    def _initialize_weights(self):
        for i in range(self.K):
            nn.init.kaiming_uniform_(self.weight[i])


    def update_temperature(self):
        self.attention.updata_temperature()

    def forward(self, x):#将batch视作维度变量，进行组卷积，因为组卷积的权重是不同的，动态卷积的权重也是不同的
        softmax_attention = self.attention(x)
        batch_size, in_planes, height, width = x.size()
        print('x',x.size())
        
        x = x.view(1, -1, height, width)# 变化成一个维度进行组卷积
        weight = self.weight.view(self.K, -1)
        

        print('softmax_attention',softmax_attention.size())
        
        # 动态卷积的权重的生成， 生成的是batch_size个卷积参数（每个参数不同）
        aggregate_weight = torch.mm(softmax_attention, weight).view(-1, self.in_planes, self.kernel_size, self.kernel_size)
        print('aggregate_weight',aggregate_weight.size())
        

        if self.bias is not None:
            aggregate_bias = torch.mm(softmax_attention, self.bias).view(-1)
            output = F.conv2d(x, weight=aggregate_weight, bias=aggregate_bias, stride=self.stride, padding=self.padding,
                              dilation=self.dilation, groups=self.groups*batch_size)
        else:
            output = F.conv2d(x, weight=aggregate_weight, bias=None, stride=self.stride, padding=self.padding,
                              dilation=self.dilation, groups=self.groups * batch_size)

        output = output.view(batch_size, self.out_planes, output.size(-2), output.size(-1))
        return output


class RangeQuantizedConv2d(nn.Module):

    '''RangeQuantizedConv2d Convolution assuming the input is BxCxhxw
    This is just an example that explains LightConv clearer than the TBC version.
    We don't use this module in the model.
    Args:
        input_size: # of channels of the input and output
        kernel_size: convolution channels
        padding: padding
        num_range_slots: number of heads used. The weight is of shape
            `(k,in_channels,out_channels, kernel_size, kernel_size)`
        weight_softmax: normalize the weight with softmax before the convolution
    Shape:
        Input: BxCxhxw, i.e. (batch_size, input_size, h,w)
        Output: BxCxhxw, i.e. (batch_size, input_size, h,w)
    Attributes:
        weight: the learnable weights of the module of shape
            `(k,in_channels,out_channels, kernel_size, kernel_size)`
        bias: the learnable bias of the module of shape `(input_size)`
    '''
    
    def __init__(self, in_channels,out_channels, kernel_size=3,stride=1, padding=1,padding_mode='zeros',
                 dilation = 1,groups=1, bias=True,ranges=[(0,0.5),(0.5,1)],weight_softmax=True, weight_dropout=0.,):
        
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.ranges = ranges
        self.num_range_slots = len(ranges)
        self.padding = padding
        self.stride = stride
        self.weight_softmax = weight_softmax
        self.weight = nn.Parameter(torch.Tensor(self.num_range_slots, in_channels,out_channels, kernel_size,kernel_size))

        self.dilation = dilation
        if bias:
            self.bias = nn.Parameter(torch.Tensor(out_channels))
        else:
            self.bias = None

        self.weight_dropout = weight_dropout
        self.reset_parameters()
        
        self.use_res_connect = stride == 1 and in_channels == out_channels

    def reset_parameters(self):
        nn.init.xavier_uniform_(self.weight)
        if self.bias is not None:
            nn.init.constant_(self.bias, 0.)

    def forward(self, input,input_coord,input_mask=None):
        
        '''
        input size: B x D x h x w
        input_coord size: B x 3 x h x w 
        input_mask size: B x h x w 
        output size: B x D' x h x w
        '''
        
        B, C, h, w = input.size()
        
        Ri = input_coord[:,2,:,:].view(B,1,h,w) # B x 1 x h x w # range channel dimension
        # print('Ri.size()',Ri.size())
        # unfold to perform neighourhoods calculation
        Ri_unfold = torch.nn.functional.unfold(Ri,self.kernel_size,stride=self.stride,
            padding=self.padding,dilation = self.dilation)

        # print('Ri_unfold.size()',Ri_unfold.size())

        Ri_unfold = Ri_unfold.view(B,self.kernel_size,self.kernel_size,-1)
        # print('Ri_unfold_expend.size() ',Ri_unfold.size())

        center_size = int(self.kernel_size-1/2) 
        centers = Ri_unfold[:,center_size:center_size+1,center_size:center_size+1,:]
        # print('centers.size()',centers.size())

        delta_r = Ri_unfold - centers

        delta_r = delta_r.view(B,self.kernel_size*self.kernel_size,-1)
        # print('delta_r.size()',delta_r.size())


        delta_r_bin = []
        for r in self.ranges:
            delta_r_bin.append((r[0]<delta_r ) & (delta_r<r[1]))

        delta_r_bin = torch.cat(delta_r_bin,-1).view(B,self.kernel_size,self.kernel_size,-1,
            self.num_range_slots)

        # print('delta_r_bin.size()',delta_r_bin.size())

        # merge mask for valid pixels
        if input_mask is not None:
            input_mask = input_mask.view(B,1,h,w) # B x 1 x h x w
            # print('input_mask.size()',input_mask.size())
            # unfold to perform neighourhoods calculation
            input_mask_unfold = torch.nn.functional.unfold(input_mask,self.kernel_size,stride=self.stride,
                padding=self.padding,dilation = self.dilation)

            # print('input_mask_unfold.size()',input_mask_unfold.size())

            input_mask_unfold = input_mask_unfold.view(B,self.kernel_size,self.kernel_size,-1)
            # print('input_mask_unfold_expend.size() ',input_mask_unfold.size())

            centers = input_mask_unfold[:,center_size:center_size+1,center_size:center_size+1,:]
            # print('centers.size()',centers.size())
            
            input_mask_unfold = input_mask_unfold * centers
            # print('input_mask_unfold.size()',input_mask_unfold.size())
            
            delta_r_bin = torch.einsum('abcd,abcde->abcde',[input_mask_unfold, delta_r_bin.type_as(self.weight)])
            # print('delta_r_bin.size()',delta_r_bin.size())


        input_unfold = torch.nn.functional.unfold(input,self.kernel_size,stride=self.stride,
            padding=self.padding,dilation = self.dilation).view(B,C,self.kernel_size,self.kernel_size,-1)
        _,_,_,_,n_windows = input_unfold.size()

        # print('input_unfold.size()',input_unfold.size())


        # weight size : heads,in,out,kernel,kernel
        # delta_r size: batch,kernel,kernel,n_windows,heads 
        weight_refine = torch.einsum('abcde,fdega->fbcdeg',[self.weight, delta_r_bin.type_as(self.weight)])
        # print('weight_refine.size()',weight_refine.size())

        # weight_refine size: batch,in,out,kernel,kernel,n_windows 
        # input_unfold size : batch,in,kernel,kernel,n_windows 

        weight_refine = torch.einsum('abcde,fdega->fbcdeg',[self.weight, delta_r_bin.type_as(self.weight)])
        output = torch.einsum('abcdef,abdef->acf',[weight_refine, input_unfold])

        output = output.contiguous().view(B,-1,n_windows)

        # print('output.size()',output.size())
        output = torch.nn.functional.fold(output,(h,w),1)
        # print('output_fold.size()',output.size())
        
        
        if self.bias is not None:
            output = output + self.bias.view(1, -1, 1, 1)

        if self.use_res_connect:
            input = input.view(B, C, h, w)
            output = input+output
        return output




if __name__ == '__main__':
    
    layer = RangeQuantizedConv2d(5,64,3)

    sample = torch.rand(1,5,64,64)
    sample_coord = torch.rand(1,3,64,64)
    sample_mask = torch.rand(1,64,64)

    res = layer(sample,sample_coord,sample_mask)







    # x = torch.nn.functional.unfold(sample,3)
    # print(x,x.size())

    # x = torch.nn.functional.fold(x,(64,64),3)
    # print(x,x.size())
    