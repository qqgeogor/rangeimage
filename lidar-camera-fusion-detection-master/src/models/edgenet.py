import torch
import torch.nn as nn

from src.models.dla_edgeconv import DeepLayerAggregation,DeepLayerAggregationEdge,DeepLayerSimpleAggregation
# from src.models.dla import DeepLayerAggregation

from src.models.edgeconv import EdgeConv,SmartDownSampling,Dynamic_conv2d
from src.models.rqconv2d import RangeQuantizedConv2d
from src.models.edgeconv import EdgeConv,SmartDownSampling,Dynamic_conv2d,LightweightConv2d
kernel_func = nn.Conv2d


class CenternetDetector(nn.Module):
    
    def __init__(self, in_channels, n: int = 7):
        super(CenternetDetector, self).__init__()
        self.in_channels = in_channels
        self.num_classes = n

        self.classes = nn.Sequential(
            kernel_func(in_channels, self.in_channels, kernel_size=3,padding=1),
            nn.BatchNorm2d(in_channels),
            nn.ReLU(inplace=True),
            kernel_func(in_channels, self.num_classes, kernel_size=1),
            )
        
        # relative center (x, y, z), relative orientation (wx, wy) = (cos w, sin w), and dimensions l, w, h
        # + one channel for each class label
        self.bb_params = nn.Sequential(
            kernel_func(in_channels, self.in_channels, kernel_size=3,padding=1),
            nn.BatchNorm2d(in_channels),
            nn.ReLU(inplace=True),
            kernel_func(in_channels=in_channels,
                                   out_channels=8,
                                   kernel_size=1)
            )

        self.shared_conv = nn.Sequential(
            kernel_func(in_channels, in_channels,
            kernel_size=3, padding=1, bias=True),
            nn.BatchNorm2d(in_channels),
            nn.ReLU(inplace=True)
        )


    def _sigmoid(self, x):
        y = torch.clamp(x.sigmoid_(), min=1e-4, max=1-1e-4)
        return y
        
        
    def forward(self, x):
        
        x = self.shared_conv(x)

        class_pred = self.classes(x)
        
        class_pred = self._sigmoid(class_pred)

        bb_pred = self.bb_params(x)
        
        
        return class_pred,bb_pred


class EdgeNet(nn.Module):
    
    def __init__(self, in_channels=5,num_classes: int = 8,kernel=Dynamic_conv2d):
        """
            LaserNet implementation from https://arxiv.org/pdf/1903.08701.pdf almost..

            params:
                num_classes - number of target classes
        """
        super(EdgeNet, self).__init__()
        self.in_channels = in_channels
        self.num_classes = num_classes
        self.kernel = kernel
        self.dla = DeepLayerAggregation(in_channels=in_channels,kernel=kernel)
        
        self.detector = CenternetDetector(in_channels=64,n=num_classes)
        
        
    def forward(self, x) -> tuple:
        """
            input - tensor of size (N, 7, width, height) with main features
            input_coord - tensor of size (N, 3, width, height), aziumth, inclination, range
            input_coord - tensor of size (N, width, height) mask of validate pixel
        """
        
        # print(x.size())
        
        input = x[:,0:7,:,:]
        input_coord = x[:,7:10,:,:]
        input_mask = x[:,10:11,:,:]
        
        dla_out = self.dla(input,input_coord,input_mask)
        # dla_out = self.dla(input,)
        
        class_preds,reg_preds = self.detector(dla_out)
        
        # print('reg_preds',reg_preds)
        
        return class_preds,reg_preds



class EdgeFirstNet(nn.Module):
    
    def __init__(self, in_channels=5,num_classes: int = 8,kernel=Dynamic_conv2d):
        """
            LaserNet implementation from https://arxiv.org/pdf/1903.08701.pdf almost..

            params:
                num_classes - number of target classes
        """
        super(EdgeFirstNet, self).__init__()
        self.in_channels = in_channels
        self.num_classes = num_classes
        self.kernel = kernel
        self.dla = DeepLayerAggregationEdge(in_channels=in_channels,kernel=kernel)
        
        self.detector = CenternetDetector(in_channels=64,n=num_classes)
        
        
    def forward(self, x) -> tuple:
        """
            input - tensor of size (N, 7, width, height) with main features
            input_coord - tensor of size (N, 3, width, height), aziumth, inclination, range
            input_coord - tensor of size (N, width, height) mask of validate pixel
        """
        
        # print(x.size())
        
        input = x[:,0:7,:,:]
        input_coord = x[:,7:10,:,:]
        input_mask = x[:,10:11,:,:]
        
        dla_out = self.dla(input,input_coord,input_mask)
        # dla_out = self.dla(input,)
        
        class_preds,reg_preds = self.detector(dla_out)
        
        # print('reg_preds',reg_preds)
        
        return class_preds,reg_preds

class EdgeSimpleNet(nn.Module):
    
    def __init__(self, in_channels=5,num_classes: int = 8,kernel=Dynamic_conv2d):
        """
            LaserNet implementation from https://arxiv.org/pdf/1903.08701.pdf almost..

            params:
                num_classes - number of target classes
        """
        
        super(EdgeSimpleNet, self).__init__()
        in_channels = 7
        self.in_channels = in_channels
        self.num_classes = num_classes
        self.kernel = kernel
        self.dla = DeepLayerSimpleAggregation(in_channels=in_channels,kernel=kernel)
        
        self.detector = CenternetDetector(in_channels=64,n=num_classes)
        
        
    def forward(self, x) -> tuple:
        """
            input - tensor of size (N, 7, width, height) with main features
            input_coord - tensor of size (N, 3, width, height), aziumth, inclination, range
            input_coord - tensor of size (N, width, height) mask of validate pixel
        """
        
        # print(x.size())
        
        input = x[:,0:7,:,:]
        input_coord = x[:,7:10,:,:]
        input_mask = x[:,10:11,:,:]
        
        dla_out = self.dla(input,input_coord,input_mask)
        # dla_out = self.dla(input,)
        
        class_preds,reg_preds = self.detector(dla_out)
        
        # print('reg_preds',reg_preds)
        
        return class_preds,reg_preds
