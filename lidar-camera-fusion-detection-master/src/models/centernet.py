import torch
import torch.nn as nn

import logging
from collections import defaultdict
import torch
from torch import double, nn
import copy 
# class CenterHead(nn.Module):
#     def __init__(
#         self,
#         in_channels=[128,],
#         tasks=[],
#         dataset='nuscenes',
#         weight=0.25,
#         code_weights=[],
#         common_heads=dict(),
#         logger=None,
#         init_bias=-2.19,
#         share_conv_channel=64,
#         num_hm_conv=2,
#         dcn_head=False,
#     ):
#         super(CenterHead, self).__init__()

#         num_classes = [len(t["class_names"]) for t in tasks]
#         self.class_names = [t["class_names"] for t in tasks]
#         self.code_weights = code_weights 
#         self.weight = weight  # weight between hm loss and loc loss
#         self.dataset = dataset

#         self.in_channels = in_channels
#         self.num_classes = num_classes

#         self.crit = FastFocalLoss()
#         self.crit_reg = RegLoss()
        
#         self.box_n_dim = 9 if 'vel' in common_heads else 7  
#         self.use_direction_classifier = False 
        
#         if not logger:
#             logger = logging.getLogger("CenterHead")
#         self.logger = logger

#         logger.info(
#             f"num_classes: {num_classes}"
#         )

#         # a shared convolution 
#         self.shared_conv = nn.Sequential(
#             nn.Conv2d(in_channels, share_conv_channel,
#             kernel_size=3, padding=1, bias=True),
#             nn.BatchNorm2d(share_conv_channel),
#             nn.ReLU(inplace=True)
#         )

#         self.tasks = nn.ModuleList()
#         print("Use HM Bias: ", init_bias)

#         if dcn_head:
#             print("Use Deformable Convolution in the CenterHead!")

#         for num_cls in num_classes:
#             heads = copy.deepcopy(common_heads)
#             if not dcn_head:
#                 heads.update(dict(hm=(num_cls, num_hm_conv)))
#                 self.tasks.append(
#                     SepHead(share_conv_channel, heads, bn=True, init_bias=init_bias, final_kernel=3)
#                 )
#             else:
#                 self.tasks.append(
#                     DCNSepHead(share_conv_channel, num_cls, heads, bn=True, init_bias=init_bias, final_kernel=3)
#                 )

#         logger.info("Finish CenterHead Initialization")

#     def forward(self, x, *kwargs):
#         ret_dicts = []

#         x = self.shared_conv(x)

#         for task in self.tasks:
#             ret_dicts.append(task(x))

#         return ret_dicts

#     def _sigmoid(self, x):
#         y = torch.clamp(x.sigmoid_(), min=1e-4, max=1-1e-4)
#         return y

#     def loss(self, example, preds_dicts, **kwargs):
#         rets = []
#         for task_id, preds_dict in enumerate(preds_dicts):
#             # heatmap focal loss
#             preds_dict['hm'] = self._sigmoid(preds_dict['hm'])

#             hm_loss = self.crit(preds_dict['hm'], example['hm'][task_id], example['ind'][task_id], example['mask'][task_id], example['cat'][task_id])

#             target_box = example['anno_box'][task_id]
#             # reconstruct the anno_box from multiple reg heads
#             if self.dataset in ['waymo', 'nuscenes']:
#                 if 'vel' in preds_dict:
#                     preds_dict['anno_box'] = torch.cat((preds_dict['reg'], preds_dict['height'], preds_dict['dim'],
#                                                         preds_dict['vel'], preds_dict['rot']), dim=1)  
#                 else:
#                     preds_dict['anno_box'] = torch.cat((preds_dict['reg'], preds_dict['height'], preds_dict['dim'],
#                                                         preds_dict['rot']), dim=1)   
#                     target_box = target_box[..., [0, 1, 2, 3, 4, 5, -2, -1]] # remove vel target                       
#             else:
#                 raise NotImplementedError()

#             ret = {}
 
#             # Regression loss for dimension, offset, height, rotation            
#             box_loss = self.crit_reg(preds_dict['anno_box'], example['mask'][task_id], example['ind'][task_id], target_box)

#             loc_loss = (box_loss*box_loss.new_tensor(self.code_weights)).sum()

#             loss = hm_loss + self.weight*loc_loss

#             ret.update({'loss': loss, 'hm_loss': hm_loss.detach().cpu(), 'loc_loss':loc_loss, 'loc_loss_elem': box_loss.detach().cpu(), 'num_positive': example['mask'][task_id].float().sum()})

#             rets.append(ret)
        
#         """convert batch-key to key-batch
#         """
#         rets_merged = defaultdict(list)
#         for ret in rets:
#             for k, v in ret.items():
#                 rets_merged[k].append(v)

#         return rets_merged

#     @torch.no_grad()
#     def predict(self, example, preds_dicts, test_cfg, **kwargs):
#         """decode, nms, then return the detection result. Additionaly support double flip testing 
#         """
#         # get loss info
#         rets = []
#         metas = []

#         double_flip = test_cfg.get('double_flip', False)

#         post_center_range = test_cfg.post_center_limit_range
#         if len(post_center_range) > 0:
#             post_center_range = torch.tensor(
#                 post_center_range,
#                 dtype=preds_dicts[0]['hm'].dtype,
#                 device=preds_dicts[0]['hm'].device,
#             )

#         for task_id, preds_dict in enumerate(preds_dicts):
#             # convert N C H W to N H W C 
#             for key, val in preds_dict.items():
#                 preds_dict[key] = val.permute(0, 2, 3, 1).contiguous()

#             batch_size = preds_dict['hm'].shape[0]

#             if double_flip:
#                 assert batch_size % 4 == 0, print(batch_size)
#                 batch_size = int(batch_size / 4)
#                 for k in preds_dict.keys():
#                     # transform the prediction map back to their original coordinate befor flipping
#                     # the flipped predictions are ordered in a group of 4. The first one is the original pointcloud
#                     # the second one is X flip pointcloud(y=-y), the third one is Y flip pointcloud(x=-x), and the last one is 
#                     # X and Y flip pointcloud(x=-x, y=-y).
#                     # Also please note that pytorch's flip function is defined on higher dimensional space, so dims=[2] means that
#                     # it is flipping along the axis with H length(which is normaly the Y axis), however in our traditional word, it is flipping along
#                     # the X axis. The below flip follows pytorch's definition yflip(y=-y) xflip(x=-x)
#                     _, H, W, C = preds_dict[k].shape
#                     preds_dict[k] = preds_dict[k].reshape(int(batch_size), 4, H, W, C)
#                     preds_dict[k][:, 1] = torch.flip(preds_dict[k][:, 1], dims=[1]) 
#                     preds_dict[k][:, 2] = torch.flip(preds_dict[k][:, 2], dims=[2])
#                     preds_dict[k][:, 3] = torch.flip(preds_dict[k][:, 3], dims=[1, 2])

#             if "metadata" not in example or len(example["metadata"]) == 0:
#                 meta_list = [None] * batch_size
#             else:
#                 meta_list = example["metadata"]
#                 if double_flip:
#                     meta_list = meta_list[:4*int(batch_size):4]

#             batch_hm = torch.sigmoid(preds_dict['hm'])

#             batch_dim = torch.exp(preds_dict['dim'])

#             batch_rots = preds_dict['rot'][..., 0:1]
#             batch_rotc = preds_dict['rot'][..., 1:2]
#             batch_reg = preds_dict['reg']
#             batch_hei = preds_dict['height']

#             if double_flip:
#                 batch_hm = batch_hm.mean(dim=1)
#                 batch_hei = batch_hei.mean(dim=1)
#                 batch_dim = batch_dim.mean(dim=1)

#                 # y = -y reg_y = 1-reg_y
#                 batch_reg[:, 1, ..., 1] = 1 - batch_reg[:, 1, ..., 1]
#                 batch_reg[:, 2, ..., 0] = 1 - batch_reg[:, 2, ..., 0]

#                 batch_reg[:, 3, ..., 0] = 1 - batch_reg[:, 3, ..., 0]
#                 batch_reg[:, 3, ..., 1] = 1 - batch_reg[:, 3, ..., 1]
#                 batch_reg = batch_reg.mean(dim=1)

#                 # first yflip 
#                 # y = -y theta = pi -theta
#                 # sin(pi-theta) = sin(theta) cos(pi-theta) = -cos(theta)
#                 # batch_rots[:, 1] the same
#                 batch_rotc[:, 1] *= -1

#                 # then xflip x = -x theta = 2pi - theta
#                 # sin(2pi - theta) = -sin(theta) cos(2pi - theta) = cos(theta)
#                 # batch_rots[:, 2] the same
#                 batch_rots[:, 2] *= -1

#                 # double flip 
#                 batch_rots[:, 3] *= -1
#                 batch_rotc[:, 3] *= -1

#                 batch_rotc = batch_rotc.mean(dim=1)
#                 batch_rots = batch_rots.mean(dim=1)

#             batch_rot = torch.atan2(batch_rots, batch_rotc)

#             batch, H, W, num_cls = batch_hm.size()

#             batch_reg = batch_reg.reshape(batch, H*W, 2)
#             batch_hei = batch_hei.reshape(batch, H*W, 1)

#             batch_rot = batch_rot.reshape(batch, H*W, 1)
#             batch_dim = batch_dim.reshape(batch, H*W, 3)
#             batch_hm = batch_hm.reshape(batch, H*W, num_cls)

#             ys, xs = torch.meshgrid([torch.arange(0, H), torch.arange(0, W)])
#             ys = ys.view(1, H, W).repeat(batch, 1, 1).to(batch_hm)
#             xs = xs.view(1, H, W).repeat(batch, 1, 1).to(batch_hm)

#             xs = xs.view(batch, -1, 1) + batch_reg[:, :, 0:1]
#             ys = ys.view(batch, -1, 1) + batch_reg[:, :, 1:2]

#             xs = xs * test_cfg.out_size_factor * test_cfg.voxel_size[0] + test_cfg.pc_range[0]
#             ys = ys * test_cfg.out_size_factor * test_cfg.voxel_size[1] + test_cfg.pc_range[1]

#             if 'vel' in preds_dict:
#                 batch_vel = preds_dict['vel']

#                 if double_flip:
#                     # flip vy
#                     batch_vel[:, 1, ..., 1] *= -1
#                     # flip vx
#                     batch_vel[:, 2, ..., 0] *= -1

#                     batch_vel[:, 3] *= -1
                    
#                     batch_vel = batch_vel.mean(dim=1)

#                 batch_vel = batch_vel.reshape(batch, H*W, 2)
#                 batch_box_preds = torch.cat([xs, ys, batch_hei, batch_dim, batch_vel, batch_rot], dim=2)
#             else: 
#                 batch_box_preds = torch.cat([xs, ys, batch_hei, batch_dim, batch_rot], dim=2)

#             metas.append(meta_list)

#             if test_cfg.get('per_class_nms', False):
#                 pass 
#             else:
#                 rets.append(self.post_processing(batch_box_preds, batch_hm, test_cfg, post_center_range, task_id)) 

#         # Merge branches results
#         ret_list = []
#         num_samples = len(rets[0])

#         ret_list = []
#         for i in range(num_samples):
#             ret = {}
#             for k in rets[0][i].keys():
#                 if k in ["box3d_lidar", "scores"]:
#                     ret[k] = torch.cat([ret[i][k] for ret in rets])
#                 elif k in ["label_preds"]:
#                     flag = 0
#                     for j, num_class in enumerate(self.num_classes):
#                         rets[j][i][k] += flag
#                         flag += num_class
#                     ret[k] = torch.cat([ret[i][k] for ret in rets])

#             ret['metadata'] = metas[0][i]
#             ret_list.append(ret)

#         return ret_list 

#     @torch.no_grad()
#     def post_processing(self, batch_box_preds, batch_hm, test_cfg, post_center_range, task_id):
#         batch_size = len(batch_hm)

#         prediction_dicts = []
#         for i in range(batch_size):
#             box_preds = batch_box_preds[i]
#             hm_preds = batch_hm[i]

#             scores, labels = torch.max(hm_preds, dim=-1)

#             score_mask = scores > test_cfg.score_threshold
#             distance_mask = (box_preds[..., :3] >= post_center_range[:3]).all(1) \
#                 & (box_preds[..., :3] <= post_center_range[3:]).all(1)

#             mask = distance_mask & score_mask 

#             box_preds = box_preds[mask]
#             scores = scores[mask]
#             labels = labels[mask]

#             boxes_for_nms = box_preds[:, [0, 1, 2, 3, 4, 5, -1]]

#             if test_cfg.get('circular_nms', False):
#                 centers = boxes_for_nms[:, [0, 1]] 
#                 boxes = torch.cat([centers, scores.view(-1, 1)], dim=1)
#                 selected = _circle_nms(boxes, min_radius=test_cfg.min_radius[task_id], post_max_size=test_cfg.nms.nms_post_max_size)  
#             else:
#                 selected = box_torch_ops.rotate_nms_pcdet(boxes_for_nms.float(), scores.float(), 
#                                     thresh=test_cfg.nms.nms_iou_threshold,
#                                     pre_maxsize=test_cfg.nms.nms_pre_max_size,
#                                     post_max_size=test_cfg.nms.nms_post_max_size)

#             selected_boxes = box_preds[selected]
#             selected_scores = scores[selected]
#             selected_labels = labels[selected]

#             prediction_dict = {
#                 'box3d_lidar': selected_boxes,
#                 'scores': selected_scores,
#                 'label_preds': selected_labels
#             }

#             prediction_dicts.append(prediction_dict)

#         return prediction_dicts 

import numpy as np 
def _circle_nms(boxes, min_radius, post_max_size=83):
    """
    NMS according to center distance
    """
    keep = np.array(circle_nms(boxes.cpu().numpy(), thresh=min_radius))[:post_max_size]

    keep = torch.from_numpy(keep).long().to(boxes.device)

    return keep  
    
# 0.25 for pedestrians
# 0.5 for vehicles
def gaussian(alpha,gamma=0.25):
    phi = torch.exp(-gamma*alpha.pow(2))
    return phi

class RBF(nn.Module):
    """
    Transforms incoming data using a given radial basis function:
    u_{i} = rbf(||x - c_{i}|| / s_{i})
    Arguments:
        in_features: size of each input sample
        out_features: size of each output sample
    Shape:
        - Input: (N, in_features) where N is an arbitrary batch size
        - Output: (N, out_features) where N is an arbitrary batch size
    Attributes:
        centres: the learnable centres of shape (out_features, in_features).
            The values are initialised from a standard normal distribution.
            Normalising inputs to have mean 0 and standard deviation 1 is
            recommended.
        
        sigmas: the learnable scaling factors of shape (out_features).
            The values are initialised as ones.
        
        basis_func: the radial basis function used to transform the scaled
            distances.
    """

    def __init__(self, in_features, out_features, basis_func=gaussian,gamma=0.25):
        super(RBF, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.gamma = gamma
        self.centres = nn.Parameter(torch.Tensor(out_features, in_features))
        self.sigmas = nn.Parameter(torch.Tensor(out_features))
        self.basis_func = basis_func
        self.reset_parameters()

    def reset_parameters(self):
        nn.init.normal_(self.centres, 0, 1)
        nn.init.constant_(self.sigmas, 1)

    def forward(self, input):
        size = (input.size(0), self.out_features, self.in_features)
        x = input.unsqueeze(1).expand(size)
        c = self.centres.unsqueeze(0).expand(size)

        distances = (x - c).pow(2).sum(-1).pow(0.5) * self.sigmas.unsqueeze(0)
        return self.basis_func(distances,gamma=self.gamma)



class CenternetDetector(nn.Module):
	
    def __init__(self, in_channels, n: int = 8,gamma=0.25):
        super(CenternetDetector, self).__init__()
        self.gamma = gamma
        self.classes = nn.Sequential(
            nn.Conv2d(in_channels, self.in_channels, kernel_size=(3, 3)),
            nn.ReLU(),
            nn.Conv2d(in_channels, self.num_classes, kernel_size=(1, 1))
            )
        
        # relative center (x, y, z), relative orientation (wx, wy) = (cos w, sin w), and dimensions l, w, h
        # + one channel for each class label
        self.bb_params = nn.Sequential(
            nn.Conv2d(in_channels, self.in_channels, kernel_size=(3, 3)),
            nn.ReLU(),
            nn.Conv2d(in_channels=in_channels,
                                   out_channels=n,
                                   kernel_size=(1, 1))
            )
        
        
    def forward(self, x):
        
        class_pred = self.classes(x)
        
        bb_pred = self.bb_params(x)
        
        
        return class_pred,bb_pred
