import torch
import numpy as np
# from pyquaternion import Quaternion
import threading 
# from src.settings import NUSCENES, RV_WIDTH, RV_HEIGHT, LABEL_NUMBER
import numpy as np
import cv2


import numba 
import numpy as np 

@numba.jit(nopython=True)
def circle_nms(dets, thresh):
    x1 = dets[:, 0]
    y1 = dets[:, 1]
    z1 = dets[:, 2]
    scores = dets[:, 3]
    order = scores.argsort()[::-1].astype(np.int32)  # highest->lowest
    ndets = dets.shape[0]
    suppressed = np.zeros((ndets), dtype=np.int32)
    keep = []
    for _i in range(ndets):
        i = order[_i]  # start with highest score box
        if suppressed[i] == 1:  # if any box have enough iou with this, remove it
            continue
        keep.append(i)
        for _j in range(_i + 1, ndets):
            j = order[_j]
            if suppressed[j] == 1:
                continue
            # calculate center distance between i and j box
            dist = (x1[i]-x1[j])**2 + (y1[i]-y1[j])**2

            # ovr = inter / areas[j]
            if dist <= thresh:
                suppressed[j] = 1
    return keep




def _circle_nms(boxes, min_radius, post_max_size=83):
    """
    NMS according to center distance
    """
    keep = np.array(circle_nms(boxes.detach().cpu().numpy(), thresh=min_radius))[:post_max_size]

    keep = torch.from_numpy(keep).long().to(boxes.device)

    return keep 


def post_nms(hm_preds,box_preds,min_radius=2,post_max_size=100):

    hm_preds = hm_preds
    box_preds = box_preds

    scores, labels = torch.max(hm_preds, dim=-1)

    score_mask = scores > 0.2
    mask = score_mask 
    
    # print('box_preds',box_preds.size())
    box_preds = box_preds[mask]
    
    
    # print('box_preds',box_preds.size())
    scores = scores[mask]
    labels = labels[mask]
    
    boxes_for_nms = box_preds[:, [0, 1, 2, 3, 4, 5, -1]]
    #print('boxes_for_nms',boxes_for_nms)
    centers = boxes_for_nms[:, [0, 1, 2]] 
    boxes = torch.cat([centers, scores.view(-1, 1)], dim=1)
    #print('boxes',boxes,boxes.size())
    selected = _circle_nms(boxes, min_radius=min_radius, post_max_size=post_max_size)  
    
    # print('box_preds',box_preds.size())
    selected_boxes = box_preds[selected]
    selected_scores = scores[selected]
    selected_labels = labels[selected]
    
    return selected,selected_boxes,selected_scores,selected_labels


def gaussian_radius(det_size, min_overlap=0.5):
    height, width = det_size

    a1  = 1
    b1  = (height + width)
    c1  = width * height * (1 - min_overlap) / (1 + min_overlap)
    sq1 = np.sqrt(b1 ** 2 - 4 * a1 * c1)
    r1  = (b1 + sq1) / 2

    a2  = 4
    b2  = 2 * (height + width)
    c2  = (1 - min_overlap) * width * height
    sq2 = np.sqrt(b2 ** 2 - 4 * a2 * c2)
    r2  = (b2 + sq2) / 2

    a3  = 4 * min_overlap
    b3  = -2 * min_overlap * (height + width)
    c3  = (min_overlap - 1) * width * height
    sq3 = np.sqrt(b3 ** 2 - 4 * a3 * c3)
    r3  = (b3 + sq3) / 2
    return min(r1, r2, r3)

def gaussian2D(shape, sigma=1):
    m, n = [(ss - 1.) / 2. for ss in shape]
    y, x = np.ogrid[-m:m+1,-n:n+1]

    h = np.exp(-(x * x + y * y) / (2 * sigma * sigma))
    h[h < np.finfo(h.dtype).eps * h.max()] = 0
    return h


def draw_umich_gaussian(heatmap, center, radius, k=1):
    diameter = 2 * radius + 1
    gaussian = gaussian2D((diameter, diameter), sigma=diameter / 6)

    x, y = int(center[0]), int(center[1])

    height, width = heatmap.shape[0:2]

    left, right = min(x, radius), min(width - x, radius + 1)
    top, bottom = min(y, radius), min(height - y, radius + 1)

    masked_heatmap  = heatmap[y - top:y + bottom, x - left:x + right]
    masked_gaussian = gaussian[radius - top:radius + bottom, radius - left:radius + right]
    if min(masked_gaussian.shape) > 0 and min(masked_heatmap.shape) > 0: # TODO debug
        np.maximum(masked_heatmap, masked_gaussian * k, out=masked_heatmap)
    return heatmap

def _gather_feat(feat, ind, mask=None):
    dim  = feat.size(2)
    ind  = ind.unsqueeze(2).expand(ind.size(0), ind.size(1), dim)
    feat = feat.gather(1, ind)
    if mask is not None:
        mask = mask.unsqueeze(2).expand_as(feat)
        feat = feat[mask]
        feat = feat.view(-1, dim)
    return feat

def _transpose_and_gather_feat(feat, ind):
    feat = feat.permute(0, 2, 3, 1).contiguous()
    feat = feat.view(feat.size(0), -1, feat.size(3))
    feat = _gather_feat(feat, ind)
    return feat



def bilinear_interpolate_torch(im, x, y):
    """
    Args:
        im: (H, W, C) [y, x]
        x: (N)
        y: (N)
    Returns:
    """
    x0 = torch.floor(x).long()
    x1 = x0 + 1

    y0 = torch.floor(y).long()
    y1 = y0 + 1

    x0 = torch.clamp(x0, 0, im.shape[1] - 1)
    x1 = torch.clamp(x1, 0, im.shape[1] - 1)
    y0 = torch.clamp(y0, 0, im.shape[0] - 1)
    y1 = torch.clamp(y1, 0, im.shape[0] - 1)

    Ia = im[y0, x0]
    Ib = im[y1, x0]
    Ic = im[y0, x1]
    Id = im[y1, x1]

    wa = (x1.type_as(x) - x) * (y1.type_as(y) - y)
    wb = (x1.type_as(x) - x) * (y - y0.type_as(y))
    wc = (x - x0.type_as(x)) * (y1.type_as(y) - y)
    wd = (x - x0.type_as(x)) * (y - y0.type_as(y))
    ans = torch.t((torch.t(Ia) * wa)) + torch.t(torch.t(Ib) * wb) + torch.t(torch.t(Ic) * wc) + torch.t(torch.t(Id) * wd)
    return ans

def compute_box_3d(dim, location, rotation_y):
  # dim: 3
  # location: 3
  # rotation_y: 1
  # return: 8 x 3
  c, s = np.cos(rotation_y), np.sin(rotation_y)
  R = np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]], dtype=np.float32)
  l, w, h = dim[2], dim[1], dim[0]
  x_corners = [l/2, l/2, -l/2, -l/2, l/2, l/2, -l/2, -l/2]
  y_corners = [0,0,0,0,-h,-h,-h,-h]
  z_corners = [w/2, -w/2, -w/2, w/2, w/2, -w/2, -w/2, w/2]

  corners = np.array([x_corners, y_corners, z_corners], dtype=np.float32)
  corners_3d = np.dot(R, corners) 
  corners_3d = corners_3d + np.array(location, dtype=np.float32).reshape(3, 1)
  return corners_3d.transpose(1, 0)

def project_to_image(pts_3d, P):
  # pts_3d: n x 3
  # P: 3 x 4
  # return: n x 2
  pts_3d_homo = np.concatenate(
    [pts_3d, np.ones((pts_3d.shape[0], 1), dtype=np.float32)], axis=1)
  pts_2d = np.dot(P, pts_3d_homo.transpose(1, 0)).transpose(1, 0)
  pts_2d = pts_2d[:, :2] / pts_2d[:, 2:]
  # import pdb; pdb.set_trace()
  return pts_2d

def compute_orientation_3d(dim, location, rotation_y):
  # dim: 3
  # location: 3
  # rotation_y: 1
  # return: 2 x 3
  c, s = np.cos(rotation_y), np.sin(rotation_y)
  R = np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]], dtype=np.float32)
  orientation_3d = np.array([[0, dim[2]], [0, 0], [0, 0]], dtype=np.float32)
  orientation_3d = np.dot(R, orientation_3d)
  orientation_3d = orientation_3d + \
                   np.array(location, dtype=np.float32).reshape(3, 1)
  return orientation_3d.transpose(1, 0)

def draw_box_3d(image, corners, c=(0, 0, 255)):
    face_idx = [[0,1,5,4],
              [1,2,6, 5],
              [2,3,7,6],
              [3,0,4,7]]
    for ind_f in range(3, -1, -1):
        f = face_idx[ind_f]
    for j in range(4):
      cv2.line(image, (corners[f[j], 0], corners[f[j], 1]),
               (corners[f[(j+1)%4], 0], corners[f[(j+1)%4], 1]), c, 2, lineType=cv2.LINE_AA)
    if ind_f == 0:
      cv2.line(image, (corners[f[0], 0], corners[f[0], 1]),
               (corners[f[2], 0], corners[f[2], 1]), c, 1, lineType=cv2.LINE_AA)
      cv2.line(image, (corners[f[1], 0], corners[f[1], 1]),
               (corners[f[3], 0], corners[f[3], 1]), c, 1, lineType=cv2.LINE_AA)
    return image

def unproject_2d_to_3d(pt_2d, depth, P):
    # pts_2d: 2
    # depth: 1
    # P: 3 x 4
    # return: 3
    z = depth - P[2, 3]
    x = (pt_2d[0] * depth - P[0, 3] - P[0, 2] * z) / P[0, 0]
    y = (pt_2d[1] * depth - P[1, 3] - P[1, 2] * z) / P[1, 1]
    pt_3d = np.array([x, y, z], dtype=np.float32)
    return pt_3d

def alpha2rot_y(alpha, x, cx, fx):
    """
    Get rotation_y by alpha + theta - 180
    alpha : Observation angle of object, ranging [-pi..pi]
    x : Object center x to the camera center (x-W/2), in pixels
    rotation_y : Rotation ry around Y-axis in camera coordinates [-pi..pi]
    """
    rot_y = alpha + np.arctan2(x - cx, fx)
    if rot_y > np.pi:
      rot_y -= 2 * np.pi
    if rot_y < -np.pi:
      rot_y += 2 * np.pi
    return rot_y

def rot_y2alpha(rot_y, x, cx, fx):
    """
    Get rotation_y by alpha + theta - 180
    alpha : Observation angle of object, ranging [-pi..pi]
    x : Object center x to the camera center (x-W/2), in pixels
    rotation_y : Rotation ry around Y-axis in camera coordinates [-pi..pi]
    """
    alpha = rot_y - np.arctan2(x - cx, fx)
    if alpha > np.pi:
      alpha -= 2 * np.pi
    if alpha < -np.pi:
      alpha += 2 * np.pi
    return alpha


def ddd2locrot(center, alpha, dim, depth, calib):
    # single image
    locations = unproject_2d_to_3d(center, depth, calib)
    locations[1] += dim[0] / 2
    rotation_y = alpha2rot_y(alpha, center[0], calib[0, 2], calib[0, 0])
    return locations, rotation_y

def project_3d_bbox(location, dim, rotation_y, calib):
    box_3d = compute_box_3d(dim, location, rotation_y)
    box_2d = project_to_image(box_3d, calib)
    return box_2d


def rotation_matrix(angles: torch.Tensor):
    """
    :param angles:
    :return:
    """

    cos, sin = torch.cos(angles), torch.sin(angles)
    s1 = torch.stack((cos, -sin))  # 2xNx128x32
    s2 = torch.stack((cos, sin))  # 2xNx128x32
    
    s3 = torch.stack((s1, s2)).squeeze(2) # 2x2xNx128x32

#     # check that stacking went as expected
#     assert s3[0, 1, 0, 0, 0] == s1[1, 0, 0, 0]
#     assert s3[1, 0, 0, 0, 0] == s2[0, 0, 0, 0]
#     assert s3[1, 1, 0, 0, 0] == s2[1, 0, 0, 0]

    return s3  # 2x2xNxRV_WIDTHxRV_HEIGHT 


def params_to_box_centers(point_center_params: torch.Tensor,
                          point_coordinates: torch.Tensor,
                          angles: torch.Tensor) -> torch.Tensor:  # 1
    """
    this function converts relative bounding box parameters to coordinates

    :param point_center_params:  N x 2 x RV_WIDTH x RV_HEIGHT
    :param point_coordinates: N x 2 x RV_WIDTH x RV_HEIGHT
    :param angles:
    :return:
    """
    rotation_matrices = rotation_matrix(angles)  # 2 x 2 x N x RV_WIDTH x RV_HEIGHT
    rotation_matrices = rotation_matrices.permute(2, 3, 4, 0, 1)  # N x RV_WIDTH x RV_HEIGHT x 2 x 2

    point_center_params = point_center_params.permute(0, 2, 3, 1).unsqueeze(4)  # N x RV_WIDTH x RV_HEIGHT x 2 x 1
    point_coordinates = point_coordinates.permute(0, 2, 3, 1).unsqueeze(4)  # N x RV_WIDTH x RV_HEIGHT x 2 x 1

    abs_coords = point_coordinates + rotation_matrices @ point_center_params  # N x RV_WIDTH x RV_HEIGHT x 2 x 1
    
    return abs_coords.squeeze(4).permute(0, 3, 1, 2)  # N x 2 x RV_WIDTH x RV_HEIGHT


def params_to_corners(bb_center_coords: torch.Tensor,
                      bb_orientations: torch.Tensor,
                      lengths: torch.Tensor,
                      widths: torch.Tensor) -> torch.Tensor:  # 2
    """
    :param bb_center_coords: N x 2 x RV_WIDTH x RV_HEIGHT
    :param bb_orientations:  N x RV_WIDTH x RV_HEIGHT
    :param lengths:  N x RV_WIDTH x RV_HEIGHT
    :param widths: N x RV_WIDTH x RV_HEIGHT
    :return:
    """
    

    R = rotation_matrix(bb_orientations).permute(2, 3, 4, 0, 1) # N x RV_WIDTH x RV_HEIGHT x 2 x 2

    b1 = R @ torch.stack(( lengths,  widths)).permute(1, 2, 3, 0).unsqueeze(4) # N x RV_WIDTH x RV_HEIGHT x 2 x 1
    b2 = R @ torch.stack(( lengths, -widths)).permute(1, 2, 3, 0).unsqueeze(4) 
    b3 = R @ torch.stack((-lengths, -widths)).permute(1, 2, 3, 0).unsqueeze(4) 
    b4 = R @ torch.stack((-lengths,  widths)).permute(1, 2, 3, 0).unsqueeze(4)

    b1 = bb_center_coords + b1.squeeze(4).permute(0, 3, 1, 2) / 2 
    b2 = bb_center_coords + b2.squeeze(4).permute(0, 3, 1, 2) / 2
    b3 = bb_center_coords + b3.squeeze(4).permute(0, 3, 1, 2) / 2
    b4 = bb_center_coords + b4.squeeze(4).permute(0, 3, 1, 2) / 2

    b = torch.hstack((b1, b2, b3, b4))

    return b


def params_to_box_corners(bb_params: torch.Tensor,
                          point_coordinates: torch.Tensor,
                          angles: torch.Tensor) -> torch.Tensor:
    """
        This function turns relative predicted bounding box parameters
        into 4 coordinates of a box 
        
    :param bb_params: tensor of size    [N, 6, RV_WIDTH, RV_HEIGHT],
                      6 components are  [d_x, d_y, w_x, w_y, length, width]
    :param point_coordinates: point coordinates in vehicle ego space
    :param angles:
    :return: tensor of size    [N, 8, RV_WIDTH, RV_HEIGHT]
    """

    (point_centers_xy,
     w_x, w_y,
     lengths, widths) = (bb_params[:, :2],
                         bb_params[:, 2], bb_params[:, 3],
                         bb_params[:, 4], bb_params[:, 5])
    
    angles = torch.deg2rad(angles)  # later in rotation matrix we will need radians, not degrees
    
    bb_center_coords = params_to_box_centers(point_centers_xy, point_coordinates, angles)  # N x 2 x RV_WIDTH x RV_HEIGHT

    bb_orientation = angles + torch.atan2(w_y, w_x)  # N x RV_WIDTH x RV_HEIGHT


    bb_corners = params_to_corners(bb_center_coords,
                                   bb_orientation,
                                   lengths,
                                   widths)

    return bb_corners


# def get_front_bb(sample: dict, nusc):
#     """
#     Function computes and returns
#     An array of points points of a bounding box in sensor coordinates.
#     Each point is a (x, y) coordinate, each BB is 4 points

#     :param sample: nuscenes sample dictionary
#     :return: np.array of shape (N, 8, 3)
#     """
#     my_sample_lidar_data = nusc.get('sample_data', sample['data']['LIDAR_TOP'])
#     sample_annotation = nusc.get_boxes(my_sample_lidar_data['token'])

#     ego_record = nusc.get('ego_pose', my_sample_lidar_data['ego_pose_token'])
#     cs_record = nusc.get('calibrated_sensor', my_sample_lidar_data['calibrated_sensor_token'])

#     # first step: transform from absolute to ego
#     ego_translation = -np.array(ego_record['translation'])

#     # second step: transform from ego to sensor
#     cs_translation = -np.array(cs_record['translation'])

#     corners = []
#     for box in sample_annotation:
#         box.translate(ego_translation)
#         box.rotate(Quaternion(ego_record['rotation']).inverse)

#         box.translate(cs_translation)
#         box.rotate(Quaternion(cs_record['rotation']).inverse)

#         # at this point bounding boxes are in sensor coordinate system
#         # now we want to exclude such BB that do not have their center
#         # lying in the front 90 degrees

#         if box.center[1] <= 0:
#             continue

#         box.center_azimuth = np.degrees(np.arctan(box.center[0] / box.center[1]))

#         # Transform front azimuth to be in range from 0 to 180
#         box.center_azimuth = 90 - box.center_azimuth
#         if not (45 < box.center_azimuth < 135):
#             continue

#         corners.append(box.bottom_corners())
        
#     return np.transpose(np.array(corners).T, (2, 0, 1))


# def get_bb_targets(coordinates, bounding_box_corners):
#     """
#     coordinate.shape (N, 3, RV_WIDTH, RV_HEIGHT)
#     bounding_box_corners.shape (N, M_n, 4, 3) where M_n is different for each N
#     bounding_box_labeles.shape (N, M_n, 1) where M_n is different for each N
#     """

#     bb_targets = []
#     for sample_i in range(len(coordinates)):
#         bbc_targets_single_rv = np.zeros((4, 3, RV_WIDTH, RV_HEIGHT))

#         sample_coords = coordinates[sample_i]
#         s_coords_x = sample_coords[0]
#         s_coords_y = sample_coords[1]

#         sample_boxes_corners = bounding_box_corners[sample_i]

#         # bounding_box_corners[cs_i] - bounding box corners for according point cloud
#         for bb_i in range(len(sample_boxes_corners)):
#             bb_c = sample_boxes_corners[bb_i]  # bb_c[left_top, left_bottom, right_bottom, right_top]

#             min_bb_x = bb_c[:, 0].min()
#             max_bb_x = bb_c[:, 0].max()
#             min_bb_y = bb_c[:, 1].min()
#             max_bb_y = bb_c[:, 1].max()
        
#             c1 = min_bb_x <= s_coords_x  # left_top/left_bottom.x <= coordinate.x
#             c2 = max_bb_x >= s_coords_x  # right_bottom/right_top.x >= coordinate.x
#             c3 = min_bb_y <= s_coords_y  # left/right_bottom.y <= coordinate.y
#             c4 = max_bb_y >= s_coords_y  # right_top/left_top.y >= coordinate.y
            
#             c = np.logical_and(np.logical_and(c1, c2),
#                                np.logical_and(c3, c4))

#             bbc_targets_single_rv[:, :, c] = np.expand_dims(bb_c, 2)

#         bbc_targets_single_rv = bbc_targets_single_rv[:, :2].reshape((8, RV_WIDTH, RV_HEIGHT))

#         bb_targets.append(bbc_targets_single_rv)

#     return np.array(bb_targets)


# def accuracy(inputs, targets):
#     pred_labels = torch.argmax(inputs, axis=1)
#     true_labels = torch.argmax(targets, axis=1)
        
#     correct_preds = torch.sum(pred_labels == true_labels)
#     all_points = torch.sum(true_labels == true_labels)
        
#     return correct_preds / all_points


# def get_heatmap(center,dim,angle,feature_map_size=(128,128),n_classes=2,output_stride=2,max_objs=100,min_overlap=0.1,pc_range=[-75.2, -75.2]):
#     hm = np.zeros((n_classes, feature_map_size[0], feature_map_size[1]),
#                               dtype=np.float32)
#     w, l, h= dim
#     x,y,z = center


#     coor_x, coor_y = (x - pc_range[0]) / voxel_size[0] / self.out_size_factor, \
#                      (y - pc_range[1]) / voxel_size[1] / self.out_size_factor

#     ct = np.array(
#         [coor_x, coor_y], dtype=np.float32)  
#     ct_int = ct.astype(np.int32)

#     # throw out not in range objects to avoid out of array area when creating the heatmap
#     if not (0 <= ct_int[0] < feature_map_size[0] and 0 <= ct_int[1] < feature_map_size[1]):
#         continue 

#     draw_gaussian(hm[cls_id], ct, radius)

