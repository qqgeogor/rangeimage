
import torch
import torch.nn as nn
import torch.nn.functional as F
import time
from datetime import timedelta

from src.models.edgeconv import EdgeConv,Dynamic_conv2d,LightweightConv2d,BNReLUConv2d
from src.models.rqconv2d import RangeQuantizedConv2d
from src.models.dla_edgeconv import DeepLayerAggregation
from src.models.edgenet import EdgeNet,EdgeFirstNet,EdgeSimpleNet
from config import *    

from src.utils import _circle_nms

from torchsummaryX import summary


if torch.cuda.is_available():
    torch.cuda.set_device(3)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') 

def get_time_dif(start_time):
    end_time = time.time()
    time_dif = end_time - start_time
    print('time_dif',time_dif)
    return timedelta(seconds=int(round(time_dif)))


if __name__ == '__main__':
    # # with torch.no_grad():
    # dla = DeepLayerAggregation(7).to(device)
    # # dla = DeepLayerAggregation(7,kernel_func = EdgeConv)
    
    # sample = torch.rand(1,7,64,2650).to(device)
    # sample_coord = torch.rand(1,3,64,2650).to(device)
    # sample_mask = torch.rand(1,1,64,2650).to(device)
    # res = dla(sample,sample_coord,sample_mask)
    # print(res.size())
    
    
    # model = EdgeNet(7,kernel=LightweightConv2d).to(device)
    # model = EdgeNet(7,kernel=Dynamic_conv2d).to(device)
    # model = EdgeNet(7,kernel=Dynamic_conv2d).to(device)
    #model = EdgeNet(7,kernel=EdgeConv).to(device)
    # model = EdgeSimpleNet(7,kernel=BNReLUConv2d).to(device)
    # model = EdgeSimpleNet(7,kernel=Dynamic_conv2d).to(device)
    # model = EdgeSimpleNet(7,kernel=EdgeConv).to(device)
    model = EdgeFirstNet(7,kernel=Dynamic_conv2d).to(device)
    # model = EdgeFirstNet(7,kernel=EdgeConv).to(device)
    # model = EdgeNet(7,kernel=BNReLUConv2d).to(device)
    
    # model = DeepLayerAggregation(7,kernel_func = EdgeConv)
    sample = torch.rand(1,7,64,2650).to(device)
    sample_coord = torch.rand(1,3,64,2650).to(device)
    sample_mask = torch.rand(1,1,64,2650).to(device)
    
    x = torch.cat([sample,sample_coord,sample_mask],1)
    

    model.train()
    summary_result = summary(model,x)
    print('summary_result',summary_result)


    print('0 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=x.device))
    model.eval()
    with torch.no_grad():
        start_time = time.time()
        for _ in range(200):
            res = model(x)
    time_diff = get_time_dif(start_time)

    print('time_diff',time_diff)
    print(res[0].size())
    print(res[1].size())
    print('1 torch.cuda.memory_allocated(device=device)',torch.cuda.memory_allocated(device=x.device))

    box_preds = res[0][0]
    hm_preds = res[1][0]
	
    scores, labels = torch.max(hm_preds, dim=-1)

    score_mask = scores > 0.1
    mask = score_mask 

    box_preds = box_preds[mask]
    scores = scores[mask]
    labels = labels[mask]

    boxes_for_nms = box_preds[:, [0, 1, 2, 3, 4, 5, -1]]

    centers = boxes_for_nms[:, [0, 1]] 
    boxes = torch.cat([centers, scores.view(-1, 1)], dim=1)
    selected = _circle_nms(boxes, min_radius=2, post_max_size=100)  
    print('selected',selected)



